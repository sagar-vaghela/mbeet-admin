import React, {Component} from 'react';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';




// Style
import './css/FilterToolbar.css'

class FilterToolbar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: props.rowShowing,
      filters: parseInt(props.currentFilter),
    };
    this.handleChangeFilter = this.handleChangeFilter.bind(this)

  }

  handleChange = (event, index, value) => {
    this.setState({value: value});
    this.props.onchange(value)
  }
  handleChangeFilter = (event, index, value) => {
    const {filters, onChangeFilter} = this.props
    this.setState({filters: value})
    onChangeFilter(filters[value].params,filters[value].value, value)
  }

  render() {
    const {showList, onSearch, filters, onChangeFilter, search, customFilter} = this.props

    return (
      <div>
        <Toolbar className="toolbar">

          <ToolbarGroup>
            <ToolbarTitle text="Show" className="filter-text" />
            <DropDownMenu className="show-count" value={this.state.value} onChange={this.handleChange}>
              {showList.map(item => {
                return (<MenuItem key={'filterlist'+item.count} value={item.count} primaryText={item.name} />)
              })}
            </DropDownMenu>

            <ToolbarSeparator className="separator" />

            <TextField
              hintText="Search ..."
              onChange={onSearch}
              defaultValue={search} />



          </ToolbarGroup>


          {filters &&
          <div className="filter-btns">
            <ToolbarTitle text="Filter:" className="filter-text" />

            <DropDownMenu value={this.state.filters} onChange={this.handleChangeFilter}>
              {filters.map((filter, key) => {
                return (<MenuItem
                            key={key}
                            value={key}
                            primaryText={filter.name} />)
              })}

            </DropDownMenu>

          </div> }
        </Toolbar>

        {customFilter}

      </div>
    );
  }
}

FilterToolbar.defaultProps = {
  customFilter: () => {}
}

export default FilterToolbar
