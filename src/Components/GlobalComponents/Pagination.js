import React from 'react'

// Style
import './css/Pagination.css'

function paginationNumbers(total, show, current){
  let limit = 7
  const pages = Math.ceil(total / show, 1)
  limit = (pages <= limit) ? pages : limit
  const range = Math.floor(limit / 2, 1)
  let end = (current > range) ? current + range : limit
  end = (end >= pages) ? pages : end

  let start = (current > range) ? current - range : 1
  start = (end === pages) ? ((end - limit) + 1) : start

  const result = []

  for(;start <= end ; start++){
    result.push(start)
  }

  return result
}

const Pagination = (props) => {
  const {currentPage, onPageChange, total, show} = props
  const pages = paginationNumbers(total, show, parseInt(currentPage))
  const pagesCount = Math.ceil(total / show, 1)

  if(pages.length <= 1) return false


  return (
    <nav aria-label="..." className="pagination-container">
      <ul className="pagination">

      <li className={'page-item '+((currentPage == 1) ? 'disabled' : '')}>
        <a className="page-link" href="#/"  tabIndex="-1" onClick={(e) => onPageChange(e,currentPage - 1)} >Previous</a>
      </li>

      {pages.map(page => {
        let active = (page == currentPage) ? 'active' : ''
        return(
          <li className={'page-item '+active} key={'pagin'+page}>
            <a className="page-link" href="#/" onClick={(e) => onPageChange(e,page)}>
              {page}
            </a>
          </li>
        )
      })}

      <li className={'page-item '+((currentPage == pagesCount) ? 'disabled' : '')}>
        <a className="page-link" href="#" onClick={(e) => onPageChange(e, parseInt(currentPage) + 1)}>Next</a>
      </li>

      </ul>
    </nav>
  )

  /*
  if(props.total <= props.show) return false
  const {currentPage, onPageChange} = props
  let numbers = []

  const totalView = 4
  const pages     = Math.ceil(props.total / props.show, 1)
  const startPage = (currentPage > totalView) ? (currentPage - 2) : 1
  const endPage   = ((currentPage + totalView) < pages) ? (startPage + totalView) : pages

  if(startPage > 1){
    numbers.push(<li key={'pagin'+Math.random()} className="page-item"><a onClick={(e) => onPageChange(e,1)} className="page-link" href="#/">1</a></li>)
    numbers.push(<li key={'pagin'+Math.random()} className="page-item disabled"><a  className="page-link" href="#/"  tabIndex="-1">...</a></li>)
  }

  for(let i = startPage; i<= endPage; i++){
    let active = (i == currentPage) ? 'active' : ''
    numbers.push(<li key={'pagin'+i} className={'page-item '+active}><a onClick={(e) => onPageChange(e,i)} className="page-link" href="#/">{i}</a></li>)
  }

  if(endPage < pages){
    numbers.push(<li key={'pagin'+Math.random()} className="page-item disabled"><a className="page-link" href="#/"  tabIndex="-1">...</a></li>)
    numbers.push(<li key={'pagin'+Math.random()} className="page-item"><a onClick={(e) => onPageChange(e,pages)} className="page-link" href="#/">{pages}</a></li>)
  }

  return (
    <nav aria-label="..." className="pagination-container">
      <ul className="pagination">
        <li key={'pagin'+Math.random()} className={'page-item '+((currentPage == 1) ? 'disabled' : '')}>
          <a className="page-link" href="#/"  tabIndex="-1" onClick={(e) => onPageChange(e,currentPage - 1)} >Previous</a>
        </li>
        {numbers}
        <li key={'pagin'+Math.random()} className={'page-item '+((currentPage == pages) ? 'disabled' : '')}>
          <a className="page-link" href="#" onClick={(e) => onPageChange(e,currentPage + 1)}>Next</a>
        </li>
      </ul>
    </nav>
  )
  */
}
export default Pagination
