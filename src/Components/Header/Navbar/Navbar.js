import React, {Component} from 'react'

import NavCollapse from './navCollapse/NavCollapse'
import UserProfileBtn from './userProfileBtn/UserProfileBtn'
import './navStyle.css'

export default class Navbar extends Component{
  render(){
    const isLogged = (sessionStorage.getItem('TOKEN')) ? true : false
    return (
      <nav className="navbar navbar-toggleable-md navbar-light bg-faded fixed-top">
        <NavCollapse />
        {isLogged && <UserProfileBtn />}
      </nav>
    )
  }
}
