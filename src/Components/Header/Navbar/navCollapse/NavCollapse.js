import React from 'react'

import Logo from './Logo'
import TopMenuList from './TopMenuList'

const NavCollapse = (props) => {
    return (
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <Logo />
      <TopMenuList />
    </div>
  )
}
export default NavCollapse
