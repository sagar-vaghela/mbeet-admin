import React, {Component} from 'react'
import {connect} from 'react-redux'

import LoginContainer from "../loginContainer/LoginContainer"
import ResetPasswordForm from './ResetPasswordForm'

// External Packages
import serializeForm from 'form-serialize'

// Global Components
import Loading from '../../../GlobalComponents/Loading'
import AlertMsg from '../../../GlobalComponents/AlertMsg'

// Login Action - Redux Action
import {resetPassword, initAuth} from '../../../../Redux/actions/authActions'

class ResetPassword extends Component{

  constructor(props){
    super(props)

    this.handleResetPassword = this.handleResetPassword.bind(this)
  }

  componentWillMount(){
    const {dispatch} = this.props
    dispatch(initAuth())
  }

  handleResetPassword(e){
    e.preventDefault()
    // get dispatch from props
    const {dispatch} = this.props

    // get form data
    let formData = serializeForm(e.target, { hash: true })

    // dispatch
    dispatch(resetPassword(formData.email))
  }

  render(){
    // get messages from Redux store
    const {errMsg, succMsg, fetching} = this.props

    return(
      <LoginContainer title="Reset Password">
        {/* Loading */}
        {fetching && <Loading />}

        {/* after dispatch, display the error or success message */}
        {(errMsg || succMsg) && <AlertMsg error={errMsg} success={succMsg} />}

        <ResetPasswordForm onSubmit={this.handleResetPassword} done={succMsg ? true : false} />
      </LoginContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    errMsg: store.auth.errMsg,
    succMsg: store.auth.succMsg,
    fetching: store.auth.fetching,
  }
}

ResetPassword = connect(mapStateToProps)(ResetPassword);
export default ResetPassword;
