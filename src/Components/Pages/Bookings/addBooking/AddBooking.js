/*
* AddBooking: Compnent
* Child of: Bookings Container
*/

// Main Packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

import {addBooking, clearMsg} from '../../../../Redux/actions/bookingsActions'
import {getUsers, getCurrentUserPermission} from '../../../../Redux/actions/usersActions'
import {getCities} from '../../../../Redux/actions/citiesActions'
import {getUnits} from '../../../../Redux/actions/unitsActions'

// Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  FilterToolbar,
  FlatsList,
  Loading,
  AlertMsg,} from '../../../GlobalComponents/GlobalComponents'

import Form from './Form/Form'


// Material UI Icons
import LocationCity       from 'material-ui/svg-icons/social/location-city'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

class AddBooking extends Component{

  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      loading: false,
    }
  }


  handleSubmit(e){
    e.preventDefault()
    const {dispatch} = this.props
    var formData = new FormData(e.target)

    dispatch(addBooking(formData))
    this.setState({loading: false})
  }


  componentWillMount(){
    const {dispatch, match} = this.props
    dispatch(getUsers(100000, 0))
    dispatch(getCities(10000))
    dispatch(getCurrentUserPermission())
    dispatch(getUnits());
  }


  // render
  render(){

    // Store props
    const {fetching, errMsg, succMsg, users, cities,  current_user_permissions, units} = this.props
    const {loading} = this.state

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Add New Booking',
      icon: (<LocationCity className="pagetitle-icon"/>),
    }


    return(
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['bookings','Add New Booking']} />
          <PageHeader {...pageHeaderOptions} />
          <div className="page-container">

            {(
                fetching
                || (!users || !cities )
                || succMsg
                || loading)
            && <Loading success={succMsg} />}

            {(users && cities) &&
              <Form
                formType="add"
                handleSubmit={this.handleSubmit}
                formType="add"
                users={users}
                cities={cities}
                permissions={current_user_permissions}
                units={units}
                 />
              }

            {(errMsg) && <AlertMsg error={errMsg} />}

          </div>
        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}


const mapStateToProps = (store) => {
  return {
    errMsg: store.bookings.errMsg,
    succMsg: store.bookings.succMsg,
    fetching: store.bookings.fetching,
    users: store.users.users,
    cities: store.cities.cities,
    units: store.units.units,
    current_user_permissions: store.users.current_user_permissions,
  }
}

AddBooking = connect(mapStateToProps)(AddBooking)
export default AddBooking
