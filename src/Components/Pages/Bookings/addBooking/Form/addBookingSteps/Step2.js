/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';

import { TextValidator, SelectValidator} from 'react-material-ui-form-validator'

import Avatar       from 'material-ui/Avatar';
import { Link }           from 'react-router-dom'
import ReactTooltip from 'react-tooltip'

import RowUnit from './RowUnit'

class Step2 extends Component{
  constructor(props){
    super(props)

    this.state = {
      unit_id: ''
    }
    this.handleSelectUnit = this.handleSelectUnit.bind(this)
  }

  handleSelectUnit(unit){
    this.setState({unit_id: unit.id});
    this.props.onChange('unit_id', unit.id)
  }

  render(){
    const {units, permissions, display} = this.props
    const {unit_id} = this.state;
    return(
      <div style={{display: (display ? 'block' : 'none')}}>
        <div className="page-container units-container">
          <div className="card-deck">
            {units.map(unit => {
              return (<RowUnit key={'unit'+unit.id} unit={unit} permissions={permissions} handleSelectUnit={this.handleSelectUnit} selected={unit_id}/>)
            })}
          </div>
        </div>
      </div>
    )
  }

}

export default Step2
