import React from 'react'
import dateFormat  from 'dateformat'
import { Link }           from 'react-router-dom'

import {paymentsStatus, leaseStatus} from '../../../../Utilities/options'

const BookingDetails = (props) => {
  const {booking} = props
  return (
    <table className="table table-hoverd">

  <tbody>
      <tr>
        <td>ID</td>
        <td>{booking.id}</td>
      </tr>
      <tr>
        <td>Check In</td>
        <td>{dateFormat(new Date(booking.check_in),'ddd, d mmm yyyy h:MM TT')}</td>
      </tr>
      <tr>
        <td>Check Out</td>
        <td>{dateFormat(new Date(booking.check_out),'ddd, d mmm yyyy h:MM TT')}</td>
      </tr>
      <tr>
        <td>Payment Status</td>
        <td>{paymentsStatus[booking.payment_status]}</td>
      </tr>
      {/*
      <tr>
        <td>Lease Status</td>
        <td>{leaseStatus[booking.lease_status]}</td>
      </tr>
      */}
      <tr>
        <td>Total amount</td>
        <td>SAR {booking.total_amount}</td>
      </tr>
      <tr>
        <td>Booking Cancelled</td>
        <td>
          {booking.booking_cancelled ? 'Yes' : 'No'}
          {booking.reason &&
            <span>
              <br />
              <b>Cancel Reason :</b> {booking.reason.title}
            </span>
          }
        </td>
      </tr>
      <tr>
        <td>Created At</td>
        <td>{dateFormat(new Date(booking.created_at),'ddd, d mmm yyyy h:MM TT')}</td>
      </tr>


      <tr>
        <td>Unit</td>
        <td>
          <Link to={'/units/show/'+booking.unit.id}>
            {booking.unit.title}
          </Link>
        </td>
      </tr>

      <tr>
        <td>User</td>
        <td>
          <Link to={'/users/show/'+booking.user.id}>
            {booking.user.name}
          </Link>
        </td>
      </tr>

      <tr>
        <td>Transaction ID</td>
        <td>{booking.payment_txn.transaction_id}</td>
      </tr>
      </tbody>
    </table>
  )
}
export default BookingDetails
