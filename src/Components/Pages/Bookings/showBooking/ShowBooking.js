import React, {Component} from 'react'
import {connect} from 'react-redux'

// Users Redux Actions
import {getBooking} from '../../../../Redux/actions/bookingsActions'


import EventNoteIcon      from 'material-ui/svg-icons/notification/event-note'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

import BookingDetails from './BookingDetails'

import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading} from '../../../GlobalComponents/GlobalComponents'

class ShowBooking extends Component {

  componentWillMount(){
    const {dispatch, match} = this.props
    const booking_id = match.params.id

    dispatch(getBooking(booking_id))

  }
  render(){

    // Store props
    const {booking, fetching, errMsg} = this.props

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Booking Details',
      icon: (<EventNoteIcon className="pagetitle-icon"/>),
    }

    return (
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['booking']} />
          <PageHeader {...pageHeaderOptions} />
        </CSSTransitionGroup>
        <div className="page-container">
          {(fetching || !booking) && <Loading error={errMsg} />}
          {booking && <BookingDetails booking={booking} />}
        </div>
      </PagesContainer>
    )
  }

}

const mapStateToProps = (store) => {
  return {
    booking: store.bookings.booking,
    errMsg: store.bookings.errMsg,
    fetching: store.bookings.fetching,
  }
}

ShowBooking = connect(mapStateToProps)(ShowBooking)
export default ShowBooking
