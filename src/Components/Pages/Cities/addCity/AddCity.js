import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Redirect } from 'react-router'

import PlaceIcon          from 'material-ui/svg-icons/maps/place'

import AddCityForm from './AddCityForm'
import swal from 'sweetalert'

// Users Redux Actions
import {addCity} from '../../../../Redux/actions/citiesActions'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading,
  AlertMsg} from '../../../GlobalComponents/GlobalComponents'

import {
      cloudinaryUpload,
      imagePath,
      pathToUrl,
      filePreview
    } from '../../../../Utilities/cloudinaryUpload'

class AddCity extends Component{

  constructor(props){
    super(props)
    this.state = {
      redirect: false,
      loading: false,
      image: null,
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleSetImage = this.handleSetImage.bind(this)
    this.fetchAddCity = this.fetchAddCity.bind(this)
  }

  handleSetImage(image){
    this.setState({image})
  }

  handleSubmit(e){
    e.preventDefault()
    const {dispatch, match, succMsg, errMsg} = this.props

    const {image} = this.state
    var formData = new FormData(e.target)
    let obj = e.target

    this.setState({loading: true})
    // if the user change the image
    if(image){
      cloudinaryUpload(image, false).then(img => {

        formData.append('picture_attributes[name]', imagePath(img[0].url));

        this.fetchAddCity(formData, obj)
      })


      //formData.append('pictures_attributes[name]', 123);
    }else{
      this.fetchAddCity(formData, obj)
    }

  }

  fetchAddCity(formData, form){
    const {dispatch} = this.props
    dispatch(addCity(formData)).then(() => {

      const _this = this
      form.reset()

      swal({
        title: "City added successfully",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Back to cities list",
        closeOnConfirm: true,
      },
      function(){
        _this.setState({redirect: true})
      })
      this.setState({loading: false, image: null})
    })
    .catch(() => {
      this.setState({loading: false})
    })

  }


  // render
  render(){
    const {fetching, succMsg, errMsg} = this.props
    const {loading} = this.state
    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Create New City',
      icon: (<PlaceIcon className="pagetitle-icon"/>),
    }

    return (
      <PagesContainer>
        <Breadcrumb path={['cities','add city']} />
        <PageHeader {...pageHeaderOptions} />

        <div className="page-container">

          {loading && <Loading />}

          {this.state.redirect && <Redirect to="/cities" />}

          <div className="row justify-content-center">
            <div  className="col-lg-8 col-md-8">
              <AddCityForm
                handleSubmit={this.handleSubmit}
                handleSetImage={this.handleSetImage}
                image={this.state.image}/>

              {(errMsg) && <AlertMsg error={errMsg} />}
            </div>
          </div>


        </div>

      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    errMsg: store.cities.errMsg,
    succMsg: store.cities.succMsg,
    fetching: store.cities.fetching,
  }
}

AddCity = connect(mapStateToProps)(AddCity)
export default AddCity
