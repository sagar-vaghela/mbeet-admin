/*
* AddCityForm: Object
* Child of: AddCity
*/

// Main Packages
import React, {Component} from 'react'

import Checkbox from 'material-ui/Checkbox'

import {filePreview} from '../../../../Utilities/cloudinaryUpload'

// Material UI
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';




class AddCityForm extends Component {
  constructor(props){
    super(props)
    this.handleUpload = this.handleUpload.bind(this)
  }

  handleUpload(e){
    e.preventDefault()
    const files = e.target.files

    // stop do anyting if the user click Cancel Btn
    if (files.length === 0) return false


    filePreview(files).then(image => {
      this.props.handleSetImage(image)
    })

  }
  render(){

    return(
      <form onSubmit={this.props.handleSubmit}>

       <div className="form-field">
         <TextField
          required
          hintText="Name English"
          style={{width: '100%'}}
          name="name_en"
        />
      </div>

      <div className="form-field">
        <TextField
         required
         hintText="Name Arabic"
         style={{width: '100%'}}
         name="name_ar"
       />
      </div>

      <div className="form-field">
        <label>Image </label>
        <img src={this.props.image} width="100" height="100" /> <br />
        <input type="file" name="file" onChange={this.handleUpload} />

      </div>


      <div className="form-field">
        <Checkbox label="Publish" name="published" value="true" />
      </div>



      <div className="form-field submit">
        <RaisedButton label="Create City" type="submit" primary={true} />
      </div>

      </form>
    )
  }

}

export default AddCityForm;
