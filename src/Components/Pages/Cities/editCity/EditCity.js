import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Redirect } from 'react-router'
import PlaceIcon          from 'material-ui/svg-icons/maps/place'

import EditCityForm from './EditCityForm'
import swal from 'sweetalert'
// Users Redux Actions
import {getCity, editCity} from '../../../../Redux/actions/citiesActions'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading,
  AlertMsg} from '../../../GlobalComponents/GlobalComponents'

import {
    cloudinaryUpload,
    imagePath,
    pathToUrl,
    filePreview
  } from '../../../../Utilities/cloudinaryUpload'

class EditCity extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      redirect: false,
      image: null,
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleSetImage = this.handleSetImage.bind(this)
  }

  handleSetImage(image){
    this.setState({image})
  }

  componentWillMount(){

    const {dispatch, match} = this.props
    const cityid = match.params.id

    dispatch(getCity(cityid)).then(() => this.setState({loading: false}))
  }

  fetchForm(cityid, formData, form){
    const {dispatch} = this.props

    dispatch(editCity(cityid, formData)).then(() => {
      const _this = this
      this.setState({loading: false})

      swal({
        title: "City updated successfully",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Back to citis list",
        closeOnConfirm: true,
      },
      function(){
        _this.setState({redirect: true})
      });
    })
    .catch(() => {
      this.setState({loading: false})
    })
  }
  handleSubmit(e){
    e.preventDefault()
    const {dispatch, match, succMsg, errMsg} = this.props
    const cityid = match.params.id
    const {image} = this.state
    var formData = new FormData(e.target)
    let obj = e.target

    this.setState({loading: true})
    // if the user change the image
    if(image){
      cloudinaryUpload(image, false).then(img => {
        const img_id = formData.get('image_id')

        formData.append('picture_attributes[name]', imagePath(img[0].url));

        if(img_id)
          formData.append('picture_attributes[id]', img_id);


        this.fetchForm(cityid, formData, obj)
      })


      //formData.append('pictures_attributes[name]', 123);
    }else{
      this.fetchForm(cityid, formData, obj)
    }

  }
  // render
  render(){
    const {city, fetching, succMsg, errMsg} = this.props
    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Edit City',
      icon: (<PlaceIcon className="pagetitle-icon"/>),
    }

    return (
      <PagesContainer>
        <Breadcrumb path={['cities','Edit city']} />
        <PageHeader {...pageHeaderOptions} />

        <div className="page-container">

          {this.state.loading && <div><Loading error={errMsg} success={succMsg} /> <br /><br /><br /><br /></div> }

          {this.state.redirect && <Redirect to="/cities" />}

          <div className="row justify-content-center">
            <div  className="col-lg-8 col-md-8" style={{position: 'inherit'}}>

              {city
                && <EditCityForm
                        handleSubmit={this.handleSubmit}
                        city={city}
                        handleSetImage={this.handleSetImage}
                        image={this.state.image} /> }

              {(errMsg) && <AlertMsg error={errMsg} />}
            </div>
          </div>


        </div>

      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    city: store.cities.city,
    errMsg: store.cities.errMsg,
    succMsg: store.cities.succMsg,
    fetching: store.cities.fetching,
  }
}

EditCity = connect(mapStateToProps)(EditCity)
export default EditCity
