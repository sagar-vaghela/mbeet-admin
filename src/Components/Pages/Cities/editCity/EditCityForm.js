/*
* EditCityForm: Object
* Child of: EditCity
*/

// Main Packages
import React, {Component} from 'react'

import Checkbox from 'material-ui/Checkbox';

import {filePreview} from '../../../../Utilities/cloudinaryUpload'

// Material UI
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';




class EditCityForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      published: this.props.city.published,
      image: null,
      upload_new: true,
    }
    this.handlePublished = this.handlePublished.bind(this)
    this.handleUpload = this.handleUpload.bind(this)
  }

  componentWillMount(){
    const {city} = this.props

    if(city.image){
      const url = city.image.url
      this.setState({image: url, upload_new: false})
    }

  }


  handleUpload(e){
    e.preventDefault()
    const files = e.target.files

    // stop do anyting if the user click Cancel Btn
    if (files.length === 0) return false



    filePreview(files).then(image => {
      this.setState({image})
      this.props.handleSetImage(image)

    })


  }

  handlePublished(e){
    if(e.target.value == 'true'){
      this.setState({published: false})
    }else{
      this.setState({published: true})
    }
  }

  render(){
    const {city, image} = this.props
    return(
      <form onSubmit={this.props.handleSubmit}>

       <div className="form-field">
         <TextField
          required
          hintText="Name English"
          style={{width: '100%'}}
          name="name_en"
          defaultValue={city.name_en}
        />
      </div>

      <div className="form-field">
        <TextField

         hintText="Name Arabic"
         style={{width: '100%'}}
         name="name_ar"
         defaultValue={city.name_ar}
       />
     </div>


     <div className="form-field">
       <label>Image </label>
       {!this.state.upload_new ? (
         <span>
           <img src={this.state.image} width="100" height="100" /> <br />
           <input type="file" name="file" onChange={this.handleUpload} />
           <input type="hidden" name="image_id" value={city.image.id} />
          </span>
       ):(
         <span>
           {this.state.image &&
             <span>
             <img src={this.state.image} width="100" height="100" /> <br />
             </span>
           }
           <input type="file" name="file" onChange={this.handleUpload} />
         </span>
       )}
     </div>



      <div className="form-field">
        <Checkbox label="Publish" onClick={this.handlePublished}  value={this.state.published} defaultChecked={city.published} />
        <input type="hidden" value={this.state.published} name="published" />
      </div>



      <div className="form-field submit">
        <RaisedButton label="Update" type="submit" primary={true} />
      </div>

      </form>
    )
  }

}

export default EditCityForm;
