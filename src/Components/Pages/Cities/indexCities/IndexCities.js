import React, {Component} from 'react'
import {connect} from 'react-redux'

// Users Redux Actions
import {getCities, deleteCity, citiesSearch} from '../../../../Redux/actions/citiesActions'

import { getCurrentUserPermission } from '../../../../Redux/actions/usersActions'

import {deleteItem} from '../../../../Utilities/functions'

import swal from 'sweetalert'

import RowCity from './rowCity/RowCity'
import {DummyCityRow} from './rowCity/DummyCityRow'
// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  TableList} from '../../../GlobalComponents/GlobalComponents'

import PlaceIcon          from 'material-ui/svg-icons/maps/place'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

class IndexCities extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,

      showList : [
        {count: 10, name: '10'},
        {count: 15, name: '15'},
        {count: 30, name: '30'},
      ],
      filters: [
        {value: 'false', params: 'soft_delete', name: 'All'},
        {value: 'true',params: 'published', name: 'Published'},
        {value: 'false',params: 'published', name: 'Unpublished'},
        {value: 'true', params: 'soft_delete', name: 'Deleted'},
      ],

      comID: 'cities',
      currentFilter: 0,
      currentPage: 1,
      show: 10,
      sort_by: 'id',
      sort_direction: 'asc',
      search: '',
    }
    this.handlePagination   = this.handlePagination.bind(this)
    this.handleShowing = this.handleShowing.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleFilters = this.handleFilters.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    this.handleSorting = this.handleSorting.bind(this)

    this.setSession = this.setSession.bind(this)
    this.getSession = this.getSession.bind(this)
  }

  setSession(par, value){

    if(typeof(value) === 'object'){
      value = this.state[par]
    }

    sessionStorage.setItem(this.state.comID+'-'+par,value)
    var obj = {};
    obj[par] = value
    this.setState(obj)
  }

  getSession(par){
    return sessionStorage.getItem(this.state.comID+'-'+par)
  }

  componentWillMount(){

    this.setSession('currentFilter', this.getSession('currentFilter'))
    this.setSession('currentPage', this.getSession('currentPage'))
    this.setSession('show', this.getSession('show'))
    this.setSession('sort_by', this.getSession('sort_by'))
    this.setSession('sort_direction', this.getSession('sort_direction'))
    this.setSession('search', this.getSession('search'))

    const {filters} = this.state

    const offset = (this.getSession('currentPage') - 1) * this.getSession('show')

    // On Load the Component get the users
    const {dispatch} = this.props
    dispatch(getCities(
      this.getSession('show'),
      offset,
      filters[this.getSession('currentFilter')],
      this.getSession('sort_by'),
      this.getSession('sort_direction'),
      this.getSession('search'),
    )).then(() => {

      // stop loading if successfull fetched the users
      this.setState({loading: false})
    })
    dispatch(getCurrentUserPermission())
  }

  /***************************
  * On Click Delete
  * @userid : (Intger)
  ****************************/
  handleDelete(cityid){

    const {dispatch, errMsg, succMsg} = this.props
    let _this = this
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: 'Yes, Delete it!',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    () => {
      dispatch(deleteCity(cityid)).then(res => {
        swal({
          title: 'Deleted',
          type: 'success',
          text: succMsg,
          timer: 2000,
          showConfirmButton: false
        })
      })
      .catch(err => {
        _this.forceUpdate()
        swal("Error", err.response.data.message, "error");
      })
    })

  }

  /***************************
  * On Change Filter
  ****************************/
  handleFilters(filter_type, value, index){
    const {dispatch} = this.props
    const {filters} = this.state
    this.setSession('currentFilter', index)
    this.setSession('currentPage', 1)
    dispatch(getCities(
      this.state.show, // limit
      0, // offset
      filters[index],// filter
      this.state.sort_by, //sort by
      this.state.sort_direction, // sort direction
      this.state.search,
    ))
  }

  /***************************
  * On search
  ****************************/
  handleSearch(e){
    const {dispatch} = this.props
    const {filters} = this.state
    const length = e.target.value.length
    const s = e.target.value

    if(length >= 3 || length === 0)
      dispatch(getCities(
        this.getSession('show'),
        0,
        filters[this.getSession('currentFilter')],
        this.getSession('sort_by'),
        this.getSession('sort_direction'),
        s,
      ))

      this.setSession('search', s)
      this.setSession('currentPage', 1)
  }

  /***************************
  * Handle Sorting
  ****************************/
  handleSorting(sort_by){
    if(sort_by === 0) return false
    const {dispatch} = this.props
    const {filters, currentFilter, sort_direction} = this.state
    const sort_dir = (sort_direction === 'asc') ? 'desc' : 'asc'

    this.setState({
      sort_by: sort_by,
      sort_direction: sort_dir
    })

    this.setSession('sort_by', sort_by)
    this.setSession('sort_direction', sort_dir)
    this.setSession('currentPage', 1)

    dispatch(getCities(
      this.getSession('show'), // limit
      0, // offset
      filters[this.getSession('currentFilter')], // filter
      this.getSession('sort_by'), // sort by
      this.getSession('sort_direction'), // sort direction
      this.getSession('search'),
    ))

  }

  /***************************
  * On Click Pagination
  ****************************/
  handlePagination(e, page){
    e.preventDefault()
    const {dispatch} = this.props
    const {filters, currentFilter, sort_by, sort_direction, search} = this.state
    // prepair the offset
    const offset = (page - 1) * this.state.show

    // get users
    dispatch(getCities(
      this.state.show,  // limit
      offset, // offset
      filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
    )).then(() => {
      // change the Pagination number
      this.setState({currentPage: page})
      this.setSession('currentPage', page)
    })

  }

  /***************************
  * On change showing
  ****************************/
  handleShowing(count){
    const {dispatch} = this.props
    const {filters, currentFilter, sort_by, sort_direction, search} = this.state
    dispatch(getCities(
      count, // limit
      0, // offset
      filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
    )).then(() => {
      this.setState({show: count, currentPage: 1})
      this.setSession('show', count)
      this.setSession('currentPage', 1)
    })
  }

  render(){

    // Table columns
    const columns = [
      {params: 'id', title: 'ID'},
      {params: 0, title: 'Name English'},
      {params: 0, title: 'Name Arabic'},
      {params: 0, title: 'Units'},
      {params: 'published', title: 'Published'},
      {params: 0, title: 'Action'}
    ]

    // Store props
    const {cities, total, errMsg, fetching, current_user_permissions} = this.props

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Cities List',
      icon: (<PlaceIcon className="pagetitle-icon"/>),
      addbtn: current_user_permissions.city_permissions.create? '/cities/add' : ''
    }

    // TableList Options
    const tableListOptions = {
      columns: columns,
      loading: this.state.loading,
      update: fetching,
      onPageChange: this.handlePagination,
      total: total,
      rowShowing: parseInt(this.getSession('show')),
      onChangeShowing: this.handleShowing,
      page: this.getSession('currentPage'),
      showList: this.state.showList,
      onSearch: this.handleSearch,
      search: this.getSession('search'),
      onError: errMsg,
      filters: this.state.filters,
      onChangeFilter: this.handleFilters,
      currentFilter: this.getSession('currentFilter'),
      onSorting: this.handleSorting,
    }

    return(
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['cities']} />
          <PageHeader {...pageHeaderOptions} />

          <TableList {...tableListOptions}>

            { // if fetched users
              !this.state.loading ?
              (
                cities.map(city => {
                  return (<RowCity key={'city'+city.id} city={city} onDelete={this.handleDelete} permissions={current_user_permissions} />)
                })
              )
              :
              (
                DummyCityRow()
              )
            /* end if */ }

          </TableList>

        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    cities: store.cities.cities,
    current_user_permissions: store.users.current_user_permissions,
    total: store.cities.total,
    errMsg: store.cities.errMsg,
    fetching: store.cities.fetching,
  }
}

IndexCities = connect(mapStateToProps)(IndexCities)
export default IndexCities
