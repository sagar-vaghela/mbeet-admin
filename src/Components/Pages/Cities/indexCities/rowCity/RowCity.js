import React from 'react'
import { Link }           from 'react-router-dom'

import dateFormat  from 'dateformat'

// Material UI
import DoneIcon           from 'material-ui/svg-icons/action/done';
import ClearIcon          from 'material-ui/svg-icons/content/clear';
import MenuItem from 'material-ui/MenuItem';
import IconMenu     from 'material-ui/IconMenu';
import IconButton   from 'material-ui/IconButton';
import MoreVertIcon       from 'material-ui/svg-icons/navigation/more-vert';
import DeleteIcon         from 'material-ui/svg-icons/action/delete';
import DeleteForeverIcon  from 'material-ui/svg-icons/action/delete-forever';
import ModeEditIcon       from 'material-ui/svg-icons/editor/mode-edit';
import RemoveRedEyeIcon   from 'material-ui/svg-icons/image/remove-red-eye';


const RowBooking = (props) => {
  const {city, onDelete, permissions} = props
  return (
    <tr>
      <td>{city.id}</td>
      <td>{city.name_en}</td>
      <td>{city.name_ar}</td>
      <td>{city.units_no}</td>
      <td>
        {city.published ? (
          <DoneIcon className="done-icon" />
        ):(
          <ClearIcon className="clear-icon" />
        )}
      </td>

      <td>
        {!city.soft_delete ? (
          <IconMenu
             iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
             anchorOrigin={{horizontal: 'left', vertical: 'top'}}
             targetOrigin={{horizontal: 'left', vertical: 'top'}}
          >

            {permissions.city_permissions.edit &&
              <Link to={'/cities/edit/'+city.id} className="MenuItemBtn">
                <MenuItem primaryText="Edit" leftIcon={<ModeEditIcon />} />
              </Link>
            }

            {permissions.city_permissions.delete &&
              <MenuItem primaryText="Delete" onClick={() => onDelete(city.id)} leftIcon={<DeleteIcon />} />
            }
          </IconMenu>
        ):(
          <DeleteIcon className="deleted-icon" />
        )}

      </td>
    </tr>
  )
}

export default RowBooking
