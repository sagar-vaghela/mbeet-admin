import React,{Component} from 'react'
import {connect} from 'react-redux'
import { Redirect } from 'react-router'

import AddCouponForm from './AddCouponForm'

// Coupons Redux Actions
import {addCoupon} from '../../../../Redux/actions/couponsActions'
import swal from 'sweetalert'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

// Material UI Icons
import LoyaltyIcon        from 'material-ui/svg-icons/action/loyalty'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  AlertMsg,
  Loading} from '../../../GlobalComponents/GlobalComponents'

class AddCoupon extends Component{

  constructor(props){
    super(props)
    this.state = {
      redirect: false
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(e){
    e.preventDefault()
    const {dispatch, succMsg} = this.props
    var formData = new FormData(e.target)
    let obj = e.target
    dispatch(addCoupon(formData)).then(() => {
      const _this = this
      obj.reset()
      swal({
        title: "Coupon added successfully",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Back to coupons list",
        closeOnConfirm: true,
      },
      function(){
        _this.setState({redirect: true})
      });
    })

  }

  render(){

    // Store props
    const {errMsg, succMsg, fetching} = this.props

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Add New Coupon',
      icon: (<LoyaltyIcon className="pagetitle-icon"/>),
    }
    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }
    return(
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['coupons','add coupon']} />
          <PageHeader {...pageHeaderOptions} />

          <div className="page-container">

            {fetching && <Loading  />}
            {this.state.redirect && <Redirect to="/coupons" />}

            <div className="row justify-content-center">
              <div  className="col-lg-8 col-md-8" style={{position: 'inherit'}}>
                <AddCouponForm handleSubmit={this.handleSubmit} />
              </div>
            </div>

            {/* after dispatch, display the error or success message */}
            {(errMsg) && <AlertMsg error={errMsg} />}

          </div>

        </CSSTransitionGroup>
      </PagesContainer>
    )
  }

}

const mapStateToProps = (store) => {
  return {
    errMsg: store.coupons.errMsg,
    fetching: store.coupons.fetching,
    succMsg: store.coupons.succMsg,
  }
}

AddCoupon = connect(mapStateToProps)(AddCoupon)
export default AddCoupon
