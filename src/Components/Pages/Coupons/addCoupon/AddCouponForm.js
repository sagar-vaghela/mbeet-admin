/*
* AddCouponForm: Object
* Child of: AddCoupon
*/

// Main Packages
import React, {Component} from 'react'

// External Packages
import {Loading} from '../../../GlobalComponents/GlobalComponents'

// Material UI
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';

import { ValidatorForm, TextValidator, SelectValidator, DateValidator} from 'react-material-ui-form-validator'

class AddCouponForm extends Component {
  constructor(props){
    super(props)

    this.state = {
      loading: false,
      disabled: false,
      status: null,
      form: {},
    }

    this.hanldedisabled = this.hanldedisabled.bind(this)
    this.handleChangeField = this.handleChangeField.bind(this)
    this.handleStatus = this.handleStatus.bind(this)
  }

  handleChangeField(event){
    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })
  }

  handleOnChangeFromStarting_date = (a, data) => this.setState({starting_date: data})
  handleOnChangeFromExpiration_date = (a, data) => this.setState({expiration_date: data})

  hanldedisabled(event, isInputChecked)
  {
    this.setState({disabled: isInputChecked});
    this.handleStatus();
  }

  handleStatus()
  {
    if (this.state.disabled)
    {
      this.setState({status: "disable"});
    }
    else
    {
      this.setState({status: "enable"});
    }
  }

  render(){
    const { starting_date , expiration_date , form } = this.state


    return(
      <ValidatorForm ref="form" onSubmit={this.props.handleSubmit}>

        {this.state.loading && <Loading />}

        <div className="form-field">
          <TextValidator
              floatingLabelText="Value"
              onChange={this.handleChangeField}
              name="value"
              value={form.value? form.value : ''}
              style={{width: '100%'}}
              validators={['required']}
              errorMessages={['this field is required']}
            />
        </div>

        <div className="form-field">
          <DateValidator
              hintText="Starting Date"
              container="inline"
              name="starting_date"
              mode="landscape"
              value={starting_date ? starting_date : null}
              onChange={this.handleOnChangeFromStarting_date}
              validators={['required']}
              errorMessages={['this field is required']}
              style={{color: 'red'}} />
        </div>

        <div className="form-field">
          <DateValidator
              hintText="Expiration Date"
              container="inline"
              name="expiration_date"
              validators={['required']}
              errorMessages={['this field is required']}
              mode="landscape"
              value={expiration_date ? expiration_date : null}
              onChange={this.handleOnChangeFromExpiration_date}
              style={{color: 'red'}} />
        </div>

        <div className="form-field">
            <Toggle
              label="Disabled"
              defaultToggled={this.state.disabled? this.state.disabled : false}
              onToggle={this.hanldedisabled}
              />
            <input type="hidden" name="status" value={this.state.status? this.state.status : 'disable'} />
        </div>

        <div className="form-field">
          <TextValidator
              floatingLabelText="Coupon Code"
              onChange={this.handleChangeField}
              name="code"
              value={form.code? form.code : ''}
              style={{width: '100%'}}
              validators={['required']}
              errorMessages={['this field is required']}
            />
        </div>

      <div className="form-field submit">
        <RaisedButton label="Create Coupon" type="submit" primary={true} />
      </div>

      </ValidatorForm>
    )
  }

}
export default AddCouponForm;
