import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Redirect } from 'react-router'
import swal from 'sweetalert'
// Coupons Redux Actions
import {getCoupon, editCoupon} from '../../../../Redux/actions/couponsActions'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading,
  AlertMsg} from '../../../GlobalComponents/GlobalComponents'

import EditCouponForm from './EditCouponForm'

// Material UI Icons
import LoyaltyIcon        from 'material-ui/svg-icons/action/loyalty'

class EditCoupon extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      redirect: false,
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount(){

    const {dispatch, match} = this.props
    const couponid = match.params.id

    dispatch(getCoupon(couponid)).then(() => this.setState({loading: false}))
  }

  handleSubmit(e){
    e.preventDefault()
    const {dispatch, match, succMsg} = this.props
    const couponid = match.params.id
    var formData = new FormData(e.target)
    let obj = e.target
    dispatch(editCoupon(couponid, formData)).then(() => {
      const _this = this
      obj.reset()
      swal({
        title: "Coupon updated successfully",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Back to coupons list",
        closeOnConfirm: true,
      },
      function(){
        _this.setState({redirect: true})
      });
    })

  }

  // render
  render(){

    // Store props
    const {coupon, errMsg, succMsg, fetching} = this.props

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Edit Coupon',
      icon: (<LoyaltyIcon className="pagetitle-icon"/>),
    }

    return(
      <PagesContainer path={this.props.location}>
        <Breadcrumb path={['coupons','edit']} />
        <PageHeader {...pageHeaderOptions} />

        {this.state.redirect && <Redirect to="/coupons" />}

        <div className="page-container">
          {(fetching) && <Loading />}

          <div className="row justify-content-center">
            <div  className="col-lg-8 col-md-8">
              {(coupon && !this.state.loading) && <EditCouponForm handleSubmit={this.handleSubmit} coupon={coupon} />}
              {(errMsg) && <AlertMsg error={errMsg} />}
            </div>
          </div>

        </div>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    coupon: store.coupons.coupon,
    errMsg: store.coupons.errMsg,
    succMsg: store.coupons.succMsg,
    fetching: store.coupons.fetching,
  }
}

EditCoupon = connect(mapStateToProps)(EditCoupon)
export default EditCoupon
