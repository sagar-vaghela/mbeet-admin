/*
* AddCouponForm: Object
* Child of: AddCoupon
*/

// Main Packages
import React, {Component} from 'react'

// Material UI
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';

class EditCouponForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      disabled: (this.props.coupon.coupons.status === "enable") ? true : false,
      locale: this.props.coupon.coupons.locale,
      status: this.props.coupon.coupons.status,
      form: {},
    }

    this.handleLocaleChange = this.handleLocaleChange.bind(this)
    this.hanldedisabled = this.hanldedisabled.bind(this)
    this.handleChangeField = this.handleChangeField.bind(this)
    this.handleStatus = this.handleStatus.bind(this)
  }

  handleChangeField(event){
    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })
  }

  handleLocaleChange = (event, index, value) => this.setState({locale: value})
  hanldedisabled(event, isInputChecked)
  {
    this.setState({disabled: isInputChecked});
    this.handleStatus();
  }

  handleStatus()
  {
    if (this.state.disabled)
    {
      this.setState({status: "disable"});
    }
    else
    {
      this.setState({status: "enable"});
    }
  }

  render(){
    const {coupon} = this.props;

    return(
      <form onSubmit={this.props.handleSubmit}>

        <div className="form-field">
           <TextField
            required
            floatingLabelText="Value"
            hintText="Name"
            style={{width: '100%'}}
            name="value"
            defaultValue={coupon.coupons.value}
          />
        </div>

        <div className="form-field">
          <DatePicker
              required
              floatingLabelText="Starting Date"
              hintText="Starting Date"
              container="inline"
              name="starting_date"
              mode="landscape"
              defaultDate={new Date(coupon.coupons.starting_date)}
              style={{color: 'red'}} />
        </div>

        <div className="form-field">
          <DatePicker
              required
              floatingLabelText="Expiration Date"
              hintText="Expiration Date"
              container="inline"
              name="expiration_date"
              mode="landscape"
              defaultDate={new Date(coupon.coupons.expiration_date)}
              style={{color: 'red'}} />
        </div>

        <div className="form-field">
            <Toggle
              label="Disabled"
              defaultToggled={this.state.disabled}
              onToggle={this.hanldedisabled}
              />
            <input type="hidden" name="status" value={this.state.status} />
        </div>

        <div className="form-field">
           <TextField
            required
            floatingLabelText="Coupon Code"
            hintText="Coupon Code"
            style={{width: '100%'}}
            name="code"
            defaultValue={coupon.coupons.code}
          />
        </div>

        <div className="form-field submit">
          <RaisedButton label="Update Coupon" type="submit" primary={true} />
        </div>

      </form>
    )
  }

}

export default EditCouponForm;
