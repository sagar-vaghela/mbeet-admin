/*
* IndexCoupons - Component connected to redux
*/

// import main packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

import swal from 'sweetalert'

// Coupons Redux Actions
import {
  getCoupons,
  deleteCoupon,
  couponsSearch,
  applyFilter,
  restoreCoupon} from '../../../../Redux/actions/couponsActions'

import { getCurrentUserPermission } from '../../../../Redux/actions/usersActions'

import {deleteItem} from '../../../../Utilities/functions'

// Table row coupon
import RowCoupon  from './rowCoupon/RowCoupon'
import {DummyRowCoupon} from './rowCoupon/DummyRowCoupon'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  TableList} from '../../../GlobalComponents/GlobalComponents'

// Material UI Icons
import LoyaltyIcon        from 'material-ui/svg-icons/action/loyalty'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

class IndexCoupons extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      comID: 'coupons',
      showList : [
        {count: 10, name: '10'},
        {count: 15, name: '15'},
        {count: 30, name: '30'},
      ],
      // filters: [
      //   {value: 'false', params: 'soft_delete', name: 'All'},
      //   {value: 'true',params: 'soft_delete', name: 'Deleted'},
      //   {value: 'owner',params: 'role', name: 'Owners'},
      //   {value: 'normal', params: 'role', name: 'Normal'},
      //   {value: 'super admin', params: 'role', name: 'Super Admin'},
      //   {value: 'true', params: 'phone_verified', name: 'Phone Verified'},
      //   {value: true, params: 'email_verified', name: 'Email Verified'},
      // ],
      currentFilter: 0,
      currentPage: 1,
      showCoupons: 10,
      sort_by: 'value',
      sort_direction: 'desc',
      search: ' ',

    }
    this.handlePagination   = this.handlePagination.bind(this)
    this.handleShowingCoupons = this.handleShowingCoupons.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    // this.handleFilters = this.handleFilters.bind(this)
    this.handleSorting = this.handleSorting.bind(this)
    this.setSession = this.setSession.bind(this)
    this.getSession = this.getSession.bind(this)
  }

  componentWillMount(){

    this.setSession('currentFilter', this.getSession('currentFilter'))
    this.setSession('currentPage', this.getSession('currentPage'))
    this.setSession('showCoupons', this.getSession('showCoupons'))
    this.setSession('sort_by', this.getSession('sort_by'))
    this.setSession('sort_direction', this.getSession('sort_direction'))
    this.setSession('search', this.getSession('search'))

    // const {filters, currentFilter} = this.state
    const offset = (this.getSession('currentPage') - 1) * this.getSession('showCoupons')

    // On Load the Component get the coupons
    const {dispatch} = this.props
    dispatch(getCoupons(
      this.getSession('showCoupons'),
      offset,
      // filters[this.getSession('currentFilter')],
      this.getSession('sort_by'),
      this.getSession('sort_direction'),
      this.getSession('search')),
    ).then(() => {

      // stop loading if successfull fetched the coupons
      this.setState({loading: false})

    })
    dispatch(getCurrentUserPermission())
  }

  setSession(par, value){

    if(typeof(value) === 'object'){
      value = this.state[par]
    }

    sessionStorage.setItem(this.state.comID+'-'+par,value)
    var obj = {};
    obj[par] = value
    this.setState(obj)
  }

  getSession(par){
    return sessionStorage.getItem(this.state.comID+'-'+par)
  }

  /***************************
  * On Click Delete
  * @couponid : (Intger)
  ****************************/
  handleDelete(couponid){
    const {dispatch, errMsg, succMsg} = this.props
    let _this = this
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: 'Yes, Delete it!',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    () => {
      dispatch(deleteCoupon(couponid)).then(res => {
        swal({
          title: 'Deleted',
          type: 'success',
          text: succMsg,
          timer: 2000,
          showConfirmButton: false
        })
      })
      .catch(err => {
        _this.forceUpdate()
        swal("Error", err.response.data.message, "error");
      })
    })

    //deleteItem(couponid,dispatch, deleteCoupon,this.props.errMsg)
  }

  handleRestoreCoupon = (e, coupon_id) => {
    const {dispatch} = this.props
    e.preventDefault()
    dispatch(restoreCoupon(coupon_id))
    .then(() => {
      swal({
        title: 'Restored',
        type: 'success',
        text: 'Coupon has been restored successfully',
        timer: 2000,
        showConfirmButton: false
      })
    })
    //alert(coupon_id)
  }

  /***************************
  * On Click Pagination
  ****************************/
  handlePagination(e, page){
    e.preventDefault()
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_by, sort_direction, search} = this.state
    const {sort_by, sort_direction, search} = this.state
    // prepair the offset
    const offset = (page - 1) * this.state.showCoupons

    // get coupons
    dispatch(getCoupons(
      this.state.showCoupons,  // limit
      offset, // offset
      // filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
    )).then(() => {
      // change the Pagination number
      this.setState({currentPage: page})
      this.setSession('currentPage', page)
    })

  }

  /***************************
  * Handle Sorting
  ****************************/
  handleSorting(sort_by){
    if(sort_by === 0) return false
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_direction} = this.state
    const {currentFilter, sort_direction} = this.state
    const sort_dir = (sort_direction === 'asc') ? 'desc' : 'asc'

    this.setState({
      sort_by: sort_by,
      sort_direction: sort_dir
    })
    this.setSession('sort_by', sort_by)
    this.setSession('sort_direction', sort_dir)
    this.setSession('currentPage', 1)

    dispatch(getCoupons(
      this.getSession('showCoupons'), // limit
      0, // offset
      // filters[this.getSession('currentFilter')], // filter
      this.getSession('sort_by'), // sort by
      this.getSession('sort_direction'), // sort direction
      this.getSession('search'),
    ))

  }


  /***************************
  * On Change Filter
  ****************************/
  // handleFilters(filter_type, value, index){
  //   const {dispatch} = this.props
  //   const {filters} = this.state
  //   this.setSession('currentFilter', index)
  //   this.setSession('currentPage', 1)
  //   //this.setState({currentFilter: index})
  //   dispatch(getCoupons(
  //     this.state.showCoupons, // limit
  //     0, // offset
  //     filters[index],// filter
  //     this.state.sort_by, //sort by
  //     this.state.sort_direction, // sort direction
  //     this.state.search,
  //   ))
  // }

  /***************************
  * On change showing coupons
  ****************************/
  handleShowingCoupons(count){
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_by, sort_direction, search} = this.state
    const {sort_by, sort_direction, search} = this.state
    dispatch(getCoupons(
      count, // limit
      0, // offset
      // filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
    )).then(() => {
      this.setState({showCoupons: count, currentPage: 1})
      this.setSession('showCoupons', count)
      this.setSession('currentPage', 1)
    })
  }

  /***************************
  * On search
  ****************************/
  handleSearch(e){
    const {dispatch} = this.props
    const length = e.target.value.length
    const s = e.target.value

    // const {filters, currentFilter, sort_by, sort_direction} = this.state
    const {sort_by, sort_direction} = this.state

    if(length >= 3 || length === 0){
      dispatch(getCoupons(
        this.getSession('showCoupons'),
        0,
        // filters[this.getSession('currentFilter')],
        this.getSession('sort_by'),
        this.getSession('sort_direction'),
        s
      ))

      this.setSession('search', s)
      this.setSession('currentPage', 1)
    }

  }

  // render
  render(){

    // Table columns
    const columns = [
      {params: 'id', title: 'ID'},
      {params: 'value', title: 'Value'},
      {params: 'starting_date', title: 'Starting Date'},
      {params: 'expiration_date', title: 'Expiration Date'},
      {params: 'used_coupon_count', title: 'Used Coupon Count'},
      {params: 'total_charged_amount', title: 'Total Charged Amount'},
      {params: 'total_discounted_amount', title: 'Total Discounted Amount'},
      {params: 'average_charged_amount', title: 'Average Charged Amount'},
      {params: 'average_discount_amount', title: 'Average Discount Amount'},
      {params: 'status', title: 'Status'},
      {params: 'code', title: 'Coupon Code'},
      {params: 0, title: 'Action'},
    ]
    // store data
    const {coupons, fetching, errMsg, totalCoupons, current_user_permissions} = this.props

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Coupons List',
      icon: (<LoyaltyIcon className="pagetitle-icon"/>),
      addbtn: current_user_permissions.coupon_permissions.create? "/coupons/add" : ''
    }

    // TableList Options
    const tableListOptions = {
      columns: columns,
      loading: this.state.loading,
      update: fetching,
      onPageChange: this.handlePagination,
      total: totalCoupons,
      rowShowing: parseInt(this.getSession('showCoupons')),
      onChangeShowing: this.handleShowingCoupons,
      page: this.getSession('currentPage'),
      showList: this.state.showList,
      onSearch: this.handleSearch,
      search: this.getSession('search'),
      onError: errMsg,
      // filters: this.state.filters,
      // onChangeFilter: this.handleFilters,
      currentFilter: this.getSession('currentFilter'),
      onSorting: this.handleSorting
    }

    return(
      <PagesContainer path={this.props.location}>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['coupons']} />
          <PageHeader {...pageHeaderOptions} />
          <TableList {...tableListOptions}>

            { // if fetched coupons
              // !this.state.loading && coupons == null ?
              !this.state.loading ?
              (
                coupons.map(coupon => {
                  return (<RowCoupon
                              key={'coupon'+coupon.id}
                              coupon={coupon}
                              onDelete={this.handleDelete}
                              restoreCoupon={this.handleRestoreCoupon}
                              permissions={current_user_permissions}
                              />)
                })
              )
              :
              (
                DummyRowCoupon()
              )
            /* end if */ }

          </TableList>
        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    coupons: store.coupons.coupons,
    current_user_permissions: store.users.current_user_permissions,
    totalCoupons: store.coupons.totalCoupons,
    errMsg: store.coupons.errMsg,
    fetching: store.coupons.fetching,
  }
}

IndexCoupons = connect(mapStateToProps)(IndexCoupons)
export default IndexCoupons
