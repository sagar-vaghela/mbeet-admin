// Main Packages
import React from 'react';
import { Link }           from 'react-router-dom'

// Material UI
import MenuItem from 'material-ui/MenuItem';
import IconMenu     from 'material-ui/IconMenu';
import IconButton   from 'material-ui/IconButton';

// Material UI Icons
import DeleteIcon         from 'material-ui/svg-icons/action/delete';
import ModeEditIcon       from 'material-ui/svg-icons/editor/mode-edit';
import MoreVertIcon       from 'material-ui/svg-icons/navigation/more-vert';
import dateFormat         from 'dateformat';

// Style
import './rowCoupon.css'

import { CSSTransitionGroup } from 'react-transition-group'

const RowCoupon = (props) => {
  const {coupon, onDelete, permissions} = props
  return (
    <CSSTransitionGroup
      component="tr"
transitionName="example"
transitionAppear={true}
transitionAppearTimeout={5000}
transitionEnter={false}
transitionLeave={false}>
      {/* ID */}
      <td>{coupon.id}</td>

      <td>{coupon.value}</td>

      <td>{dateFormat(new Date(coupon.starting_date),'ddd, d mmm yyyy h:MM TT')}</td>

      <td>{dateFormat(new Date(coupon.expiration_date),'ddd, d mmm yyyy h:MM TT')}</td>

      <td>{coupon.used_coupon_count}</td>

      <td>{coupon.total_charged_amount}</td>

      <td>{coupon.total_discounted_amount}</td>

      <td>{coupon.average_charged_amount ? coupon.average_charged_amount : "0.0"}</td>

      <td>{coupon.average_discounted_amount ? coupon.average_discounted_amount : "0.0"}</td>

      <td>{coupon.status}</td>

      <td className="remove-capitalization">{coupon.code}</td>


      {/* Action */}
      <td>
        <IconMenu
           iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
           anchorOrigin={{horizontal: 'left', vertical: 'top'}}
           targetOrigin={{horizontal: 'left', vertical: 'top'}}
        >

          {permissions.coupon_permissions.edit &&
            <Link to={'/coupons/edit/'+coupon.id} className="MenuItemBtn">
              <MenuItem primaryText="Edit" leftIcon={<ModeEditIcon />} />
            </Link>
          }

          {permissions.coupon_permissions.delete &&
            <MenuItem primaryText="Delete" leftIcon={<DeleteIcon />} onClick={() => onDelete(coupon.id)} />
          }
        </IconMenu>
      </td>
    </CSSTransitionGroup>
  )
}
export default RowCoupon
