import React, {Component} from 'react'
import {connect} from 'react-redux'
import {
  getRegisteredUsers,
  getTotalBookings
} from '../../../Redux/actions/dashboardActions'
import moment from 'moment'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading} from '../../GlobalComponents/GlobalComponents'

import People from 'material-ui/svg-icons/social/people'

import TotalRegisteredUsers from './TotalRegisteredUsers'
import TotalBookings from './TotalBookings'

import './dashboard.css'
// Components
import { CSSTransitionGroup } from 'react-transition-group'

class Dashboard extends Component{

  componentWillMount(){
    const {dispatch} = this.props

    // registerd users
    dispatch(getRegisteredUsers())
    dispatch(getRegisteredUsers(moment().format('YYYY-MM-DD')))

    // total bookings
    dispatch(getTotalBookings())
    dispatch(getTotalBookings(moment().format('YYYY-MM-DD'), 1))

  }

  render(){
    console.log(sessionStorage.getItem('TOKEN'));
    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Dashboard',
      icon: (<People className="pagetitle-icon"/>),
    }

    const {total_users, today_users, total_bookings, today_bookings} = this.props

    return (
      <PagesContainer path={this.props.location}>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <div className="dashboard-container">
            <div className="row">
              <div className="col-lg-6 col-md-6">
                <div className="widget-container">
                  {(!total_users.data || total_users.errMsg) &&
                    <div className="loading-container">
                      <Loading error={total_users.errMsg} />
                    </div>
                  }

                  {total_users.data &&
                    <TotalRegisteredUsers
                      title="total REGISTERED users"
                      active={total_users.data.active}
                      deleted={total_users.data.deleted}
                    />
                  }
                </div>
              </div>

              <div className="col-lg-6 col-md-6">
                <div className="widget-container">
                  {(!total_bookings.data || total_bookings.errMsg) &&
                    <div className="loading-container">
                      <Loading error={total_bookings.errMsg} />
                    </div>
                  }

                  {total_bookings.data &&
                    <TotalBookings
                      title="This month bookings"
                      pending={total_bookings.data.pending}
                      cancelled={total_bookings.data.cancelled}
                      confirmed={total_bookings.data.confirmed}
                      completed={total_bookings.data.completed}
                    />
                  }
                </div>
              </div>
            </div>

            <div className="row space-top-row">
              <div className="col-lg-6 col-md-6 ">
                <div className="widget-container">
                  {(!today_users.data || today_users.errMsg) &&
                    <div className="loading-container">
                      <Loading error={today_users.errMsg} />
                    </div>
                  }

                  {today_users.data &&
                    <TotalRegisteredUsers
                      title="today REGISTERED users"
                      active={today_users.data.active}
                      deleted={today_users.data.deleted}
                    />
                  }
                </div>
              </div>

              <div className="col-lg-6 col-md-6">
                <div className="widget-container">
                  {(!today_bookings.data || today_bookings.errMsg) &&
                    <div className="loading-container">
                      <Loading error={today_bookings.errMsg} />
                    </div>
                  }

                  {today_bookings.data &&
                    <TotalBookings
                      title="Today BOOKINGS"
                      pending={today_bookings.data.pending}
                      cancelled={today_bookings.data.cancelled}
                      confirmed={today_bookings.data.confirmed}
                      completed={today_bookings.data.completed}
                    />
                  }
                </div>
              </div>
            </div>
          </div>
        </CSSTransitionGroup>
      </PagesContainer>

    )
  }
}

const mapStateToProps = (state) => {
  return {
    total_users: state.dashboard.total_users,
    today_users: state.dashboard.today_users,
    total_bookings: state.dashboard.total_bookings,
    today_bookings: state.dashboard.today_bookings,
  }
}
Dashboard = connect(mapStateToProps)(Dashboard)
export default Dashboard
