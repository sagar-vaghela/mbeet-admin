import React from 'react'

const TotalRegisteredUsers = ({title, active, deleted}) => (
  <div className="dashboard-widget">
    <div className="middle-box">
      <span className="title">{title}</span>
      <span className="big-number">{deleted + active}</span>
    </div>

    <div className="row space-top">
      <div className="col-lg-6 col-md-6 col-sm-6 center">
        <span className="title-md">active</span>
        <span className="number-md yellow">{active}</span>
      </div>

      <div className="col-lg-6 col-md-6 col-sm-6 center">
        <span className="title-md">deleted</span>
        <span className="number-md red">{deleted}</span>
      </div>
    </div>
  </div>
)
export default TotalRegisteredUsers
