import React, {Component} from 'react'
import {connect} from 'react-redux'

// Users Redux Actions
import {getLoginAttempts, loginAttemptsSearch} from '../../../../Redux/actions/loginAttemptsActions'

import { getCurrentUserPermission } from '../../../../Redux/actions/usersActions'

import swal from 'sweetalert'

import RowLoginAttempt from './rowLoginAttempt/RowLoginAttempt'
import {DummyLoginAttemptRow} from './rowLoginAttempt/DummyLoginAttemptRow'
// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  TableList} from '../../../GlobalComponents/GlobalComponents'

import PlaceIcon          from 'material-ui/svg-icons/maps/place'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

class IndexLoginAttempts extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,

      showList : [
        {count: 10, name: '10'},
        {count: 15, name: '15'},
        {count: 30, name: '30'},
      ],
      comID: 'loginAttempts',
      currentFilter: 0,
      currentPage: 1,
      show: 10,
      sort_by: 'id',
      sort_direction: 'asc',
      search: '',
    }
    this.handlePagination   = this.handlePagination.bind(this)
    this.handleShowing = this.handleShowing.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    this.handleSorting = this.handleSorting.bind(this)
    this.setSession = this.setSession.bind(this)
    this.getSession = this.getSession.bind(this)
  }

  setSession(par, value){

    if(typeof(value) === 'object'){
      value = this.state[par]
    }

    sessionStorage.setItem(this.state.comID+'-'+par,value)
    var obj = {};
    obj[par] = value
    this.setState(obj)
  }

  getSession(par){
    return sessionStorage.getItem(this.state.comID+'-'+par)
  }

  componentWillMount(){

    this.setSession('currentFilter', this.getSession('currentFilter'))
    this.setSession('currentPage', this.getSession('currentPage'))
    this.setSession('show', this.getSession('show'))
    this.setSession('sort_by', this.getSession('sort_by'))
    this.setSession('sort_direction', this.getSession('sort_direction'))
    this.setSession('search', this.getSession('search'))

    const offset = (this.getSession('currentPage') - 1) * this.getSession('show')

    // On Load the Component get the users
    const {dispatch} = this.props
    dispatch(getLoginAttempts(
      this.getSession('show'),
      offset,
      '',
      this.getSession('sort_by'),
      this.getSession('sort_direction'),
      this.getSession('search'),
    )).then(() => {

      // stop loading if successfull fetched the users
      this.setState({loading: false})
    })
    dispatch(getCurrentUserPermission())
  }

  /***************************
  * On search
  ****************************/
  handleSearch(e){
    const {dispatch} = this.props
    const length = e.target.value.length
    const s = e.target.value

    if(length >= 0 || length === 0)
      dispatch(getLoginAttempts(
        this.getSession('show'),
        0,
        '',
        this.getSession('sort_by'),
        this.getSession('sort_direction'),
        s,
      ))

      this.setSession('search', s)
      this.setSession('currentPage', 1)
  }

  /***************************
  * Handle Sorting
  ****************************/
  handleSorting(sort_by){
    if(sort_by === 0) return false
    const {dispatch} = this.props
    const {currentFilter, sort_direction} = this.state
    const sort_dir = (sort_direction === 'asc') ? 'desc' : 'asc'

    this.setState({
      sort_by: sort_by,
      sort_direction: sort_dir
    })

    this.setSession('sort_by', sort_by)
    this.setSession('sort_direction', sort_dir)
    this.setSession('currentPage', 1)

    dispatch(getLoginAttempts(
      this.getSession('show'), // limit
      0, // offset
      '', // filter
      this.getSession('sort_by'), // sort by
      this.getSession('sort_direction'), // sort direction
      this.getSession('search'),
    ))

  }

  /***************************
  * On Click Pagination
  ****************************/
  handlePagination(e, page){
    e.preventDefault()
    const {dispatch} = this.props
    const {currentFilter, sort_by, sort_direction, search} = this.state
    // prepair the offset
    const offset = (page - 1) * this.state.show

    // get users
    dispatch(getLoginAttempts(
      this.state.show,  // limit
      offset, // offset
      '', // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
    )).then(() => {
      // change the Pagination number
      this.setState({currentPage: page})
      this.setSession('currentPage', page)
    })

  }

  /***************************
  * On change showing
  ****************************/
  handleShowing(count){
    const {dispatch} = this.props
    const {currentFilter, sort_by, sort_direction, search} = this.state
    dispatch(getLoginAttempts(
      count, // limit
      0, // offset
      '', // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
    )).then(() => {
      this.setState({show: count, currentPage: 1})
      this.setSession('show', count)
      this.setSession('currentPage', 1)
    })
  }

  render(){

    // Table columns
    const columns = [
      {params: 'id', title: 'ID'},
      {params: 'user', title: 'User'},
      {params: 'role', title: 'Role'},
      {params: 'success', title: 'Success'},
      {params: 'ip', title: 'IP'},
      {params: 'user_agent', title: 'User-Agent'},
      {params: 'os', title: 'OS'}
    ]

    // Store props
    const {loginAttempts, total, errMsg, fetching, current_user_permissions} = this.props

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'LoginAttempts List',
      icon: (<PlaceIcon className="pagetitle-icon"/>),
    }

    // TableList Options
    const tableListOptions = {
      columns: columns,
      loading: this.state.loading,
      update: fetching,
      onPageChange: this.handlePagination,
      total: total,
      rowShowing: parseInt(this.getSession('show')),
      onChangeShowing: this.handleShowing,
      page: this.getSession('currentPage'),
      showList: this.state.showList,
      onSearch: this.handleSearch,
      search: this.getSession('search'),
      onError: errMsg,
      onSorting: this.handleSorting,
    }

    return(
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['loginAttempts']} />
          <PageHeader {...pageHeaderOptions} />

          <TableList {...tableListOptions}>

            { // if fetched users
              !this.state.loading ?
              (
                loginAttempts.map(loginAttempt => {
                  return (<RowLoginAttempt key={'loginAttempt'+loginAttempt.id} loginAttempt={loginAttempt} permissions={current_user_permissions} />)
                })
              )
              :
              (
                DummyLoginAttemptRow()
              )
            /* end if */ }

          </TableList>

        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    loginAttempts: store.loginAttempts.loginAttempts,
    current_user_permissions: store.users.current_user_permissions,
    total: store.loginAttempts.total,
    errMsg: store.loginAttempts.errMsg,
    fetching: store.loginAttempts.fetching,
  }
}

IndexLoginAttempts = connect(mapStateToProps)(IndexLoginAttempts)
export default IndexLoginAttempts
