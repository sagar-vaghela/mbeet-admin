import React from 'react'
import { Link }           from 'react-router-dom'

import dateFormat  from 'dateformat'

// Material UI
import DoneIcon           from 'material-ui/svg-icons/action/done';
import ClearIcon          from 'material-ui/svg-icons/content/clear';
import MenuItem from 'material-ui/MenuItem';
import IconMenu     from 'material-ui/IconMenu';
import IconButton   from 'material-ui/IconButton';
import MoreVertIcon       from 'material-ui/svg-icons/navigation/more-vert';
import DeleteIcon         from 'material-ui/svg-icons/action/delete';
import DeleteForeverIcon  from 'material-ui/svg-icons/action/delete-forever';
import ModeEditIcon       from 'material-ui/svg-icons/editor/mode-edit';
import RemoveRedEyeIcon   from 'material-ui/svg-icons/image/remove-red-eye';


const RowBooking = (props) => {
  const {loginAttempt, onDelete, permissions} = props
  return (
    <tr className={"alert " + (loginAttempt.success ? 'alert-success' : 'alert-danger')}>
      <td>{loginAttempt.id}</td>
      <td>{loginAttempt.user.name}</td>
      <td>{loginAttempt.user.role}</td>
      <td>
      {loginAttempt.success ? (
        <DoneIcon className="done-icon" />
      ):(
        <ClearIcon className="clear-icon" />
      )}
      </td>
      <td>{loginAttempt.ip}</td>
      <td>{loginAttempt.user_agent}</td>
      <td>{loginAttempt.os}</td>
    </tr>
  )
}

export default RowBooking
