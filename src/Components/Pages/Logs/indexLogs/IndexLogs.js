/*
* IndexLogs - Component connected to redux
*/

// import main packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

import swal from 'sweetalert'

// Logs Redux Actions
import {
  getLogs,
  deleteLog,
  LogsSearch,
  applyFilter } from '../../../../Redux/actions/logsActions'

import { getCurrentUserPermission } from '../../../../Redux/actions/usersActions'

import {deleteItem} from '../../../../Utilities/functions'

// Table row Log
import RowLog  from './rowLog/RowLog'
import {DummyRowLog} from './rowLog/DummyRowLog'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  LogsTableList} from '../../../GlobalComponents/GlobalComponents'

// Material UI Icons
import LogIcon          from 'material-ui/svg-icons/action/track-changes'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

class IndexLogs extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      comID: 'Logs',
      showList : [
        {count: 10, name: '10'},
        {count: 15, name: '15'},
        {count: 30, name: '30'},
      ],
      // filters: [
      //   {value: 'false', params: 'soft_delete', name: 'All'},
      //   {value: 'true',params: 'soft_delete', name: 'Deleted'},
      //   {value: 'owner',params: 'log', name: 'Owners'},
      //   {value: 'normal', params: 'log', name: 'Normal'},
      //   {value: 'super admin', params: 'log', name: 'Super Admin'},
      //   {value: 'true', params: 'phone_verified', name: 'Phone Verified'},
      //   {value: true, params: 'email_verified', name: 'Email Verified'},
      // ],
      currentFilter: 0,
      currentPage: 1,
      showLogs: 10,
      sort_by: 'name',
      sort_direction: 'desc',
      search: ' ',

    }
    this.handlePagination   = this.handlePagination.bind(this)
    this.handleShowingLogs = this.handleShowingLogs.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    // this.handleFilters = this.handleFilters.bind(this)
    this.handleSorting = this.handleSorting.bind(this)
    this.setSession = this.setSession.bind(this)
    this.getSession = this.getSession.bind(this)
  }

  componentWillMount(){

    this.setSession('currentFilter', this.getSession('currentFilter'))
    this.setSession('currentPage', this.getSession('currentPage'))
    this.setSession('showLogs', this.getSession('showLogs'))
    this.setSession('sort_by', this.getSession('sort_by'))
    this.setSession('sort_direction', this.getSession('sort_direction'))
    this.setSession('search', this.getSession('search'))

    // const {filters, currentFilter} = this.state
    const offset = (this.getSession('currentPage') - 1) * this.getSession('showLogs')

    // On Load the Component get the Logs
    const {dispatch} = this.props
    dispatch(getLogs(
      this.getSession('showLogs'),
      offset,
      // filters[this.getSession('currentFilter')],
      this.getSession('sort_by'),
      this.getSession('sort_direction'),
      this.getSession('search')),
    ).then(() => {

      // stop loading if successfull fetched the Logs
      this.setState({loading: false})

    })
    dispatch(getCurrentUserPermission())
  }

  setSession(par, value){

    if(typeof(value) === 'object'){
      value = this.state[par]
    }

    sessionStorage.setItem(this.state.comID+'-'+par,value)
    var obj = {};
    obj[par] = value
    this.setState(obj)
  }

  getSession(par){
    return sessionStorage.getItem(this.state.comID+'-'+par)
  }

  /***************************
  * On Click Delete
  * @Logid : (Intger)
  ****************************/
  handleDelete(Logid){
    const {dispatch, errMsg, succMsg} = this.props
    let _this = this
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: 'Yes, Delete it!',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    () => {
      dispatch(deleteLog(Logid)).then(res => {
        swal({
          title: 'Deleted',
          type: 'success',
          text: succMsg,
          timer: 2000,
          showConfirmButton: false
        })
      })
      .catch(err => {
        _this.forceUpdate()
        swal("Error", err.response.data.message, "error");
      })
    })

    //deleteItem(Logid,dispatch, deleteLog,this.props.errMsg)
  }

  /***************************
  * On Click Pagination
  ****************************/
  handlePagination(e, page){
    e.preventDefault()
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_by, sort_direction, search} = this.state
    const {sort_by, sort_direction, search} = this.state
    // prepair the offset
    const offset = (page - 1) * this.state.showLogs

    // get Logs
    dispatch(getLogs(
      this.state.showLogs,  // limit
      offset, // offset
      // filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
    )).then(() => {
      // change the Pagination number
      this.setState({currentPage: page})
      this.setSession('currentPage', page)
    })

  }

  /***************************
  * Handle Sorting
  ****************************/
  handleSorting(sort_by){
    if(sort_by === 0) return false
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_direction} = this.state
    const {currentFilter, sort_direction} = this.state
    const sort_dir = (sort_direction === 'asc') ? 'desc' : 'asc'

    this.setState({
      sort_by: sort_by,
      sort_direction: sort_dir
    })
    this.setSession('sort_by', sort_by)
    this.setSession('sort_direction', sort_dir)
    this.setSession('currentPage', 1)

    dispatch(getLogs(
      this.getSession('showLogs'), // limit
      0, // offset
      // filters[this.getSession('currentFilter')], // filter
      this.getSession('sort_by'), // sort by
      this.getSession('sort_direction'), // sort direction
      this.getSession('search'),
    ))

  }


  /***************************
  * On Change Filter
  ****************************/
  // handleFilters(filter_type, value, index){
  //   const {dispatch} = this.props
  //   const {filters} = this.state
  //   this.setSession('currentFilter', index)
  //   this.setSession('currentPage', 1)
  //   //this.setState({currentFilter: index})
  //   dispatch(getLogs(
  //     this.state.showLogs, // limit
  //     0, // offset
  //     filters[index],// filter
  //     this.state.sort_by, //sort by
  //     this.state.sort_direction, // sort direction
  //     this.state.search,
  //   ))
  // }

  /***************************
  * On change showing Logs
  ****************************/
  handleShowingLogs(count){
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_by, sort_direction, search} = this.state
    const {sort_by, sort_direction, search} = this.state
    dispatch(getLogs(
      count, // limit
      0, // offset
      // filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
    )).then(() => {
      this.setState({showLogs: count, currentPage: 1})
      this.setSession('showLogs', count)
      this.setSession('currentPage', 1)
    })
  }

  /***************************
  * On search
  ****************************/
  handleSearch(e){
    const {dispatch} = this.props
    const length = e.target.value.length
    const s = e.target.value

    // const {filters, currentFilter, sort_by, sort_direction} = this.state
    const {sort_by, sort_direction} = this.state

    if(length >= 3 || length === 0){
      dispatch(getLogs(
        this.getSession('showLogs'),
        0,
        // filters[this.getSession('currentFilter')],
        this.getSession('sort_by'),
        this.getSession('sort_direction'),
        s
      ))

      this.setSession('search', s)
      this.setSession('currentPage', 1)
    }

  }

  // render
  render(){

    // Table columns
    const columns = [
      {params: 0 , title: 'ID'},
      {params: 0 , title: 'User Name'},
      {params: 0 , title: 'Role'},
      {params: 0 , title: 'Date & Time'},
      {params: 0 , title: 'Site Url'},
      {params: 0 , title: 'Request Path'},
      {params: 0 , title: 'Controller'},
      {params: 0 , title: 'Action'},
    ]
    // store data
    const {Logs, fetching, errMsg, total, current_user_permissions} = this.props

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Logs List',
      icon: (<LogIcon className="pagetitle-icon"/>),
    }

    // LogsTableList Options
    const tableListOptions = {
      columns: columns,
      loading: this.state.loading,
      update: fetching,
      onPageChange: this.handlePagination,
      total: total,
      rowShowing: parseInt(this.getSession('showLogs')),
      onChangeShowing: this.handleShowingLogs,
      page: this.getSession('currentPage'),
      showList: this.state.showList,
      // onSearch: this.handleSearch,
      // search: this.getSession('search'),
      onError: errMsg,
      // filters: this.state.filters,
      // onChangeFilter: this.handleFilters,
      currentFilter: this.getSession('currentFilter'),
      onSorting: this.handleSorting
    }


    return(
      <PagesContainer path={this.props.location}>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['logs']} />
            <PageHeader {...pageHeaderOptions} />
            <LogsTableList {...tableListOptions}>

            { // if fetched Logs
              // !this.state.loading && Logs == null ?
              !this.state.loading ?
              (
                Logs.map(Log => {
                  return (<RowLog
                              key={'Log'+Log.id}
                              Log={Log}
                              onDelete={this.handleDelete}
                              permissions={current_user_permissions}
                              />)
                })
              )
              :
              (
                DummyRowLog()
              )
            /* end if */ }

          </LogsTableList>
        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    Logs: store.logs.logs,
    current_user_permissions: store.users.current_user_permissions,
    total: store.logs.total,
    errMsg: store.logs.errMsg,
    fetching: store.logs.fetching,
  }
}

IndexLogs = connect(mapStateToProps)(IndexLogs)
export default IndexLogs
