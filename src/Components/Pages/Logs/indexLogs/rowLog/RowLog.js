// Main Packages
import React from 'react';
import { Link }           from 'react-router-dom'

// Material UI
import MenuItem from 'material-ui/MenuItem';
import IconMenu     from 'material-ui/IconMenu';
import IconButton   from 'material-ui/IconButton';

// Material UI Icons
import DeleteIcon         from 'material-ui/svg-icons/action/delete';
import ModeEditIcon       from 'material-ui/svg-icons/editor/mode-edit';
import MoreVertIcon       from 'material-ui/svg-icons/navigation/more-vert';
import dateFormat         from 'dateformat';
import RemoveRedEyeIcon   from 'material-ui/svg-icons/image/remove-red-eye';

// Style
import './rowLog.css'

import { CSSTransitionGroup } from 'react-transition-group'

const RowLog = (props) => {
  const {Log, onDelete, permissions} = props
  return (
    <CSSTransitionGroup
      component="tr"
transitionName="example"
transitionAppear={true}
transitionAppearTimeout={5000}
transitionEnter={false}
transitionLeave={false}>

      <td>{Log.id}</td>

      <td>{Log.user_name}</td>

      <td>{Log.role}</td>

      <td>{dateFormat(new Date(Log.date_time),'ddd, d mmm yyyy h:MM TT')}</td>

      <td>{Log.site_url}</td>

      <td>{Log.request_path}</td>

      <td>{Log.controller_name}</td>

      <td>{Log.action_name}</td>

    </CSSTransitionGroup>
  )
}
export default RowLog
