import React,{Component} from 'react'
import {connect} from 'react-redux'
import { Redirect } from 'react-router'

import AddRoleForm from './AddRoleForm'

// Roles Redux Actions
import {addRole} from '../../../../Redux/actions/rolesActions'
import swal from 'sweetalert'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

// Material UI Icons
import LoyaltyIcon        from 'material-ui/svg-icons/action/loyalty'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  AlertMsg,
  Loading} from '../../../GlobalComponents/GlobalComponents'

class AddRole extends Component{

  constructor(props){
    super(props)
    this.state = {
      redirect: false
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(e){
    e.preventDefault()
    const {dispatch, succMsg} = this.props
    var formData = new FormData(e.target)
    let obj = e.target
    dispatch(addRole(formData)).then(() => {
      const _this = this
      obj.reset()
      swal({
        title: "Role added successfully",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Back to Roles list",
        closeOnConfirm: true,
      },
      function(){
        _this.setState({redirect: true})
      });
    })

  }

  render(){

    // Store props
    const {errMsg, succMsg, fetching} = this.props

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Add New Role',
      icon: (<LoyaltyIcon className="pagetitle-icon"/>),
    }
    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }
    return(
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['roles','add Role']} />
          <PageHeader {...pageHeaderOptions} />

          <div className="page-container">

            {fetching && <Loading  />}
            {this.state.redirect && <Redirect to="/roles" />}

            <div className="row justify-content-center">
              <div  className="col-lg-8 col-md-8" style={{position: 'inherit'}}>
                <AddRoleForm handleSubmit={this.handleSubmit} />
              </div>
            </div>

            {/* after dispatch, display the error or success message */}
            {(errMsg) && <AlertMsg error={errMsg} />}

          </div>

        </CSSTransitionGroup>
      </PagesContainer>
    )
  }

}

const mapStateToProps = (store) => {
  return {
    errMsg: store.roles.errMsg,
    fetching: store.roles.fetching,
    succMsg: store.roles.succMsg,
  }
}

AddRole = connect(mapStateToProps)(AddRole)
export default AddRole
