import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Redirect } from 'react-router'
import swal from 'sweetalert'
// Roles Redux Actions
import {getRole, editRole} from '../../../../Redux/actions/rolesActions'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading,
  AlertMsg} from '../../../GlobalComponents/GlobalComponents'

import EditRoleForm from './EditRoleForm'

// Material UI Icons
import LoyaltyIcon        from 'material-ui/svg-icons/action/loyalty'

class EditRole extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      redirect: false,
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount(){

    const {dispatch, match} = this.props
    const roleid = match.params.id

    dispatch(getRole(roleid)).then(() => this.setState({loading: false}))
  }

  handleSubmit(e){
    e.preventDefault()
    const {dispatch, match, succMsg} = this.props
    const roleid = match.params.id
    var formData = new FormData(e.target)
    let obj = e.target
    dispatch(editRole(roleid, formData)).then(() => {
      const _this = this
      obj.reset()
      swal({
        title: "Role updated successfully",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Back to roles list",
        closeOnConfirm: true,
      },
      function(){
        _this.setState({redirect: true})
      });
    })

  }

  // render
  render(){

    // Store props
    const {role, errMsg, succMsg, fetching} = this.props

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Edit Role',
      icon: (<LoyaltyIcon className="pagetitle-icon"/>),
    }

    return(
      <PagesContainer path={this.props.location}>
        <Breadcrumb path={['roles','edit']} />
        <PageHeader {...pageHeaderOptions} />

        {this.state.redirect && <Redirect to="/roles" />}

        <div className="page-container">
          {(fetching) && <Loading />}

          <div className="row justify-content-center">
            <div  className="col-lg-8 col-md-8">
              {(role && !this.state.loading) && <EditRoleForm handleSubmit={this.handleSubmit} role={role} />}
              {(errMsg) && <AlertMsg error={errMsg} />}
            </div>
          </div>

        </div>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    role: store.roles.role,
    errMsg: store.roles.errMsg,
    succMsg: store.roles.succMsg,
    fetching: store.roles.fetching,
  }
}

EditRole = connect(mapStateToProps)(EditRole)
export default EditRole
