/*
* AddRoleForm: Object
* Child of: AddRole
*/

// Main Packages
import React, {Component} from 'react'

// Material UI
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';
import { ValidatorForm, TextValidator, SelectValidator, DateValidator} from 'react-material-ui-form-validator'

import Checkbox from 'material-ui/Checkbox';

// External Packages
import {Loading} from '../../../GlobalComponents/GlobalComponents'

import './editRole.css'
// External Packages
import $ from 'jquery'


class EditRoleForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      form: {},
      name: this.props.role.roles.name,
      dashboardPermissions: this.props.role.roles.role_permission && this.props.role.roles.role_permission.permitted_actions.dashboard_permissions,
      usersPermissions: this.props.role.roles.role_permission && this.props.role.roles.role_permission.permitted_actions.users_permissions,
      unitPermissions: this.props.role.roles.role_permission && this.props.role.roles.role_permission.permitted_actions.unit_permissions,
      bookingPermissions: this.props.role.roles.role_permission && this.props.role.roles.role_permission.permitted_actions.booking_permissions,
      specialPricePermissions: this.props.role.roles.role_permission && this.props.role.roles.role_permission.permitted_actions.special_price_permissions,
      couponPermissions: this.props.role.roles.role_permission && this.props.role.roles.role_permission.permitted_actions.coupon_permissions,
      cityPermissions: this.props.role.roles.role_permission && this.props.role.roles.role_permission.permitted_actions.city_permissions,
      rolePermissions: this.props.role.roles.role_permission && this.props.role.roles.role_permission.permitted_actions.role_permissions,
      topDestinationPermissions: this.props.role.roles.role_permission && this.props.role.roles.role_permission.permitted_actions.top_destination_permissions,

      dashboard_permissions: [{ id : 0 , name : "View" , checked :false , value: 'view'}],

      users_permissions: [{ id : 0 , name : "View" , checked : false , value: 'view'},{ id : 1 , name : "Create" , checked : false , value: 'create'},{ id:2 , name: "Edit" , checked : false , value: 'edit'},{ id : 3 , name : "Delete" , checked : false , value: 'delete'},{ id : 4 , name : "Show" , checked : false , value: 'show'},{ id : 5 , name : "Restore" , checked : false , value: 'restore'}],

      booking_permissions: [{ id : 0 , name : "View" , checked : false , value: 'view'},{ id : 1 , name : "Create" , checked : false , value: 'create'},{ id:2 , name: "Edit" , checked : false , value: 'edit'},{ id : 3 , name : "Delete" , checked : false , value: 'delete'},{ id : 4 , name : "Cancel" , checked : false , value: 'cancel'},{ id : 5 , name : "Show" , checked : false , value: 'show'}],

      unit_permissions: [{ id : 0 , name : "View" , checked : false , value: 'view'},{ id : 1 , name : "Create" , checked : false , value: 'create'},{ id:2 , name: "Edit" , checked : false , value: 'edit'},{ id : 3 , name : "Show" , checked : false , value: 'show'},{ id : 4 , name : "Trash" , checked : false , value: 'trash'},{ id : 5 , name : "Special Price" , checked : false , value: 'special_price'},{ id : 6 , name : "Restore" , checked : false , value: 'restore'},{ id : 7 , name : "Disable Sub-Unit" , checked : false , value: 'disable'},{ id : 8 , name : "Enable Sub-Unit" , checked : false , value: 'enable'},{ id : 9 , name : "Publish" , checked : false , value: 'publish'}],

      special_price_permissions: [{ id : 0 , name : "View" , checked : false , value: 'view'},{ id : 1 , name : "Create" , checked : false , value: 'create'},{ id:2 , name: "Edit" , checked : false , value: 'edit'},{ id : 3 , name : "Delete" , checked : false , value: 'delete'}],

      coupon_permissions: [{ id : 0 , name : "View" , checked : false , value: 'view'},{ id : 1 , name : "Create" , checked : false , value: 'create'},{ id:2 , name: "Edit" , checked : false , value: 'edit'},{ id : 3 , name : "Delete" , checked : false , value: 'delete'}],

      city_permissions: [{ id : 0 , name : "View" , checked : false , value: 'view'},{ id : 1 , name : "Create" , checked : false , value: 'create'},{ id:2 , name: "Edit" , checked : false , value: 'edit'},{ id : 3 , name : "Delete" , checked : false , value: 'delete'}],

      role_permissions: [{ id : 0 , name : "View" , checked : false , value: 'view'},{ id : 1 , name : "Create" , checked : false , value: 'create'},{ id : 2 , name : "Edit" , checked : false , value: 'edit'},{ id : 3 , name : "Show" , checked : false , value: 'show'}],

      topDestination_permissions: [{ id : 0 , name : "View" , checked : false , value: 'view'},{ id : 1 , name : "Create" , checked : false , value: 'create'},{ id:2 , name: "Edit" , checked : false , value: 'edit'},{ id : 3 , name : "Delete" , checked : false , value: 'delete'}],

      selected_dashboard_permissions: [],
      selected_users_permissions: [],
      selected_booking_permissions: [],
      selected_unit_permissions: [],
      selected_special_price_permissions: [],
      selected_coupon_permissions: [],
      selected_city_permissions: [],
      selected_role_permissions: [],
      selected_topDestination_permissions: [],


    }
    this.handleDashboard_permissions = this.handleDashboard_permissions.bind(this);
    this.handleUsers_permissions = this.handleUsers_permissions.bind(this);
    this.handleBooking_permissions = this.handleBooking_permissions.bind(this);
    this.handleUnit_permissions = this.handleUnit_permissions.bind(this);
    this.handleSpecialPrice_permissions = this.handleSpecialPrice_permissions.bind(this);
    this.handleCoupon_permissions = this.handleCoupon_permissions.bind(this);
    this.handleCity_permissions = this.handleCity_permissions.bind(this);
    this.handleRole_permissions = this.handleRole_permissions.bind(this);
    this.handleTopDestination_permissions = this.handleTopDestination_permissions.bind(this);
    this.handleChangeField = this.handleChangeField.bind(this);
  }

  componentWillMount(){




    //name
    const { form } = this.state;
    form['name'] = this.state.name;
    this.setState({ form });

    console.log(this.state.dashboardPermissions);
    //dashboard
    this.state.dashboardPermissions && this.state.dashboardPermissions.view && this.state.selected_dashboard_permissions.push('view'); this.state.dashboard_permissions[0].checked = this.state.dashboardPermissions ? this.state.dashboardPermissions.view : false;

    //users
    this.state.usersPermissions && this.state.usersPermissions.view  && this.state.selected_users_permissions.push('view'); this.state.users_permissions[0].checked = this.state.usersPermissions ? this.state.usersPermissions.view : false;
    this.state.usersPermissions && this.state.usersPermissions.create && this.state.selected_users_permissions.push('create'); this.state.users_permissions[1].checked = this.state.usersPermissions ? this.state.usersPermissions.create : false;
    this.state.usersPermissions && this.state.usersPermissions.edit && this.state.selected_users_permissions.push('edit'); this.state.users_permissions[2].checked = this.state.usersPermissions ? this.state.usersPermissions.edit : false;
    this.state.usersPermissions && this.state.usersPermissions.delete && this.state.selected_users_permissions.push('delete'); this.state.users_permissions[3].checked = this.state.usersPermissions ? this.state.usersPermissions.delete : false;
    this.state.usersPermissions && this.state.usersPermissions.show && this.state.selected_users_permissions.push('show'); this.state.users_permissions[4].checked = this.state.usersPermissions ? this.state.usersPermissions.show : false;
    this.state.usersPermissions && this.state.usersPermissions.restore && this.state.selected_users_permissions.push('restore'); this.state.users_permissions[5].checked = this.state.usersPermissions ? this.state.usersPermissions.restore : false;

    //unit
    this.state.unitPermissions && this.state.unitPermissions.view && this.state.selected_unit_permissions.push('view'); this.state.unit_permissions[0].checked = this.state.unitPermissions ? this.state.unitPermissions.view : false;
    this.state.unitPermissions && this.state.unitPermissions.create && this.state.selected_unit_permissions.push('create'); this.state.unit_permissions[1].checked = this.state.unitPermissions ? this.state.unitPermissions.create : false;
    this.state.unitPermissions && this.state.unitPermissions.edit && this.state.selected_unit_permissions.push('edit'); this.state.unit_permissions[2].checked = this.state.unitPermissions ? this.state.unitPermissions.edit : false;
    this.state.unitPermissions && this.state.unitPermissions.show && this.state.selected_unit_permissions.push('show'); this.state.unit_permissions[3].checked = this.state.unitPermissions ? this.state.unitPermissions.show : false;
    this.state.unitPermissions && this.state.unitPermissions.trash && this.state.selected_unit_permissions.push('trash'); this.state.unit_permissions[4].checked = this.state.unitPermissions ? this.state.unitPermissions.trash : false;
    this.state.unitPermissions && this.state.unitPermissions.period && this.state.selected_unit_permissions.push('special_price'); this.state.unit_permissions[5].checked = this.state.unitPermissions ? this.state.unitPermissions.period : false;
    this.state.unitPermissions && this.state.unitPermissions.restore && this.state.selected_unit_permissions.push('restore'); this.state.unit_permissions[6].checked = this.state.unitPermissions ? this.state.unitPermissions.restore : false;
    this.state.unitPermissions && this.state.unitPermissions.disableSubUnit && this.state.selected_unit_permissions.push('disable'); this.state.unit_permissions[7].checked = this.state.unitPermissions ? this.state.unitPermissions.disableSubUnit : false;
    this.state.unitPermissions && this.state.unitPermissions.enableSubUnit && this.state.selected_unit_permissions.push('enable'); this.state.unit_permissions[8].checked = this.state.unitPermissions ? this.state.unitPermissions.enableSubUnit : false;
    this.state.unitPermissions && this.state.unitPermissions.publish && this.state.selected_unit_permissions.push('publish'); this.state.unit_permissions[9].checked = this.state.unitPermissions ? this.state.unitPermissions.publish : false;

    //booking
    this.state.bookingPermissions && this.state.bookingPermissions.view && this.state.selected_booking_permissions.push('view'); this.state.booking_permissions[0].checked = this.state.bookingPermissions? this.state.bookingPermissions.view : false;
    this.state.bookingPermissions && this.state.bookingPermissions.create && this.state.selected_booking_permissions.push('create'); this.state.booking_permissions[1].checked = this.state.bookingPermissions? this.state.bookingPermissions.create : false;
    this.state.bookingPermissions && this.state.bookingPermissions.edit && this.state.selected_booking_permissions.push('edit'); this.state.booking_permissions[2].checked = this.state.bookingPermissions? this.state.bookingPermissions.edit : false;
    this.state.bookingPermissions && this.state.bookingPermissions.delete && this.state.selected_booking_permissions.push('delete'); this.state.booking_permissions[3].checked = this.state.bookingPermissions? this.state.bookingPermissions.delete : false;
    this.state.bookingPermissions && this.state.bookingPermissions.cancel && this.state.selected_booking_permissions.push('cancel'); this.state.booking_permissions[4].checked = this.state.bookingPermissions? this.state.bookingPermissions.cancel : false;
    this.state.bookingPermissions && this.state.bookingPermissions.show && this.state.selected_booking_permissions.push('show'); this.state.booking_permissions[5].checked = this.state.bookingPermissions? this.state.bookingPermissions.show : false;

    //Special Price
    this.state.specialPricePermissions && this.state.specialPricePermissions.view && this.state.selected_special_price_permissions.push('view'); this.state.special_price_permissions[0].checked = this.state.specialPricePermissions? this.state.specialPricePermissions.view : false;
    this.state.specialPricePermissions && this.state.specialPricePermissions.create && this.state.selected_special_price_permissions.push('create'); this.state.special_price_permissions[1].checked = this.state.specialPricePermissions? this.state.specialPricePermissions.create : false;
    this.state.specialPricePermissions && this.state.specialPricePermissions.edit && this.state.selected_special_price_permissions.push('edit'); this.state.special_price_permissions[2].checked = this.state.specialPricePermissions? this.state.specialPricePermissions.edit : false;
    this.state.specialPricePermissions && this.state.specialPricePermissions.delete && this.state.selected_special_price_permissions.push('delete'); this.state.special_price_permissions[3].checked = this.state.specialPricePermissions? this.state.specialPricePermissions.delete : false;

    //Coupon
    this.state.couponPermissions && this.state.couponPermissions.view && this.state.selected_coupon_permissions.push('view'); this.state.coupon_permissions[0].checked = this.state.couponPermissions? this.state.couponPermissions.view : false;
    this.state.couponPermissions && this.state.couponPermissions.create && this.state.selected_coupon_permissions.push('create'); this.state.coupon_permissions[1].checked = this.state.couponPermissions? this.state.couponPermissions.create : false;
    this.state.couponPermissions && this.state.couponPermissions.edit && this.state.selected_coupon_permissions.push('edit'); this.state.coupon_permissions[2].checked = this.state.couponPermissions? this.state.couponPermissions.edit : false;
    this.state.couponPermissions && this.state.couponPermissions.delete && this.state.selected_coupon_permissions.push('delete'); this.state.coupon_permissions[3].checked = this.state.couponPermissions? this.state.couponPermissions.delete : false;

    //City
    this.state.cityPermissions && this.state.cityPermissions.view && this.state.selected_city_permissions.push('view'); this.state.city_permissions[0].checked = this.state.cityPermissions? this.state.cityPermissions.view : false;
    this.state.cityPermissions && this.state.cityPermissions.create && this.state.selected_city_permissions.push('create'); this.state.city_permissions[1].checked = this.state.cityPermissions? this.state.cityPermissions.create : false;
    this.state.cityPermissions && this.state.cityPermissions.edit && this.state.selected_city_permissions.push('edit'); this.state.city_permissions[2].checked = this.state.cityPermissions? this.state.cityPermissions.edit : false;
    this.state.cityPermissions && this.state.cityPermissions.delete && this.state.selected_city_permissions.push('delete'); this.state.city_permissions[3].checked = this.state.cityPermissions? this.state.cityPermissions.delete : false;

    //role
    this.state.rolePermissions && this.state.rolePermissions.view && this.state.selected_role_permissions.push('view'); this.state.role_permissions[0].checked = this.state.rolePermissions? this.state.rolePermissions.view : false;
    this.state.rolePermissions && this.state.rolePermissions.create && this.state.selected_role_permissions.push('create'); this.state.role_permissions[1].checked = this.state.rolePermissions? this.state.rolePermissions.create : false;
    this.state.rolePermissions && this.state.rolePermissions.edit && this.state.selected_role_permissions.push('edit'); this.state.role_permissions[2].checked = this.state.rolePermissions? this.state.rolePermissions.edit : false;
    this.state.rolePermissions && this.state.rolePermissions.show && this.state.selected_role_permissions.push('show'); this.state.role_permissions[3].checked = this.state.rolePermissions? this.state.rolePermissions.show : false;

    //Top Destination
    this.state.topDestinationPermissions && this.state.topDestinationPermissions.view && this.state.selected_topDestination_permissions.push('view'); this.state.topDestination_permissions[0].checked = this.state.topDestinationPermissions? this.state.topDestinationPermissions.view : false;
    this.state.topDestinationPermissions && this.state.topDestinationPermissions.create && this.state.selected_topDestination_permissions.push('create'); this.state.topDestination_permissions[1].checked = this.state.topDestinationPermissions? this.state.topDestinationPermissions.create : false;
    this.state.topDestinationPermissions && this.state.topDestinationPermissions.edit && this.state.selected_topDestination_permissions.push('edit'); this.state.topDestination_permissions[2].checked = this.state.topDestinationPermissions? this.state.topDestinationPermissions.edit : false;
    this.state.topDestinationPermissions && this.state.topDestinationPermissions.delete && this.state.selected_topDestination_permissions.push('delete'); this.state.topDestination_permissions[3].checked = this.state.topDestinationPermissions? this.state.topDestinationPermissions.delete : false;
  }

  handleChangeField(event){
    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })
  }

  handleDashboard_permissions(e){
    e.preventDefault()
    let obj = $(e.target)

    if(obj.parent().hasClass('active')){
      this.setState({selected_dashboard_permissions : this.state.selected_dashboard_permissions.filter(an => an != e.target.value)})
      obj.parent().removeClass('active')
      this.state.dashboard_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = false : '')
    }else{
      this.setState({selected_dashboard_permissions : [...this.state.selected_dashboard_permissions, e.target.value]})
      obj.parent().addClass('active')
      this.state.dashboard_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = true : '')
    }
  }

  handleUsers_permissions(e){
    e.preventDefault()
    let obj = $(e.target)

    if(obj.parent().hasClass('active')){
      this.setState({selected_users_permissions : this.state.selected_users_permissions.filter(an => an != e.target.value)})
      obj.parent().removeClass('active')
      this.state.users_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = false : '')
    }else{
      this.setState({selected_users_permissions : [...this.state.selected_users_permissions, e.target.value ]})
      obj.parent().addClass('active')
      this.state.users_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = true : '')
    }
  }

  handleBooking_permissions(e){
    e.preventDefault()
    let obj = $(e.target)

    if(obj.parent().hasClass('active')){
      this.setState({selected_booking_permissions : this.state.selected_booking_permissions.filter(an => an != e.target.value)})
      obj.parent().removeClass('active')
      this.state.booking_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = false : '')
    }else{
      this.setState({selected_booking_permissions : [...this.state.selected_booking_permissions, e.target.value ]})
      obj.parent().addClass('active')
      this.state.booking_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = true : '')
    }
  }

  handleUnit_permissions(e){
    e.preventDefault()
    let obj = $(e.target)

    if(obj.parent().hasClass('active')){
      this.setState({selected_unit_permissions : this.state.selected_unit_permissions.filter(an => an != e.target.value)})
      obj.parent().removeClass('active')
      this.state.unit_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = false : '')
    }else{
      this.setState({selected_unit_permissions : [...this.state.selected_unit_permissions, e.target.value ]})
      obj.parent().addClass('active')
      this.state.unit_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = true : '')
    }
  }

  handleSpecialPrice_permissions(e){
    e.preventDefault()
    let obj = $(e.target)

    if(obj.parent().hasClass('active')){
      this.setState({selected_special_price_permissions
         : this.state.selected_special_price_permissions.filter(an => an != e.target.value)})
      obj.parent().removeClass('active')
      this.state.special_price_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = false : '')
    }else{
      this.setState({selected_special_price_permissions : [...this.state.selected_special_price_permissions, e.target.value ]})
      obj.parent().addClass('active')
      this.state.special_price_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = true : '')
    }
  }

  handleCoupon_permissions(e){
    e.preventDefault()
    let obj = $(e.target)

    if(obj.parent().hasClass('active')){
      this.setState({selected_coupon_permissions
         : this.state.selected_coupon_permissions.filter(an => an != e.target.value)})
      obj.parent().removeClass('active')
      this.state.coupon_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = false : '')
    }else{
      this.setState({selected_coupon_permissions : [...this.state.selected_coupon_permissions, e.target.value ]})
      obj.parent().addClass('active')
      this.state.coupon_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = true : '')
    }
  }

  handleCity_permissions(e){
    e.preventDefault()
    let obj = $(e.target)

    if(obj.parent().hasClass('active')){
      this.setState({selected_city_permissions
         : this.state.selected_city_permissions.filter(an => an != e.target.value)})
      obj.parent().removeClass('active')
      this.state.city_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = false : '')
    }else{
      this.setState({selected_city_permissions : [...this.state.selected_city_permissions, e.target.value ]})
      obj.parent().addClass('active')
      this.state.city_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = true : '')
    }
  }

  handleRole_permissions(e){
    e.preventDefault()
    let obj = $(e.target)

    if(obj.parent().hasClass('active')){
      this.setState({selected_role_permissions
         : this.state.selected_role_permissions.filter(an => an != e.target.value)})
      obj.parent().removeClass('active')
      this.state.role_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = false : '')
    }else{
      this.setState({selected_role_permissions : [...this.state.selected_role_permissions, e.target.value ]})
      obj.parent().addClass('active')
      this.state.role_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = true : '')
    }
  }

  handleTopDestination_permissions(e){
    e.preventDefault()
    let obj = $(e.target)

    if(obj.parent().hasClass('active')){
      this.setState({selected_topDestination_permissions
         : this.state.selected_topDestination_permissions.filter(an => an != e.target.value)})
      obj.parent().removeClass('active')
      this.state.topDestination_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = false : '')
    }else{
      this.setState({selected_topDestination_permissions : [...this.state.selected_topDestination_permissions, e.target.value ]})
      obj.parent().addClass('active')
      this.state.topDestination_permissions.filter(ftd => ftd.value == e.target.value ? ftd.checked = true : '')
    }
  }

  render(){
    const {role} = this.props;

    const { form, name, selected_dashboard_permissions, selected_users_permissions, selected_booking_permissions , selected_unit_permissions , selected_special_price_permissions, selected_coupon_permissions , selected_city_permissions, selected_role_permissions , selected_topDestination_permissions, dashboard_permissions , users_permissions, booking_permissions , unit_permissions , special_price_permissions, coupon_permissions, city_permissions , role_permissions , topDestination_permissions } = this.state

    return(
      <ValidatorForm ref="form" onSubmit={this.props.handleSubmit} className="add-role">

        {this.state.loading && <Loading />}

        <div className="form-field">
          <TextValidator
              floatingLabelText="Role Name"
              onChange={this.handleChangeField}
              name="name"
              defaultValue={name? name : ''}
              style={{width: '100%'}}
            />
        </div>

        {dashboard_permissions &&
        <div className="form-field">
          <div>
            <label>Dashboard Permission</label>
          </div>
          <div className="row">
            {dashboard_permissions.map(dashboard_permission => {
              return (
                <div key={dashboard_permission.id} className="permission-item">
                  <Checkbox
                    label={dashboard_permission.name}
                    value={dashboard_permission.value}
                    onClick={this.handleDashboard_permissions}
                    name="dashboard-permission"
                    className={'permission-item '+(dashboard_permission.checked ? 'active': '')}
                    defaultChecked={dashboard_permission.checked}
                  />
                </div>
              )
            })}
            {selected_dashboard_permissions.map((selected_dashboard_permission, key) => {
              return (<input key={selected_dashboard_permission} type="hidden" name="selected_dashboard_permissions[]" value={selected_dashboard_permission} />)
            })}
          </div>
        </div> }


        {users_permissions &&
          <div className="form-field">
            <div>
              <label>Users Permission</label>
            </div>
            <div className="row">
              {users_permissions.map(users_permission => {
                return (
                  <div key={users_permission.id} className="permission-item">
                    <Checkbox
                      className={'permission-item '+(users_permission.checked ? 'active': '')}
                      defaultChecked={users_permission.checked}
                      label={users_permission.name}
                      value={users_permission.value}
                      onClick={this.handleUsers_permissions}
                      name="users-permission"
                      />
                  </div>
                )
              })}
              {selected_users_permissions.map((selected_users_permission, key) => {
                return (<input key={selected_users_permission} type="hidden" name="selected_users_permissions[]" value={selected_users_permission} />)
              })}
            </div>
          </div> }

        {unit_permissions &&
          <div className="form-field">
            <div>
              <label>Unit Permission</label>
            </div>
            <div className="row">
              {unit_permissions.map(unit_permission => {
                return (
                  <div key={unit_permission.id} className="permission-item">
                    <Checkbox
                      className={'permission-item '+(unit_permission.checked ? 'active': '')}
                      defaultChecked={unit_permission.checked}
                      label={unit_permission.name}
                      value={unit_permission.value}
                      onClick={this.handleUnit_permissions}
                      name="unit-permission"
                      />
                  </div>
                )
              })}
              {selected_unit_permissions.map((selected_unit_permission, key) => {
                return (<input key={selected_unit_permission} type="hidden" name="selected_unit_permissions[]" value={selected_unit_permission} />)
              })}
            </div>
          </div> }

        {booking_permissions &&
        <div className="form-field">
          <div>
            <label>Booking Permission</label>
          </div>
          <div className="row">
            {booking_permissions.map(booking_permission => {
              return (
                <div key={booking_permission.id} className="permission-item">
                  <Checkbox
                    label={booking_permission.name}
                    className={'permission-item '+(booking_permission.checked ? 'active': '')}
                    defaultChecked={booking_permission.checked}
                    value={booking_permission.value}
                    onClick={this.handleBooking_permissions}
                    name="booking-permission"
                  />
                </div>
              )
            })}
            {selected_booking_permissions.map((selected_booking_permission, key) => {
              return (<input key={selected_booking_permission} type="hidden" name="selected_booking_permissions[]" value={selected_booking_permission} />)
            })}
          </div>
        </div> }

        {special_price_permissions &&
        <div className="form-field">
          <div>
            <label>Special Price Permission</label>
          </div>
          <div className="row">
            {special_price_permissions.map(special_price_permission => {
              return (
                <div key={special_price_permission.id} className="permission-item">
                  <Checkbox
                    label={special_price_permission.name}
                    className={'permission-item '+(special_price_permission.checked ? 'active': '')}
                    defaultChecked={special_price_permission.checked}
                    value={special_price_permission.value}
                    onClick={this.handleSpecialPrice_permissions}
                    name="special-price-permission"
                  />
                </div>
              )
            })}
            {selected_special_price_permissions.map((selected_special_price_permission, key) => {
              return (<input key={selected_special_price_permission} type="hidden" name="selected_special_price_permissions[]" value={selected_special_price_permission} />)
            })}
          </div>
        </div> }

        {coupon_permissions &&
        <div className="form-field">
          <div>
            <label>Coupon Permission</label>
          </div>
          <div className="row">
            {coupon_permissions.map(coupon_permission => {
              return (
                <div key={coupon_permission.id} className="permission-item">
                  <Checkbox
                    className={'permission-item '+(coupon_permission.checked ? 'active': '')}
                    defaultChecked={coupon_permission.checked}
                    label={coupon_permission.name}
                    value={coupon_permission.value}
                    onClick={this.handleCoupon_permissions}
                    name="coupon-permission"
                  />
                </div>
              )
            })}
            {selected_coupon_permissions.map((selected_coupon_permission, key) => {
              return (<input key={selected_coupon_permission} type="hidden" name="selected_coupon_permissions[]" value={selected_coupon_permission} />)
            })}
          </div>
        </div> }

        {city_permissions &&
        <div className="form-field">
          <div>
            <label>City Permission</label>
          </div>
          <div className="row">
            {city_permissions.map(city_permission => {
              return (
                <div key={city_permission.id} className="permission-item">
                  <Checkbox
                    className={'permission-item '+(city_permission.checked ? 'active': '')}
                    defaultChecked={city_permission.checked}
                    label={city_permission.name}
                    value={city_permission.value}
                    onClick={this.handleCity_permissions}
                    name="city-permission"
                  />
                </div>
              )
            })}
            {selected_city_permissions.map((selected_city_permission, key) => {
              return (<input key={selected_city_permission} type="hidden" name="selected_city_permissions[]" value={selected_city_permission} />)
            })}
          </div>
        </div> }


        {role_permissions &&
        <div className="form-field">
          <div>
            <label>Role Permission</label>
          </div>
          <div className="row">
            {role_permissions.map(role_permission => {
              return (
                <div key={role_permission.id} className="permission-item">
                  <Checkbox
                    className={'permission-item '+(role_permission.checked ? 'active': '')}
                    defaultChecked={role_permission.checked}
                    label={role_permission.name}
                    value={role_permission.value}
                    onClick={this.handleRole_permissions}
                    name="role-permission"
                  />
                </div>
              )
            })}
            {selected_role_permissions.map((selected_role_permission, key) => {
              return (<input key={selected_role_permission} type="hidden" name="selected_role_permissions[]" value={selected_role_permission} />)
            })}
          </div>
        </div> }


        {topDestination_permissions &&
        <div className="form-field">
          <div>
            <label>Top Destination Permission</label>
          </div>
          <div className="row">
            {topDestination_permissions.map(topDestination_permission => {
              return (
                <div key={topDestination_permission.id} className="permission-item">
                  <Checkbox
                    className={'permission-item '+(topDestination_permission.checked ? 'active': '')}
                    defaultChecked={topDestination_permission.checked}
                    label={topDestination_permission.name}
                    value={topDestination_permission.value}
                    onClick={this.handleTopDestination_permissions}
                    name="top-destination-permission"
                  />
                </div>
              )
            })}
            {selected_topDestination_permissions.map((selected_topDestination_permission, key) => {
              return (<input key={selected_topDestination_permission} type="hidden" name="selected_topDestination_permissions[]" value={selected_topDestination_permission} />)
            })}
          </div>
        </div> }

      <div className="form-field submit">
        <RaisedButton label="Update Role" type="submit" primary={true} />
      </div>

      </ValidatorForm>
    )
  }
}

export default EditRoleForm;
