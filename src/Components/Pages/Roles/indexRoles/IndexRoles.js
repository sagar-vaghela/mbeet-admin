/*
* IndexRoles - Component connected to redux
*/

// import main packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

import swal from 'sweetalert'

// Roles Redux Actions
import {
  getRoles,
  deleteRole,
  RolesSearch,
  applyFilter } from '../../../../Redux/actions/rolesActions'

import { getCurrentUserPermission } from '../../../../Redux/actions/usersActions'

import {deleteItem} from '../../../../Utilities/functions'

// Table row Role
import RowRole  from './rowRole/RowRole'
import {DummyRowRole} from './rowRole/DummyRowRole'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  TableList} from '../../../GlobalComponents/GlobalComponents'

// Material UI Icons
import RoleIcon           from 'material-ui/svg-icons/social/group-add'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

class IndexRoles extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      comID: 'Roles',
      showList : [
        {count: 10, name: '10'},
        {count: 15, name: '15'},
        {count: 30, name: '30'},
      ],
      // filters: [
      //   {value: 'false', params: 'soft_delete', name: 'All'},
      //   {value: 'true',params: 'soft_delete', name: 'Deleted'},
      //   {value: 'owner',params: 'role', name: 'Owners'},
      //   {value: 'normal', params: 'role', name: 'Normal'},
      //   {value: 'super admin', params: 'role', name: 'Super Admin'},
      //   {value: 'true', params: 'phone_verified', name: 'Phone Verified'},
      //   {value: true, params: 'email_verified', name: 'Email Verified'},
      // ],
      currentFilter: 0,
      currentPage: 1,
      showRoles: 10,
      sort_by: 'name',
      sort_direction: 'desc',
      search: ' ',

    }
    this.handlePagination   = this.handlePagination.bind(this)
    this.handleShowingRoles = this.handleShowingRoles.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    // this.handleFilters = this.handleFilters.bind(this)
    this.handleSorting = this.handleSorting.bind(this)
    this.setSession = this.setSession.bind(this)
    this.getSession = this.getSession.bind(this)
  }

  componentWillMount(){

    this.setSession('currentFilter', this.getSession('currentFilter'))
    this.setSession('currentPage', this.getSession('currentPage'))
    this.setSession('showRoles', this.getSession('showRoles'))
    this.setSession('sort_by', this.getSession('sort_by'))
    this.setSession('sort_direction', this.getSession('sort_direction'))
    this.setSession('search', this.getSession('search'))

    // const {filters, currentFilter} = this.state
    const offset = (this.getSession('currentPage') - 1) * this.getSession('showRoles')

    // On Load the Component get the Roles
    const {dispatch} = this.props
    dispatch(getRoles(
      this.getSession('showRoles'),
      offset,
      // filters[this.getSession('currentFilter')],
      this.getSession('sort_by'),
      this.getSession('sort_direction'),
      this.getSession('search')),
    ).then(() => {

      // stop loading if successfull fetched the Roles
      this.setState({loading: false})

    })
    dispatch(getCurrentUserPermission())
  }

  setSession(par, value){

    if(typeof(value) === 'object'){
      value = this.state[par]
    }

    sessionStorage.setItem(this.state.comID+'-'+par,value)
    var obj = {};
    obj[par] = value
    this.setState(obj)
  }

  getSession(par){
    return sessionStorage.getItem(this.state.comID+'-'+par)
  }

  /***************************
  * On Click Delete
  * @Roleid : (Intger)
  ****************************/
  handleDelete(Roleid){
    const {dispatch, errMsg, succMsg} = this.props
    let _this = this
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: 'Yes, Delete it!',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    () => {
      dispatch(deleteRole(Roleid)).then(res => {
        swal({
          title: 'Deleted',
          type: 'success',
          text: succMsg,
          timer: 2000,
          showConfirmButton: false
        })
      })
      .catch(err => {
        _this.forceUpdate()
        swal("Error", err.response.data.message, "error");
      })
    })

    //deleteItem(Roleid,dispatch, deleteRole,this.props.errMsg)
  }

  /***************************
  * On Click Pagination
  ****************************/
  handlePagination(e, page){
    e.preventDefault()
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_by, sort_direction, search} = this.state
    const {sort_by, sort_direction, search} = this.state
    // prepair the offset
    const offset = (page - 1) * this.state.showRoles

    // get Roles
    dispatch(getRoles(
      this.state.showRoles,  // limit
      offset, // offset
      // filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
    )).then(() => {
      // change the Pagination number
      this.setState({currentPage: page})
      this.setSession('currentPage', page)
    })

  }

  /***************************
  * Handle Sorting
  ****************************/
  handleSorting(sort_by){
    if(sort_by === 0) return false
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_direction} = this.state
    const {currentFilter, sort_direction} = this.state
    const sort_dir = (sort_direction === 'asc') ? 'desc' : 'asc'

    this.setState({
      sort_by: sort_by,
      sort_direction: sort_dir
    })
    this.setSession('sort_by', sort_by)
    this.setSession('sort_direction', sort_dir)
    this.setSession('currentPage', 1)

    dispatch(getRoles(
      this.getSession('showRoles'), // limit
      0, // offset
      // filters[this.getSession('currentFilter')], // filter
      this.getSession('sort_by'), // sort by
      this.getSession('sort_direction'), // sort direction
      this.getSession('search'),
    ))

  }


  /***************************
  * On Change Filter
  ****************************/
  // handleFilters(filter_type, value, index){
  //   const {dispatch} = this.props
  //   const {filters} = this.state
  //   this.setSession('currentFilter', index)
  //   this.setSession('currentPage', 1)
  //   //this.setState({currentFilter: index})
  //   dispatch(getRoles(
  //     this.state.showRoles, // limit
  //     0, // offset
  //     filters[index],// filter
  //     this.state.sort_by, //sort by
  //     this.state.sort_direction, // sort direction
  //     this.state.search,
  //   ))
  // }

  /***************************
  * On change showing Roles
  ****************************/
  handleShowingRoles(count){
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_by, sort_direction, search} = this.state
    const {sort_by, sort_direction, search} = this.state
    dispatch(getRoles(
      count, // limit
      0, // offset
      // filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
    )).then(() => {
      this.setState({showRoles: count, currentPage: 1})
      this.setSession('showRoles', count)
      this.setSession('currentPage', 1)
    })
  }

  /***************************
  * On search
  ****************************/
  handleSearch(e){
    const {dispatch} = this.props
    const length = e.target.value.length
    const s = e.target.value

    // const {filters, currentFilter, sort_by, sort_direction} = this.state
    const {sort_by, sort_direction} = this.state

    if(length >= 3 || length === 0){
      dispatch(getRoles(
        this.getSession('showRoles'),
        0,
        // filters[this.getSession('currentFilter')],
        this.getSession('sort_by'),
        this.getSession('sort_direction'),
        s
      ))

      this.setSession('search', s)
      this.setSession('currentPage', 1)
    }

  }

  // render
  render(){

    // Table columns
    const columns = [
      {params: 'id', title: 'ID'},
      {params: 'name', title: 'Name'},
      {params: 0, title: 'Action'},
    ]
    // store data
    const {Roles, fetching, errMsg, totalRoles, current_user_permissions} = this.props

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Roles List',
      icon: (<RoleIcon className="pagetitle-icon"/>),
      addbtn: current_user_permissions.role_permissions.create? "/Roles/add" : ''
    }

    // TableList Options
    const tableListOptions = {
      columns: columns,
      loading: this.state.loading,
      update: fetching,
      onPageChange: this.handlePagination,
      total: totalRoles,
      rowShowing: parseInt(this.getSession('showRoles')),
      onChangeShowing: this.handleShowingRoles,
      page: this.getSession('currentPage'),
      showList: this.state.showList,
      onSearch: this.handleSearch,
      search: this.getSession('search'),
      onError: errMsg,
      // filters: this.state.filters,
      // onChangeFilter: this.handleFilters,
      currentFilter: this.getSession('currentFilter'),
      onSorting: this.handleSorting
    }


    return(
      <PagesContainer path={this.props.location}>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['roles']} />
          <PageHeader {...pageHeaderOptions} />
          <TableList {...tableListOptions}>

            { // if fetched Roles
              // !this.state.loading && Roles == null ?
              !this.state.loading ?
              (
                Roles.map(Role => {
                  return (<RowRole
                              key={'Role'+Role.id}
                              Role={Role}
                              onDelete={this.handleDelete}
                              permissions={current_user_permissions}
                              />)
                })
              )
              :
              (
                DummyRowRole()
              )
            /* end if */ }

          </TableList>
        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    Roles: store.roles.roles,
    current_user_permissions: store.users.current_user_permissions,
    totalRoles: store.roles.totalRoles,
    errMsg: store.roles.errMsg,
    fetching: store.roles.fetching,
  }
}

IndexRoles = connect(mapStateToProps)(IndexRoles)
export default IndexRoles
