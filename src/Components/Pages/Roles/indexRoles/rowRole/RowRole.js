// Main Packages
import React from 'react';
import { Link }           from 'react-router-dom'

// Material UI
import MenuItem from 'material-ui/MenuItem';
import IconMenu     from 'material-ui/IconMenu';
import IconButton   from 'material-ui/IconButton';

// Material UI Icons
import DeleteIcon         from 'material-ui/svg-icons/action/delete';
import ModeEditIcon       from 'material-ui/svg-icons/editor/mode-edit';
import MoreVertIcon       from 'material-ui/svg-icons/navigation/more-vert';
import dateFormat         from 'dateformat';
import RemoveRedEyeIcon   from 'material-ui/svg-icons/image/remove-red-eye';

// Style
import './rowRole.css'

import { CSSTransitionGroup } from 'react-transition-group'

const RowRole = (props) => {
  const {Role, onDelete, permissions} = props
  return (
    <CSSTransitionGroup
      component="tr"
transitionName="example"
transitionAppear={true}
transitionAppearTimeout={5000}
transitionEnter={false}
transitionLeave={false}>
      {/* ID */}
      <td>{Role.id}</td>

      <td>{Role.name}</td>

      {/* Action */}
      <td>
        <IconMenu
           iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
           anchorOrigin={{horizontal: 'left', vertical: 'top'}}
           targetOrigin={{horizontal: 'left', vertical: 'top'}}
        >

          {permissions.role_permissions.show &&
            <Link to={'/roles/show/'+Role.id} className="MenuItemBtn">
              <MenuItem primaryText="View" leftIcon={<RemoveRedEyeIcon />} />
            </Link>
          }

          {permissions.role_permissions.edit &&
              <Link to={'/roles/edit/'+Role.id} className="MenuItemBtn">
              <MenuItem primaryText="Edit" leftIcon={<ModeEditIcon />} />
            </Link>
          }
        </IconMenu>
      </td>
    </CSSTransitionGroup>
  )
}
export default RowRole
