import React from 'react'
import dateFormat  from 'dateformat'
import { Link }           from 'react-router-dom'

import {paymentsStatus, leaseStatus} from '../../../../Utilities/options'

import './showRole.css'

const RoleDetails = (props) => {
  const {role} = props
  const {dashboard_permissions, users_permissions , unit_permissions, booking_permissions , coupon_permissions, city_permissions, role_permissions, top_destination_permissions , special_price_permissions} = role.roles.role_permission? role.roles.role_permission.permitted_actions : '';
  return (
    <table className="table table-hoverd showRole">

  <tbody>
      <tr>
        <td>ID</td>
        <td>{role.roles.id}</td>
      </tr>
      <tr>
        <td>Name</td>
        <td>{role.roles.name}</td>
      </tr>
      <tr>
        <td>DashBoard Permission</td>
        <td>{dashboard_permissions && dashboard_permissions.view? 'View' : ''}</td>
      </tr>
      <tr>
        <td>User Permission</td>
        <td>
          {users_permissions && users_permissions.view? 'View : ' : ''}
          {users_permissions && users_permissions.create? 'Create : ' : ''}
          {users_permissions && users_permissions.edit? 'Edit : ' : ''}
          {users_permissions && users_permissions.delete? 'Delete' : ''}
        </td>
      </tr>
      <tr>
        <td>Unit Permission</td>
        <td>
          {unit_permissions && unit_permissions.view? 'View : ' : ''}
          {unit_permissions && unit_permissions.create? 'Create : ' : ''}
          {unit_permissions && unit_permissions.edit? 'Edit : ' : ''}
          {unit_permissions && unit_permissions.show? 'Show : ' : ''}
          {unit_permissions && unit_permissions.trash? 'Trash : ' : ''}
          {unit_permissions && unit_permissions.period? 'Special Price : ' : ''}
          {unit_permissions && unit_permissions.restore? 'Restore : ' : ''}
          {unit_permissions && unit_permissions.disableSubUnit? 'Disable Sub-Unit : ' : ''}
          {unit_permissions && unit_permissions.enableSubUnit? 'Enable Sub-Unit :' : ''}
          {unit_permissions && unit_permissions.publish? 'Publish' : ''}

        </td>
      </tr>
      <tr>
        <td>Booking Permission</td>
        <td>
          {booking_permissions && booking_permissions.view? 'View : ' : ''}
          {booking_permissions && booking_permissions.create? 'Create : ' : ''}
          {booking_permissions && booking_permissions.edit? 'Edit : ' : ''}
          {booking_permissions && booking_permissions.delete? 'Delete : ' : ''}
          {booking_permissions && booking_permissions.cancel? 'Cancel : ' : ''}
          {booking_permissions && booking_permissions.show? 'Show': ''}
        </td>
      </tr>
      <tr>
        <td>Special Price Permission</td>
        <td>
          {special_price_permissions && special_price_permissions.view? 'View : ' : ''}
          {special_price_permissions && special_price_permissions.create? 'Create : ' : ''}
          {special_price_permissions && special_price_permissions.edit? 'Edit : ' : ''}
          {special_price_permissions && special_price_permissions.delete? 'Delete' : ''}
        </td>
      </tr>
      <tr>
        <td>Coupon Permission</td>
        <td>
          {special_price_permissions && coupon_permissions.view? 'View : ' : ''}
          {special_price_permissions && coupon_permissions.create? 'Create : ' : ''}
          {special_price_permissions && coupon_permissions.edit? 'Edit : ' : ''}
          {special_price_permissions && coupon_permissions.delete? 'Delete' : ''}
        </td>
      </tr>


      <tr>
        <td>City Permission</td>
        <td>
          {city_permissions && city_permissions.view? 'View : ' : ''}
          {city_permissions && city_permissions.create? 'Create : ' : ''}
          {city_permissions && city_permissions.edit? 'Edit : ' : ''}
          {city_permissions && city_permissions.delete? 'Delete' : ''}
        </td>
      </tr>

      <tr>
        <td>Role Permission</td>
        <td>
          {role_permissions && role_permissions.view? 'View : ' : ''}
          {role_permissions && role_permissions.create? 'Create : ' : ''}
          {role_permissions && role_permissions.edit? 'Edit : ' : ''}
          {role_permissions && role_permissions.show? 'Show' : ''}
        </td>
      </tr>

      <tr>
        <td>Top Destination Permission</td>
        <td>
          {top_destination_permissions && top_destination_permissions.view? 'View : ' : ''}
          {top_destination_permissions && top_destination_permissions.create? 'Create : ' : ''}
          {top_destination_permissions && top_destination_permissions.edit? 'Edit : ' : ''}
          {top_destination_permissions && top_destination_permissions.delete? 'Delete' : ''}
        </td>
      </tr>
      </tbody>
    </table>
  )
}
export default RoleDetails
