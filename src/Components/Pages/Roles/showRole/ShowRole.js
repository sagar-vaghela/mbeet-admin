import React, {Component} from 'react'
import {connect} from 'react-redux'

// Users Redux Actions
import {getRole} from '../../../../Redux/actions/rolesActions'


import EventNoteIcon      from 'material-ui/svg-icons/notification/event-note'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

import RoleDetails from './RoleDetails'

import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading} from '../../../GlobalComponents/GlobalComponents'

class ShowRole extends Component {

  componentWillMount(){
    const {dispatch, match} = this.props
    const role_id = match.params.id

    dispatch(getRole(role_id))

  }
  render(){

    // Store props
    const {role, fetching, errMsg} = this.props

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Role Details',
      icon: (<EventNoteIcon className="pagetitle-icon"/>),
    }

    return (
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['roles','Show Role']} />
          <PageHeader {...pageHeaderOptions} />
        </CSSTransitionGroup>
        <div className="page-container">
          {(fetching || !role) && <Loading error={errMsg} />}
          {role && <RoleDetails role={role} />}
        </div>
      </PagesContainer>
    )
  }

}

const mapStateToProps = (store) => {
  return {
    role: store.roles.role,
    errMsg: store.roles.errMsg,
    fetching: store.roles.fetching,
  }
}

ShowRole = connect(mapStateToProps)(ShowRole)
export default ShowRole
