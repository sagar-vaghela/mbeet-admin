import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Redirect } from 'react-router'

import PlaceIcon          from 'material-ui/svg-icons/maps/place'

import AddTopDestinationForm from './AddTopDestinationForm'
import swal from 'sweetalert'

// Users Redux Actions
import {addTopDestination} from '../../../../Redux/actions/topDestinationsActions'
// city Redux Actions
import {top_destin_cities} from '../../../../Redux/actions/citiesActions'

import TopDestination         from 'material-ui/svg-icons/action/youtube-searched-for'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading,
  AlertMsg} from '../../../GlobalComponents/GlobalComponents'

import {
      cloudinaryUpload,
      imagePath,
      pathToUrl,
      filePreview
    } from '../../../../Utilities/cloudinaryUpload'

class AddTopDestination extends Component{

  constructor(props){
    super(props)
    this.state = {
      redirect: false,
      loading: false,
      image_en: null,
      image_ar: null,
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleImageEn = this.handleImageEn.bind(this)
    this.handleImageAr = this.handleImageAr.bind(this)
    this.fetchAddTopDestination = this.fetchAddTopDestination.bind(this)
  }

  handleImageEn(image_en){
    this.setState({image_en})
  }

  handleImageAr(image_ar){
    this.setState({image_ar})
  }

  handleSubmit(e){
      e.preventDefault()
      const {dispatch, match, succMsg, errMsg} = this.props

      const {image_en, image_ar} = this.state
      var formData = new FormData(e.target)
      let obj = e.target

      this.setState({loading: true})
      // if the user change the image
      if(image_en && image_ar){
        cloudinaryUpload(image_en, false).then(img => {
          formData.append('pictures_attributes[][image_en]', imagePath(img[0].url));
          // this.fetchAddTopDestination(formData, obj)
          cloudinaryUpload(image_ar, false).then(imgs => {
            formData.append('pictures_attributes[][image_ar]', imagePath(imgs[0].url));
            this.fetchAddTopDestination(formData, obj)
          })
        })

        // this.fetchAddTopDestination(formData, obj);

      }else{
        this.fetchAddTopDestination(formData, obj);
      }

    }

  fetchAddTopDestination(formData, form){
    const {dispatch} = this.props
    dispatch(addTopDestination(formData)).then(() => {

      const _this = this
      form.reset()

      swal({
        title: "TopDestination added successfully",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Back to topDestinations list",
        closeOnConfirm: true,
      },
      function(){
        _this.setState({redirect: true})
      })
      this.setState({loading: false, image_en: '', image_ar: ''})
    })
    .catch(() => {
      this.setState({loading: false})
    })

  }

  componentWillMount(){
    const {dispatch} = this.props
    dispatch(top_destin_cities());

  }
  // render
  render(){
    const {fetching, succMsg, errMsg, top_destin_cities} = this.props
    const {loading} = this.state
    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Create New Top Destination',
      icon: (<TopDestination className="pagetitle-icon"/>),
    }

    return (
      <PagesContainer>
        <Breadcrumb path={['topDestination','Add Top Destination']} />
        <PageHeader {...pageHeaderOptions} />

        <div className="page-container">

          {loading && <Loading />}

          {this.state.redirect && <Redirect to="/topDestinations" />}

          <div className="row justify-content-center">
            <div  className="col-lg-8 col-md-8">
              <AddTopDestinationForm
                handleSubmit={this.handleSubmit}
                handleImageEn={this.handleImageEn}
                handleImageAr={this.handleImageAr}
                imageEn={this.state.image_en}
                imageAr={this.state.image_ar}
                cities={top_destin_cities}
                />

              {(errMsg) && <AlertMsg error={errMsg} />}
            </div>
          </div>


        </div>

      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    top_destin_cities: store.cities.top_destin_cities,
    errMsg: store.topDestinations.errMsg,
    succMsg: store.topDestinations.succMsg,
    fetching: store.topDestinations.fetching,
  }
}

AddTopDestination = connect(mapStateToProps)(AddTopDestination)
export default AddTopDestination
