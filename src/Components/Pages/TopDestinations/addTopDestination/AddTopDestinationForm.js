/*
* AddTopDestinationForm: Object
* Child of: AddTopDestination
*/

// Main Packages
import React, {Component} from 'react'

import Checkbox from 'material-ui/Checkbox'

import {filePreview} from '../../../../Utilities/cloudinaryUpload'

// Material UI
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import {ValidatorForm, SelectValidator} from 'react-material-ui-form-validator'
import MenuItem from 'material-ui/MenuItem';

class AddTopDestinationForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      city_id: null,
    }
    this.handleUploadEn = this.handleUploadEn.bind(this)
    this.handleUploadAr = this.handleUploadAr.bind(this)
    this.handleUploadImgs = this.handleUploadImgs.bind(this)
  }

  handleChange = (event, index, value) => this.setState({city_id: value});

  handleUploadEn(e){
    e.preventDefault()
    const files = e.target.files

    // stop do anyting if the user click Cancel Btn
    if (files.length === 0) return false

    filePreview(files).then(image => {
      this.props.handleImageEn(image)
    })

  }

  handleUploadAr(e){
    e.preventDefault()
    const files = e.target.files

    // stop do anyting if the user click Cancel Btn
    if (files.length === 0) return false

    filePreview(files).then(image => {
      this.props.handleImageAr(image)
    })

  }


  handleUploadImgs(e){

    let input = e.target
    const files = input.files // get files
    const {handleImages} = this.props // fo setState on EditUnit Component

    // stop do anyting if the user click Cancel Btn
    if (files.length === 0) return false

    let images = this.props.images // get images array from EditUnit Component

    filePreview(files)
    .then(imgs => {
      imgs.map(img => {
        images.push(img)
        handleImages(images)
        this.forceUpdate()
      })

    })
  }


  render(){
    const {cities} = this.props

    return(
      <ValidatorForm ref="form" onSubmit={this.props.handleSubmit}>
       <div className="form-field">
         <SelectValidator

            floatingLabelText="Select City"
            value={this.state.city_id}
            onChange={this.handleChange}
            name="city"
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          >
            {cities && cities.map(city => {
              return (<MenuItem
                        key={city.id}
                        value={city.id}
                        primaryText={city.name} />)
            })}
          </SelectValidator>
          <input  name="city_id" value={this.state.city_id? this.state.city_id: ''} type="hidden" />
      </div>

      <div className="form-field">
        <TextField
         required
         hintText="Description English"
         style={{width: '100%'}}
         name="description_en"
       />
      </div>


      <div className="form-field">
        <TextField
         required
         hintText="Description Arabic"
         style={{width: '100%'}}
         name="description_ar"
       />
      </div>

      <div className="form-field">
        <label>Image English</label>
        <img src={this.props.imageEn} width="100" height="100" /> <br />
        <input type="file" name="file_en" onChange={this.handleUploadEn} required/>
      </div>

      <div className="form-field">
        <label>Image Arabic</label>
        <img src={this.props.imageAr} width="100" height="100" /> <br />
        <input type="file" name="file_ar" onChange={this.handleUploadAr} required/>
      </div>

      <div className="form-field submit">
        <RaisedButton label="Create Top Destination" type="submit" primary={true} />
      </div>

    </ValidatorForm>

    )
  }

}

export default AddTopDestinationForm;
