import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Redirect } from 'react-router'
import PlaceIcon          from 'material-ui/svg-icons/maps/place'

import EditTopDestinationForm from './EditTopDestinationForm'
import swal from 'sweetalert'
// Users Redux Actions
import {getTopDestination, editTopDestination} from '../../../../Redux/actions/topDestinationsActions'

import {top_destin_cities} from '../../../../Redux/actions/citiesActions'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading,
  AlertMsg} from '../../../GlobalComponents/GlobalComponents'

import {
    cloudinaryUpload,
    imagePath,
    pathToUrl,
    filePreview
  } from '../../../../Utilities/cloudinaryUpload'

class EditTopDestination extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      redirect: false,
      imageEn: null,
      imageAr: null,
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleSetImageEn = this.handleSetImageEn.bind(this)
    this.handleSetImageAr = this.handleSetImageAr.bind(this)
  }

  handleSetImageEn(imageEn){
    this.setState({imageEn})
  }

  handleSetImageAr(imageAr){
    this.setState({imageAr})
  }

  componentWillMount(){

    const {dispatch, match} = this.props
    const topDestinationid = match.params.id

    dispatch(getTopDestination(topDestinationid)).then(() => this.setState({loading: false}))
    dispatch(top_destin_cities());
  }

  fetchForm(topDestinationid, formData, form){
    const {dispatch} = this.props

    dispatch(editTopDestination(topDestinationid, formData)).then(() => {
      const _this = this
      this.setState({loading: false})

      swal({
        title: "TopDestination updated successfully",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Back to citis list",
        closeOnConfirm: true,
      },
      function(){
        _this.setState({redirect: true})
      });
    })
    .catch(() => {
      this.setState({loading: false})
    })
  }

  handleSubmit(e){
    e.preventDefault()
    const {dispatch, match, succMsg, errMsg} = this.props
    const topDestinationid = match.params.id
    const {imageEn, imageAr} = this.state
    var formData = new FormData(e.target)
    let obj = e.target

    this.setState({loading: true})
    // if the user change the image
    if(imageEn || imageAr){

      if (imageEn)
      {
        cloudinaryUpload(imageEn, false).then(img => {
          const imgEn_id = formData.get('imageEn_id')
          formData.append('pictures_attributes[][image_en]', imagePath(img[0].url));

          if(imgEn_id)
          formData.append('pictures_attributes[][Enid]', imgEn_id);

          if (imageAr)
          {
            cloudinaryUpload(imageAr, false).then(imgs => {
              const imgAr_id = formData.get('imageAr_id')
              formData.append('pictures_attributes[][image_ar]', imagePath(imgs[0].url));

              if(imgAr_id)
              formData.append('pictures_attributes[][Arid]', imgAr_id);


              this.fetchForm(topDestinationid, formData, obj)
            })
          }
          else {
            this.fetchForm(topDestinationid, formData, obj)
          }
          // this.fetchForm(topDestinationid, formData, obj)
        })
      }
      else {
        if (imageAr)
        {
          cloudinaryUpload(imageAr, false).then(imgs => {
            const imgAr_id = formData.get('imageAr_id')
            formData.append('pictures_attributes[][image_ar]', imagePath(imgs[0].url));

            if(imgAr_id)
            formData.append('pictures_attributes[][Arid]', imgAr_id);


            this.fetchForm(topDestinationid, formData, obj)
          })
        }
        else {
          this.fetchForm(topDestinationid, formData, obj)
        }
      }


      //formData.append('pictures_attributes[name]', 123);
    }else{
      this.fetchForm(topDestinationid, formData, obj)
    }

  }
  // render
  render(){
    const {topDestination, fetching, succMsg, errMsg, top_destin_cities} = this.props
    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Edit TopDestination',
      icon: (<PlaceIcon className="pagetitle-icon"/>),
    }

    return (
      <PagesContainer>
        <Breadcrumb path={['topDestination','Edit topDestination']} />
        <PageHeader {...pageHeaderOptions} />

        <div className="page-container">

          {this.state.loading && <div><Loading error={errMsg} success={succMsg} /> <br /><br /><br /><br /></div> }

          {this.state.redirect && <Redirect to="/topDestinations" />}

          <div className="row justify-content-center">
            <div  className="col-lg-8 col-md-8" style={{position: 'inherit'}}>

              {topDestination
                && <EditTopDestinationForm
                        handleSubmit={this.handleSubmit}
                        topDestination={topDestination}
                        handleSetImageAr={this.handleSetImageAr}
                        handleSetImageEn={this.handleSetImageEn}
                        imageEn={this.state.imageEn}
                        imageAr={this.state.imageAr}
                        cities={top_destin_cities}
                        /> }

              {(errMsg) && <AlertMsg error={errMsg} />}
            </div>
          </div>


        </div>

      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    topDestination: store.topDestinations.top_destination,
    top_destin_cities: store.cities.top_destin_cities,
    errMsg: store.topDestinations.errMsg,
    succMsg: store.topDestinations.succMsg,
    fetching: store.topDestinations.fetching,
  }
}

EditTopDestination = connect(mapStateToProps)(EditTopDestination)
export default EditTopDestination
