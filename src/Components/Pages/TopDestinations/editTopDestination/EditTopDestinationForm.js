/*
* EditTopDestinationForm: Object
* Child of: EditTopDestination
*/

// Main Packages
import React, {Component} from 'react'

import Checkbox from 'material-ui/Checkbox';

import {filePreview} from '../../../../Utilities/cloudinaryUpload'

// Material UI
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import {ValidatorForm, SelectValidator} from 'react-material-ui-form-validator'
import MenuItem from 'material-ui/MenuItem';




class EditTopDestinationForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      city_id: this.props.topDestination.city.id,
      imageEn: null,
      imageAr: null,
      upload_new_en: true,
      upload_new_ar: true,
    }
    this.handleUploadEn = this.handleUploadEn.bind(this)
    this.handleUploadAr = this.handleUploadAr.bind(this)
  }

  componentWillMount(){
    const {topDestination} = this.props

    if(topDestination.image){
      topDestination.image.map(img => {
        if(img.locale === false)
        {
          this.setState({imageEn: img.url, upload_new_en: false});
        }
        else {
          this.setState({imageAr: img.url, upload_new_ar: false});
        }
      })
    }

  }

  handleChange = (event, index, value) => this.setState({city_id: value});

  handleUploadEn(e){
    e.preventDefault()
    const files = e.target.files

    // stop do anyting if the user click Cancel Btn
    if (files.length === 0) return false

    filePreview(files).then(imageEn => {
      this.setState({imageEn})
      this.props.handleSetImageEn(imageEn)
    })
  }

  handleUploadAr(e){
    e.preventDefault()
    const files = e.target.files

    // stop do anyting if the user click Cancel Btn
    if (files.length === 0) return false

    filePreview(files).then(imageAr => {
      this.setState({imageAr})
      this.props.handleSetImageAr(imageAr)

    })
  }


  render(){
    const {topDestination, imageEn, imageAr, cities} = this.props
    console.log(topDestination);
    return(
        <ValidatorForm ref="form" onSubmit={this.props.handleSubmit}>
          <div className="form-field">
            <SelectValidator
              floatingLabelText="Select City"
              value={this.state.city_id}
              onChange={this.handleChange}
              name="city"
              style={{width: '100%'}}
              validators={['required']}
              errorMessages={['this field is required']}
              >
              {cities && cities.map(city => {
                return (<MenuItem
                  key={city.id}
                  value={city.id}
                  primaryText={city.name} />)
                })}
              </SelectValidator>
              <input  name="city_id" value={this.state.city_id? this.state.city_id: ''} type="hidden" />
          </div>

          <div className="form-field">
            <TextField
              required
              hintText="Description English"
              style={{width: '100%'}}
              name="description_en"
              defaultValue={topDestination.description_translations.en}
              />
          </div>

          <div className="form-field">
            <TextField
              required
              hintText="Description Arabic"
              style={{width: '100%'}}
              name="description_ar"
              defaultValue={topDestination.description_translations.ar}
              />
          </div>

          <div className="form-field">
            <label>Image English</label>
            {!this.state.upload_new_en ? (
              <span>
                <img src={this.state.imageEn} width="100" height="100" /> <br />
                <input type="file" name="file_en" onChange={this.handleUploadEn} />
                <input type="hidden" name="imageEn_id" value={topDestination.image.filter(img => img.locale === false)[0].id} />
              </span>
            ):(
              <span>
                {this.state.imageEn &&
                  <span>
                    <img src={this.state.imageEn} width="100" height="100" /> <br />
                  </span>
                }
                <input type="file" name="file_en" onChange={this.handleUploadEn} />
              </span>
            )}
          </div>

          <div className="form-field">
            <label>Image Arabic</label>
            {!this.state.upload_new_ar ? (
              <span>
                <img src={this.state.imageAr} width="100" height="100" /> <br />
                <input type="file" name="file_ar" onChange={this.handleUploadAr } />
                <input type="hidden" name="imageAr_id" value={topDestination.image.filter(img => img.locale === true)[0].id} />
              </span>
            ):(
              <span>
                {this.state.imageAr &&
                  <span>
                    <img src={this.state.imageAr} width="100" height="100" /> <br />
                  </span>
                }
                <input type="file" name="file_ar" onChange={this.handleUploadAr} />
              </span>
            )}
          </div>

          <div className="form-field submit">
            <RaisedButton label="Update" type="submit" primary={true} />
          </div>
        </ValidatorForm>
      )
    }

  }

  export default EditTopDestinationForm;
