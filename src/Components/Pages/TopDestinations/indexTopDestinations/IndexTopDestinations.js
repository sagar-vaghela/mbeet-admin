/*
* IndexTopDestinations - Component connected to redux
*/

// import main packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

import swal from 'sweetalert'

// TopDestinations Redux Actions
import {
  getTopDestinations,
  deleteTopDestination,
  TopDestinationsSearch,
  applyFilter } from '../../../../Redux/actions/topDestinationsActions'

// Users Redux Actions
import {city_list_with_unit} from '../../../../Redux/actions/citiesActions'


import {deleteItem} from '../../../../Utilities/functions'

// Table row TopDestination
import RowTopDestination  from './rowTopDestination/RowTopDestination'
import {DummyRowTopDestination} from './rowTopDestination/DummyRowTopDestination'

import IndexTopDestinationsList from './IndexTopDestinationsList'

import { getCurrentUserPermission } from '../../../../Redux/actions/usersActions'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  TopDestinationsList } from '../../../GlobalComponents/GlobalComponents'

// Material UI Icons
import TopDestination         from 'material-ui/svg-icons/action/youtube-searched-for'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

class IndexTopDestinations extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      comID: 'TopDestinations',
      showList : [
        {count: 10, name: '10'},
        {count: 15, name: '15'},
        {count: 30, name: '30'},
      ],
      // filters: [
      //   {value: 'false', params: 'soft_delete', name: 'All'},
      //   {value: 'true',params: 'soft_delete', name: 'Deleted'},
      //   {value: 'owner',params: 'topDestination', name: 'Owners'},
      //   {value: 'normal', params: 'topDestination', name: 'Normal'},
      //   {value: 'super admin', params: 'topDestination', name: 'Super Admin'},
      //   {value: 'true', params: 'phone_verified', name: 'Phone Verified'},
      //   {value: true, params: 'email_verified', name: 'Email Verified'},
      // ],
      currentFilter: 0,
      currentPage: 1,
      showTopDestinations: 10,
      sort_by: 'name',
      sort_direction: 'desc',
      search: ' ',

    }
    this.handlePagination   = this.handlePagination.bind(this)
    this.handleShowingTopDestinations = this.handleShowingTopDestinations.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    // this.handleFilters = this.handleFilters.bind(this)
    this.handleSorting = this.handleSorting.bind(this)
    this.setSession = this.setSession.bind(this)
    this.getSession = this.getSession.bind(this)
  }

  componentWillMount(){

    this.setSession('currentFilter', this.getSession('currentFilter'))
    this.setSession('currentPage', this.getSession('currentPage'))
    this.setSession('showTopDestinations', this.getSession('showTopDestinations'))
    this.setSession('sort_by', this.getSession('sort_by'))
    this.setSession('sort_direction', this.getSession('sort_direction'))
    this.setSession('search', this.getSession('search'))

    // const {filters, currentFilter} = this.state
    const offset = (this.getSession('currentPage') - 1) * this.getSession('showTopDestinations')

    // On Load the Component get the TopDestinations
    const {dispatch} = this.props
    dispatch(getTopDestinations(
      this.getSession('showTopDestinations'),
      offset,
      // filters[this.getSession('currentFilter')],
      this.getSession('sort_by'),
      this.getSession('sort_direction'),
      this.getSession('search')),
    ).then(() => {

      // stop loading if successfull fetched the TopDestinations
      this.setState({loading: false})

    })
    dispatch(city_list_with_unit());
    dispatch(getCurrentUserPermission())
  }

  setSession(par, value){

    if(typeof(value) === 'object'){
      value = this.state[par]
    }

    sessionStorage.setItem(this.state.comID+'-'+par,value)
    var obj = {};
    obj[par] = value
    this.setState(obj)
  }

  getSession(par){
    return sessionStorage.getItem(this.state.comID+'-'+par)
  }

  /***************************
  * On Click Delete
  * @TopDestinationid : (Intger)
  ****************************/
  handleDelete(TopDestinationid){
    const {dispatch, errMsg, succMsg} = this.props
    let _this = this
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: 'Yes, Delete it!',
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    () => {
      dispatch(deleteTopDestination(TopDestinationid)).then(res => {
        swal({
          title: 'Deleted',
          type: 'success',
          text: succMsg,
          timer: 2000,
          showConfirmButton: false
        })
      })
      .catch(err => {
        _this.forceUpdate()
        swal("Error", err.response.data.message, "error");
      })
    })

    //deleteItem(TopDestinationid,dispatch, deleteTopDestination,this.props.errMsg)
  }

  /***************************
  * On Click Pagination
  ****************************/
  handlePagination(e, page){
    e.preventDefault()
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_by, sort_direction, search} = this.state
    const {sort_by, sort_direction, search} = this.state
    // prepair the offset
    const offset = (page - 1) * this.state.showTopDestinations

    // get TopDestinations
    dispatch(getTopDestinations(
      this.state.showTopDestinations,  // limit
      offset, // offset
      // filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
    )).then(() => {
      // change the Pagination number
      this.setState({currentPage: page})
      this.setSession('currentPage', page)
    })

  }

  /***************************
  * Handle Sorting
  ****************************/
  handleSorting(sort_by){
    if(sort_by === 0) return false
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_direction} = this.state
    const {currentFilter, sort_direction} = this.state
    const sort_dir = (sort_direction === 'asc') ? 'desc' : 'asc'

    this.setState({
      sort_by: sort_by,
      sort_direction: sort_dir
    })
    this.setSession('sort_by', sort_by)
    this.setSession('sort_direction', sort_dir)
    this.setSession('currentPage', 1)

    dispatch(getTopDestinations(
      this.getSession('showTopDestinations'), // limit
      0, // offset
      // filters[this.getSession('currentFilter')], // filter
      this.getSession('sort_by'), // sort by
      this.getSession('sort_direction'), // sort direction
      this.getSession('search'),
    ))

  }


  /***************************
  * On Change Filter
  ****************************/
  // handleFilters(filter_type, value, index){
  //   const {dispatch} = this.props
  //   const {filters} = this.state
  //   this.setSession('currentFilter', index)
  //   this.setSession('currentPage', 1)
  //   //this.setState({currentFilter: index})
  //   dispatch(getTopDestinations(
  //     this.state.showTopDestinations, // limit
  //     0, // offset
  //     filters[index],// filter
  //     this.state.sort_by, //sort by
  //     this.state.sort_direction, // sort direction
  //     this.state.search,
  //   ))
  // }

  /***************************
  * On change showing TopDestinations
  ****************************/
  handleShowingTopDestinations(count){
    const {dispatch} = this.props
    // const {filters, currentFilter, sort_by, sort_direction, search} = this.state
    const {sort_by, sort_direction, search} = this.state
    dispatch(getTopDestinations(
      count, // limit
      0, // offset
      // filters[currentFilter], // filter
      sort_by, // sort by
      sort_direction, // sort direction
      search,
    )).then(() => {
      this.setState({showTopDestinations: count, currentPage: 1})
      this.setSession('showTopDestinations', count)
      this.setSession('currentPage', 1)
    })
  }

  /***************************
  * On search
  ****************************/
  handleSearch(e){
    const {dispatch} = this.props
    const length = e.target.value.length
    const s = e.target.value

    // const {filters, currentFilter, sort_by, sort_direction} = this.state
    const {sort_by, sort_direction} = this.state

    if(length >= 3 || length === 0){
      dispatch(getTopDestinations(
        this.getSession('showTopDestinations'),
        0,
        // filters[this.getSession('currentFilter')],
        this.getSession('sort_by'),
        this.getSession('sort_direction'),
        s
      ))

      this.setSession('search', s)
      this.setSession('currentPage', 1)
    }

  }

  // render
  render(){

    // store data
    const {TopDestinations, fetching, errMsg, totalTopDestinations, current_user_permissions} = this.props

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Top Destinations',
      icon: (<TopDestination className="pagetitle-icon"/>),
      addbtn: current_user_permissions.top_destination_permissions.create? '/topDestination/add' : ''
    }

    // TopDestinationsOptions Options
    const TopDestinationsOptions = {
      topDestinations: TopDestinations,
      loading: this.state.loading,
      update: fetching,
      onPageChange: this.handlePagination,
      total: totalTopDestinations,
      rowShowing: parseInt(this.getSession('showTopDestinations')),
      onChangeShowing: this.handleShowingTopDestinations,
      page: this.getSession('currentPage'),
      showList: this.state.showList,
      onSearch: this.handleSearch,
      search: this.getSession('search'),
      onError: errMsg,
      // filters: this.state.filters,
      // onChangeFilter: this.handleFilters,
      currentFilter: this.getSession('currentFilter'),
      onSorting: this.handleSorting
    }


    return(
      <PagesContainer path={this.props.location}>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['TopDestinations']} />
          <PageHeader {...pageHeaderOptions} />
          <TopDestinationsList {...TopDestinationsOptions} />
        </CSSTransitionGroup>

        <hr/>

        <IndexTopDestinationsList
          permissions={current_user_permissions}
          />

      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    TopDestinations: store.topDestinations.top_destinations,
    current_user_permissions: store.users.current_user_permissions,
    totalTopDestinations: store.topDestinations.totalTop_destinations,
    errMsg: store.topDestinations.errMsg,
    fetching: store.topDestinations.fetching,
  }
}

IndexTopDestinations = connect(mapStateToProps)(IndexTopDestinations)

export default IndexTopDestinations
