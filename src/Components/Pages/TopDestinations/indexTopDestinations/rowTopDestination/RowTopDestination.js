import React from 'react'
import Avatar       from 'material-ui/Avatar';
import { Link }           from 'react-router-dom'
import ReactTooltip from 'react-tooltip'
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
// Style
import './rowTopDestination.css'


const RowTopDestination = (props) => {
  const {topDestination, permissions} = props
  const stars = [1,2,3,4,5]
    return (
      <div className={'col-lg-3 topDestination-item-container'}>
          <div className="card topDestination-item">

            <div className="card-img-top">
              <img  className="img-fluid"
                src={topDestination.image ? topDestination.image.url : topDestination.default_image }
                alt={topDestination.image ? topDestination.image.id : ''}

                />
            </div>

            <div className="card-block">
              <div className="topDestination-price">
                {topDestination.name}
              </div>

              <div className="col-lg-12 topDestination-details">
                <div className="row">
                  <div className="col-lg-6 col-md-6 topDestination-details-item">
                    {/*<i className="fa fa-bath" aria-hidden="true"></i> */}
                    Heights hits
                  </div>

                  <div className="col-lg-6 col-md-6 topDestination-details-item">
                   {topDestination.most_searched}
                  </div>
                  <div className="col-lg-6 col-md-6 topDestination-details-item">
                    Top Destination
                  </div>
                  <div className="col-lg-6 col-md-6 topDestination-details-item">
                    {topDestination.flag? 'True' : 'False'}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
}
export default RowTopDestination
