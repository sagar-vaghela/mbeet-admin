import React from 'react'
import Avatar       from 'material-ui/Avatar';
import { Link }           from 'react-router-dom'
import ReactTooltip from 'react-tooltip'
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
// Style
import './rowTopDestinationList.css'


const RowTopDestinationList = (props) => {
  const {topDestination, permissions, onDelete} = props
  const stars = [1,2,3,4,5]
  const links=<div className="topDestinationList-actions">
                {permissions.top_destination_permissions.edit &&
                  <Link to={'/topDestination/edit/'+topDestination.id} data-tip="Edits" className="action">
                    <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                  </Link>
                }
                {permissions.top_destination_permissions.delete &&
                  <a href="#" data-tip="Delete" className="action" onClick={(e) => onDelete(e, topDestination.id)}>
                    <i className="fa fa-trash-o" aria-hidden="true"></i>
                  </a>
                }
                <ReactTooltip place="left" type="dark" effect="solid" />
              </div>

      console.log(topDestination.image.length);

    return (
      <div className={'col-lg-3 topDestinationList-item-container'}>

          {links}
          <div className="card topDestinationList-item">

            <div className="card-img-top">

              {topDestination.image.length > 0 ?
                <img  className="img-fluid"
                src={topDestination.image.filter(img => img.locale === false)[0].url}
                alt={topDestination.image.filter(img => img.locale === false)[0].id}
                />
                :
                <img  className="img-fluid"
                src={topDestination.default_image}
                alt={''}
                />
              }

            </div>

            <div className="card-block">
              <div className="topDestinationList-price">
                {topDestination.city.name}
              </div>

              <div className="col-lg-12 topDestinationList-details">
                <div className="row">
                  <div className="col-lg-6 col-md-6 topDestinationList-details-item">
                    {/*<i className="fa fa-bath" aria-hidden="true"></i> */}
                    Heights hits
                  </div>

                  <div className="col-lg-6 col-md-6 topDestinationList-details-item">
                   {topDestination.city.most_searched}
                  </div>
                  <div className="col-lg-6 col-md-6 topDestination-details-item">
                    Top Destination
                  </div>
                  <div className="col-lg-6 col-md-6 topDestination-details-item">
                    True
                  </div>
                  <div className="col-lg-6 col-md-6 topDestination-details-item">
                    Description
                  </div>
                  <div className="col-lg-6 col-md-6 topDestination-details-item">
                    {topDestination.description}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
}
export default RowTopDestinationList
