import React,{Component} from 'react'
import {connect} from 'react-redux'
import { Redirect } from 'react-router'

import AddSpecialPriceForm from './AddSpecialPriceForm'

// SpecialPrices Redux Actions
import {addSpecialPrice} from '../../../../../Redux/actions/specialPricesActions'
import swal from 'sweetalert'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

import { Link }           from 'react-router-dom'

// Material UI Icons
import LocationCity       from 'material-ui/svg-icons/social/location-city'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  AlertMsg,
  Loading} from '../../../../GlobalComponents/GlobalComponents'

class AddSpecialPrice extends Component{

  constructor(props){
    super(props)
    this.state = {
      redirect: false,
      unit_id: '',
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount(){
    // On Load the Component get the AddSpecialPrices
    const {match} = this.props
    this.setState({unit_id: match.params.id})
  }

  handleSubmit(e){
    e.preventDefault()
    const {dispatch, succMsg} = this.props
    var formData = new FormData(e.target)
    let obj = e.target
    dispatch(addSpecialPrice(formData)).then(() => {
      const _this = this
      obj.reset()
      swal({
        title: "SpecialPrice added successfully",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Back to specialPrices list",
        closeOnConfirm: true,
      },
      function(){
        _this.setState({redirect: true})
      });
    })

  }

  render(){

    // Store props
    const {errMsg, succMsg, fetching , match} = this.props

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Add New SpecialPrice',
      icon: (<LocationCity className="pagetitle-icon"/>),
    }
    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }
    const redirect_path = '/units/'+this.props.match.params.id+'/special_price';

    return(
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['units',<Link to={redirect_path}> SpecialPrice </Link>,'add specialPrice']} />
          <PageHeader {...pageHeaderOptions} />

          <div className="page-container">

            {fetching && <Loading  />}
            {this.state.redirect && <Redirect to={redirect_path} />}

            <div className="row justify-content-center">
              <div  className="col-lg-8 col-md-8" style={{position: 'inherit'}}>
                <AddSpecialPriceForm handleSubmit={this.handleSubmit} {...this.state}/>
              </div>
            </div>

            {/* after dispatch, display the error or success message */}
            {(errMsg) && <AlertMsg error={errMsg} />}

          </div>

        </CSSTransitionGroup>
      </PagesContainer>
    )
  }

}

const mapStateToProps = (store) => {
  return {
    errMsg: store.specialPrices.errMsg,
    fetching: store.specialPrices.fetching,
    succMsg: store.specialPrices.succMsg,
  }
}

AddSpecialPrice = connect(mapStateToProps)(AddSpecialPrice)
export default AddSpecialPrice
