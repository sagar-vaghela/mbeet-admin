import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Redirect } from 'react-router'
import swal from 'sweetalert'
// SpecialPrices Redux Actions
import {getSpecialPrice, editSpecialPrice} from '../../../../../Redux/actions/specialPricesActions'

import { Link }           from 'react-router-dom'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading,
  AlertMsg} from '../../../../GlobalComponents/GlobalComponents'

import EditSpecialPriceForm from './EditSpecialPriceForm'

// Material UI Icons
import LocationCity       from 'material-ui/svg-icons/social/location-city'


class EditSpecialPrice extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      redirect: false,
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount(){

    const {dispatch, match} = this.props
    const specialPriceid = match.params.id

    dispatch(getSpecialPrice(specialPriceid)).then(() => this.setState({loading: false}))
  }

  handleSubmit(e){
    e.preventDefault()
    const {dispatch, match, succMsg} = this.props
    const specialPriceid = match.params.id
    var formData = new FormData(e.target)
    let obj = e.target
    dispatch(editSpecialPrice(specialPriceid, formData)).then(() => {
      const _this = this
      obj.reset()
      swal({
        title: "SpecialPrice updated successfully",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Back to specialPrices list",
        closeOnConfirm: true,
      },
      function(){
        _this.setState({redirect: true})
      });
    })

  }

  // render
  render(){

    // Store props
    const {specialPrice, errMsg, succMsg, fetching} = this.props

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Edit SpecialPrice',
      icon: (<LocationCity className="pagetitle-icon"/>),
    }

    const redirect_path = '/units/'+this.props.match.params.unit_id+'/special_price';
    return(
      <PagesContainer path={this.props.location}>
        <Breadcrumb path={['units',<Link to={redirect_path}> SpecialPrice </Link>,'edit']} />
        <PageHeader {...pageHeaderOptions} />

        {this.state.redirect && <Redirect to={redirect_path} />}

        <div className="page-container">
          {(fetching) && <Loading />}

          <div className="row justify-content-center">
            <div  className="col-lg-8 col-md-8">
              {(specialPrice && !this.state.loading) && <EditSpecialPriceForm handleSubmit={this.handleSubmit} specialPrice={specialPrice} />}
              {(errMsg) && <AlertMsg error={errMsg} />}
            </div>
          </div>

        </div>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    specialPrice: store.specialPrices.specialPrice,
    errMsg: store.specialPrices.errMsg,
    succMsg: store.specialPrices.succMsg,
    fetching: store.specialPrices.fetching,
  }
}

EditSpecialPrice = connect(mapStateToProps)(EditSpecialPrice)
export default EditSpecialPrice
