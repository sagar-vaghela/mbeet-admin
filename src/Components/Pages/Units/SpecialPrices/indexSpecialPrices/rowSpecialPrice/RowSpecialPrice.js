// Main Packages
import React from 'react';
import { Link }           from 'react-router-dom'

// Material UI
import MenuItem from 'material-ui/MenuItem';
import IconMenu     from 'material-ui/IconMenu';
import IconButton   from 'material-ui/IconButton';

// Material UI Icons
import DeleteIcon         from 'material-ui/svg-icons/action/delete';
import ModeEditIcon       from 'material-ui/svg-icons/editor/mode-edit';
import MoreVertIcon       from 'material-ui/svg-icons/navigation/more-vert';
import dateFormat         from 'dateformat';

// Style
import './rowSpecialPrice.css'

import { CSSTransitionGroup } from 'react-transition-group'

const RowSpecialPrice = (props) => {
  const {specialPrice, onDelete , unitid, permissions} = props
  return (
    <CSSTransitionGroup
            component="tr"
      transitionName="example"
      transitionAppear={true}
      transitionAppearTimeout={5000}
      transitionEnter={false}
      transitionLeave={false}>
      {/* ID */}
      <td>{specialPrice.id}</td>

      <td>{dateFormat(new Date(specialPrice.period_date),'ddd, d mmm yyyy h:MM TT')}</td>

      <td>{specialPrice.price}</td>


      {/* Action */}
      <td>
          <IconMenu
             iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
             anchorOrigin={{horizontal: 'left', vertical: 'top'}}
             targetOrigin={{horizontal: 'left', vertical: 'top'}}
          >


            {permissions.special_price_permissions.edit &&
              <Link to={'/units/'+unitid+'/special_price/edit/'+specialPrice.id} className="MenuItemBtn">
                <MenuItem primaryText="Edit" leftIcon={<ModeEditIcon />} />
              </Link>
            }

            {permissions.special_price_permissions.delete &&
              <MenuItem primaryText="Delete" leftIcon={<DeleteIcon />} onClick={() => onDelete(specialPrice.id)} />
            }
          </IconMenu>
      </td>
    </CSSTransitionGroup>
  )
}
export default RowSpecialPrice
