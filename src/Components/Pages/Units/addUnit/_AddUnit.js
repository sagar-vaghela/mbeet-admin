/*
* AddUnit: Compnent
* Child of: Units Container
*/

// Main Packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

import {addUnit, clearMsg} from '../../../../Redux/actions/unitsActions'
import {cloudinaryUpload, imagePath} from '../../../../Utilities/cloudinaryUpload'

// Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  FilterToolbar,
  FlatsList,
  Loading,
  AlertMsg,} from '../../../GlobalComponents/GlobalComponents'

import AddUnitForm from './AddUnitForm'


// Material UI Icons
import LocationCity       from 'material-ui/svg-icons/social/location-city'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

class AddUnit extends Component{

  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      images: [],
      loading: false,
    }

    this.handleImages = this.handleImages.bind(this)
  }

  handleImages(images){
    this.setState({images})
  }

  handleSubmit(e){
    e.preventDefault()
    const {dispatch} = this.props
    const {images} = this.state
    var formData = new FormData(e.target)

    if(images.length > 0){
      this.setState({loading: true})
      cloudinaryUpload(images, false).then(response => {
        response.map(img => {

          formData.append('pictures_attributes[][name]', imagePath(img.url));
        })
        dispatch(addUnit(formData))
        this.setState({loading: false})
      })

    }else{
      dispatch(addUnit(formData))
    }
  }

  componentWillMount(){
    const {dispatch} = this.props
    dispatch(clearMsg())
  }


  // render
  render(){

    // Store props
    const {fetching, errMsg, succMsg} = this.props
    const {loading} = this.state

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Add New Unit',
      icon: (<LocationCity className="pagetitle-icon"/>),
    }


    return(
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['units','Add New Unit']} />
          <PageHeader {...pageHeaderOptions} />
          <div className="page-container">

            {(fetching || succMsg || loading) && <Loading success={succMsg} />}


            <AddUnitForm handleSubmit={this.handleSubmit} handleImages={this.handleImages} images={this.state.images} />

            {(errMsg) && <AlertMsg error={errMsg} />}

          </div>
        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}


const mapStateToProps = (store) => {
  return {
    errMsg: store.units.errMsg,
    succMsg: store.units.succMsg,
    fetching: store.units.fetching,
  }
}

AddUnit = connect(mapStateToProps)(AddUnit)
export default AddUnit
