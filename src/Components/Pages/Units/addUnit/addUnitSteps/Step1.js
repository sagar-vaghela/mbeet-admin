/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
import RaisedButton from 'material-ui/RaisedButton';

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import { TextValidator, SelectValidator, AutoCompleteValidator} from 'react-material-ui-form-validator'

import AutoComplete from 'material-ui/AutoComplete';

import SimpleMap from './maps'

class Step1 extends Component{

  constructor(props){
    super(props)
    this.state = {
      user_id: null,
      form: {}
    }

    this.handleChangeField = this.handleChangeField.bind(this)
    this.handleChangeUserID = this.handleChangeUserID.bind(this)
  }
  handleChangeUserID(user){
    this.setState({user_id: user.id});
  }
  //handleChangeUserID = (event, index, value) => this.setState({user_id: value});
  handleSelectUsers     = (event, index, value) => this.setState({users: value});

  handleChangeField(event){

    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })

  }

  render(){
    const {users, display} = this.props
    const {form} = this.state
    return(
      <div style={{display: (display ? 'block' : 'none')}}>
        {/* Select User */}
        {users &&
          <div className="form-field">

            <input  name="user_id" value={this.state.user_id} type="text" className="hidden-input" />
            <AutoCompleteValidator
              floatingLabelText="Owner"
              filter={AutoComplete.caseInsensitiveFilter}
              dataSource={users}
              dataSourceConfig={ {text: 'name', value: 'id'}  }
              value={this.state.user_id}
              openOnFocus={true}
              fullWidth={true}
              onNewRequest={this.handleChangeUserID}
              name="uid"
              validators={['required']}
              errorMessages={['this field is required']}
            />

        </div> }

        {/* Title English */}
        <div className="form-field">

         <TextValidator
             floatingLabelText="Title English"
             onChange={this.handleChangeField}
             name="title_en"
             value={form.title_en}
             style={{width: '100%'}}
             validators={['required']}
             errorMessages={['this field is required']}
           />

       </div>

         {/* Title Arabic */}
         <div className="form-field">

           <TextValidator
               floatingLabelText="Title Arabic"
               onChange={this.handleChangeField}
               name="title_ar"
               value={form.title_ar}
               style={{width: '100%'}}
               validators={['required']}
               errorMessages={['this field is required']}
             />
          </div>

        {/* Description English*/}
        <div className="form-field">

        <TextValidator
            floatingLabelText="Description English"
            onChange={this.handleChangeField}
            name="body_en"
            value={form.body_en}
            multiLine={true}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          />

       </div>

        {/* Description Arabic */}
        <div className="form-field">

        <TextValidator
            floatingLabelText="Description Arabic"
            onChange={this.handleChangeField}
            name="body_ar"
            value={form.body_ar}
            multiLine={true}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          />

        </div>

      </div>
    )
  }
}
export default Step1
