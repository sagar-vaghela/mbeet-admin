/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'
// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';

import { TextValidator, SelectValidator} from 'react-material-ui-form-validator'


class Step2 extends Component{
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      unit_status: true,
      unit_enabled: true,
      sub_unit: null,
      form: {
        sub_unit: 0,
      },
    }

    this.handleChangeField = this.handleChangeField.bind(this)
  }

  handleSelectUnitType  = (event, index, value) => this.setState({unit_type: value})
  handleSelectUnitClass  = (event, index, value) => this.setState({unit_class: value})
  hanldeUnitStatus = (event, isInputChecked) => this.setState({unit_status: isInputChecked})
  hanldeUnitEnabled = (event, isInputChecked) => this.setState({unit_enabled: isInputChecked})
  handleSelectSubUnit = (event, index, value) => this.setState({sub_unit: value})


  handleChangeField = (event) => {
    const { form } = this.state;
    form[event.target.id] = event.target.value
    this.setState({ form })
  }


  render(){
    const {display} = this.props
    const {form} = this.state
    return(
      <div style={{display: (display ? 'block' : 'none')}}>

        <div className="form-field">
          <Toggle
            label="Published"
            defaultToggled={this.state.unit_status}
            onToggle={this.hanldeUnitStatus}
            />
            <input type="hidden" name="unit_status" value={this.state.unit_status} />
        </div>

        <div className="form-field">
          <Toggle
            label="User Enabled"
            defaultToggled={this.state.unit_enabled}
            onToggle={this.hanldeUnitEnabled}
            />
            <input type="hidden" name="unit_enabled" value={this.state.unit_enabled} />
        </div>

      {/* Price* */}
      <div className="form-field">

        <TextValidator
            floatingLabelText="Price"
            onChange={this.handleChangeField}
            name="price"
            id="price"
            value={form.price}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          />
      </div>

      {/* Service Charge* */}
      <div className="form-field">

        <TextValidator
            floatingLabelText="Service Charge"
            onChange={this.handleChangeField}
            id="service_charge"
            name="service_charge"
            value={form.service_charge}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          />

      </div>

      {/* Unit type */}
      <div className="form-field">

        <SelectValidator
           floatingLabelText="Select Unit Type"
           value={this.state.unit_type}
           onChange={this.handleSelectUnitType}
           name="unit_type"
           style={{width: '100%'}}
           validators={['required']}
           errorMessages={['this field is required']}
         >

           <MenuItem value={1} primaryText="Resort" />
           <MenuItem value={2} primaryText="Villa" />
           <MenuItem value={3} primaryText="Apartment" />
           <MenuItem value={4} primaryText="Shaleeh" />
           <MenuItem value={5} primaryText="Private Room" />
           <MenuItem value={6} primaryText="Shared Room" />
         </SelectValidator>
         <input type="hidden" name="unit_type" value={this.state.unit_type} />
      </div>

      {/* Unit Class */}
      <div className="form-field">

        <SelectValidator
           floatingLabelText="Unit Class"
           value={this.state.unit_class}
           onChange={this.handleSelectUnitClass}
           name="unit_class"
           style={{width: '100%'}}
           validators={['required']}
           errorMessages={['this field is required']}
         >

           <MenuItem value="A" primaryText="A" />
           <MenuItem value="B" primaryText="B" />
           <MenuItem value="C" primaryText="C" />
         </SelectValidator>
         <input type="hidden" name="unit_class" value={this.state.unit_class} />
      </div>

      {/* Total Area */}
      <div className="form-field">

        <TextValidator
            floatingLabelText="Total Area"
            onChange={this.handleChangeField}
            id="total_area"
            name="total_area"
            value={form.total_area}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          />
      </div>

      <div className="form-field">
        <TextValidator
            floatingLabelText="Number of Subunits"
            onChange={this.handleChangeField}
            name="sub_unit_fake"
            id="sub_unit"
            type="number"
            min="0"
            value={parseInt(form.sub_unit)}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
          />
          <input type="hidden" name="number_of_subunits" value={parseInt(form.sub_unit) === 0 ? 0 : parseInt(form.sub_unit) + 1} />
      </div>


      </div>
    )
  }

}

export default Step2
