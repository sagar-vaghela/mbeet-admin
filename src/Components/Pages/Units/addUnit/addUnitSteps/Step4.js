/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import { TextValidator, SelectValidator} from 'react-material-ui-form-validator'

class Step4 extends Component{

  constructor(props){
    super(props)
    this.state = {
      available_as: null,
      bed_type: null,
      form: {}
    }

    this.handleChangeField = this.handleChangeField.bind(this)
  }

  handleSelectAvailbleAs  = (event, index, value) => this.setState({available_as: value})
  handleSelectBedType     = (event, index, value) => this.setState({bed_type: value})
  handleChangeField(event){

    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })

  }

  render(){
    const {display} = this.props
    const {form} = this.state
    return(
      <div style={{display: (display ? 'block' : 'none')}}>
      {/* Unit Available As */}
      <div className="form-field">

        <SelectValidator
           floatingLabelText="Unit Available As"
           value={this.state.available_as}
           onChange={this.handleSelectAvailbleAs}
           name="available_as"
           style={{width: '100%'}}
           validators={['required']}
           errorMessages={['this field is required']}
         >
           <MenuItem value={1} primaryText="Daily" />
           <MenuItem value={2} primaryText="Weekly" />
           <MenuItem value={3} primaryText="Monthly" />
           <MenuItem value={4} primaryText="Max 3 months" />
         </SelectValidator>
         <input type="hidden" name="available_as" value={this.state.available_as} />
      </div>


      {/* Number of Guests */}
      <div className="form-field">
        <TextValidator
            floatingLabelText="Number of Guests"
            onChange={this.handleChangeField}
            name="number_of_guests"
            value={form.number_of_guests}
            style={{width: '100%'}}
            type="number"
            validators={['required']}
            errorMessages={['this field is required']}
          />

      </div>

      {/* Number of Rooms */}
      <div className="form-field">

        <TextValidator
            floatingLabelText="Number of living rooms"
            onChange={this.handleChangeField}
            name="number_of_rooms"
            value={form.number_of_rooms}
            style={{width: '100%'}}
            type="number"
            validators={['required']}
            errorMessages={['this field is required']}
          />


      </div>

      {/* Number of Beds */}
      <div className="form-field">

      <TextValidator
          floatingLabelText="Number of Beeds"
          onChange={this.handleChangeField}
          name="number_of_beds"
          value={form.number_of_beds}
          style={{width: '100%'}}
          type="number"
          validators={['required']}
          errorMessages={['this field is required']}
        />

      </div>

      {/* Number of Baths */}
      <div className="form-field">

        <TextValidator
            floatingLabelText="Number of Baths"
            onChange={this.handleChangeField}
            name="number_of_baths"
            value={form.number_of_baths}
            style={{width: '100%'}}
            type="number"
            validators={['required']}
            errorMessages={['this field is required']}
          />

      </div>

      <div className="form-field">
        <SelectValidator
           floatingLabelText="Bed Type"
           value={this.state.bed_type}
           onChange={this.handleSelectBedType}
           name="bed_type"
           style={{width: '100%'}}
           validators={['required']}
           errorMessages={['this field is required']}
         >
           <MenuItem value={1} primaryText="Single" />
           <MenuItem value={2} primaryText="Double" />
         </SelectValidator>
         <input type="hidden" name="bed_type" value={this.state.bed_type} />
      </div>


      </div>
    )
  }
}

export default Step4
