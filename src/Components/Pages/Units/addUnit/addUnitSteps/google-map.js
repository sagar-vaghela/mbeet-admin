import React, {Component} from 'react'
import {getLatLngFromData} from '../../../../../Utilities/functions.js'

import './googleMapSearchCss.css'

export default class GettingStartedExample extends Component {

  constructor(){
    super()

  }

  componentDidMount(){
    const {center, onChangePlace, zoom, setAdress} = this.props
    var map = new window.google.maps.Map(document.getElementById('map'), {
          center: center,
          zoom: zoom
        });
    var card  = document.getElementById('pac-card');
    var input = document.getElementById('pac-input');
    var mapURL = document.getElementById('pac-url');
    var types = document.getElementById('type-selector');
    var errMsg = document.getElementById('err-msg');
    var strictBounds = document.getElementById('strict-bounds-selector');

    map.controls[window.google.maps.ControlPosition.TOP_RIGHT].push(card);

    //var autocomplete = new window.google.maps.places.Autocomplete(input);
    var autocomplete = new window.google.maps.places.Autocomplete(input);

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    autocomplete.bindTo('bounds', map);

    var marker = new window.google.maps.Marker({
      map: map,
      position: center,
      anchorPoint: new window.google.maps.Point(0, -29),
      draggable:true,
    });



    mapURL.addEventListener('change', function(e){

      const url = e.target.value


      if(!url) {
        errMsg.style.display = 'none'
        return false
      }

      const center = getLatLngFromData(url)
      if(!center){
        errMsg.style.display = 'block'
      }else{
        errMsg.style.display = 'none'
        marker.setPosition(center)
        map.setCenter(center)
        map.setZoom(17)
        onChangePlace(center.lat, center.lng)

        var place = marker.getPlace();

      }
    })




    marker.addListener('dragend', function(){
      const lat = marker.getPosition().lat()
      const lng = marker.getPosition().lng()
      onChangePlace(lat, lng)
      //map.setCenter(marker.getPosition())

    });

    autocomplete.addListener('place_changed', function() {

      marker.setVisible(false);
      var place = autocomplete.getPlace();



      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }



      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);

      const lat = place.geometry.location.lat()
      const lng = place.geometry.location.lng()
      const address = `${place.name} ${place.formatted_address}`

      setAdress(address)
      onChangePlace(lat, lng)

    });



  }

  render() {
    return (
    <div style={{height: '400px'}}>
      <div className="pac-card" id="pac-card">
        <div>
          <div id="title">
            Search
          </div>
        </div>
        <br />
        <div id="pac-container">
          <input className="pac-input" id="pac-input" type="text" placeholder="Enter a location name" />
        </div>
        <div id="pac-container">
          <input className="pac-input" id="pac-url" type="text" placeholder="Enter URL" />
        </div>
        <div id="pac-container">
          <span className="err-msg" id="err-msg">Invaled URL</span>
        </div>
      </div>

      <div id="map"></div>



    </div>

    );
  }
}
