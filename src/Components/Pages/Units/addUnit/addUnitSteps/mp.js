import React from 'react'
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'

class SimpleForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = { address: '' }
    this.onChange = (address) => this.setState({ address })
  }

  handleFormSubmit = (address) => {
    this.setState({ address })
    geocodeByAddress(this.state.address)
      .then(results => getLatLng(results[0]))
      .then(latLng => this.props.onChange(latLng))
      .catch(error => console.error('Error', error))

  }

  render() {
    const inputProps = {
      value: this.state.address,
      onChange: this.handleFormSubmit,
    }

    return (
      <PlacesAutocomplete inputProps={inputProps} />
    )
  }
}

export default SimpleForm
