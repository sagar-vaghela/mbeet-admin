/*
* EditUnit: Compnent
* Child of: Units Container
*/

// Main Packages
import React, {Component} from 'react'
import {connect} from 'react-redux'

import {updateUnit, getUnit, removeImg} from '../../../../Redux/actions/unitsActions'
import {getUsers, getCurrentUserPermission} from '../../../../Redux/actions/usersActions'
import {getCities} from '../../../../Redux/actions/citiesActions'
import {getRules, getAmenities} from '../../../../Redux/actions/unitsActions'


// Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  FilterToolbar,
  FlatsList,
  Loading,
  AlertMsg,} from '../../../GlobalComponents/GlobalComponents'

import Form from '../Form/Form'
import swal from 'sweetalert'
import {cloudinaryUpload, imagePath} from '../../../../Utilities/cloudinaryUpload'

// Material UI Icons
import LocationCity       from 'material-ui/svg-icons/social/location-city'

// External Packages
import { CSSTransitionGroup } from 'react-transition-group'

class EditUnit extends Component{

  constructor(props){
    super(props)
    this.state = {
      images: [],
      loading: false,
      unit: null,
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleRemoveImg = this.handleRemoveImg.bind(this)
    this.handleImages = this.handleImages.bind(this)
  }

  componentWillMount(){
    const {dispatch, match} = this.props
    const unit_id = match.params.id
    dispatch(getUnit(unit_id))
    dispatch(getUsers(100000, 0, {params: 'role', value:'owner'}))
    dispatch(getCities(10000))
    dispatch(getRules())
    dispatch(getAmenities())
  }

  handleImages(images){
    this.setState({images})
  }

  handleRemoveImg(img_id){
    const {dispatch} = this.props

    const _this = this
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    function(){
      dispatch(removeImg(img_id)).then(() => {
        swal({
          title: 'Deleted!',
          type: 'success',
          timer: 2000,
          showConfirmButton: false
        })
        _this.forceUpdate()
      })
      .catch( err => {
        swal("Error", err, "error")
      })

    })
  }

  handleSubmit(e){
    e.preventDefault()
    const {dispatch, match} = this.props
    const {images} = this.state
    const unit_id = match.params.id
    var formData = new FormData(e.target)

    if(images.length > 0){
      this.setState({loading: true})
      cloudinaryUpload(images, false).then(response => {
        response.map(img => {
          formData.append('pictures_attributes[][name]', imagePath(img.url));
        })
        dispatch(updateUnit(unit_id, formData)).then(() => {
          this.setState({images: []})
        })
        this.setState({loading: false})
      })

    }else{
      dispatch(updateUnit(unit_id, formData))
      dispatch(getCurrentUserPermission());
    }


  }



  // render
  render(){

    // Store props
    const {unit, fetching, errMsg, succMsg, current_user_permissions} = this.props
    const {loading} = this.state



    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Edit Unit',
      icon: (<LocationCity className="pagetitle-icon"/>),
    }

    return(
      <PagesContainer>

        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['units','Edit Unit']} />
          <PageHeader {...pageHeaderOptions} />
          <div className="page-container">
            {(fetching || loading) && <Loading />}

            {unit && <Form
              unit={unit}
              handleSubmit={this.handleSubmit}
              onRemoveImg={this.handleRemoveImg}
              formType="edit"
              users={this.props.users}
              cities={this.props.cities}
              rules={this.props.rules}
              amenities={this.props.amenities}
              images={this.state.images}
              handleImages={this.handleImages}
              permissions={current_user_permissions}
              />}


            {(errMsg || succMsg) && <AlertMsg error={errMsg} success={succMsg} />}

          </div>
        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}


const mapStateToProps = (store) => {
  return {
    unit: store.units.unit,
    errMsg: store.units.errMsg,
    succMsg: store.units.succMsg,
    fetching: store.units.fetching,
    users: store.users.users,
    cities: store.cities.cities,
    rules: store.units.rules,
    amenities: store.units.amenities,
    current_user_permissions: store.users.current_user_permissions,
  }
}

EditUnit = connect(mapStateToProps)(EditUnit)
export default EditUnit
