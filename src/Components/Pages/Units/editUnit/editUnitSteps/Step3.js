/*
* AddUnitForm: Compnent
* Child of: AddUnit
*/

// Main Packages
import React, {Component} from 'react'

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';

import SimpleMap from '../../addUnit/addUnitSteps/maps'

import { TextValidator, SelectValidator, AutoCompleteValidator} from 'react-material-ui-form-validator'
import AutoComplete from 'material-ui/AutoComplete';

import GoogleMap from '../../addUnit/addUnitSteps/google-map'

class Step3 extends Component{

  constructor(props){
    super(props)

    this.state = {
      city: props.unit.city.id,
      searchText: props.unit.city.name,
      center : {
        lat: parseFloat(props.unit.latitude),
        lng: parseFloat(props.unit.longitude),
      },
      zoom: 17,
    }

    this.handleSelectLatLng = this.handleSelectLatLng.bind(this)
    this.handleOnChangePlace = this.handleOnChangePlace.bind(this)
  }

  handleSelectCity  = (city) => this.setState({city: city.id, searchText: city.name});

  handleOnChangePlace(lat, lng){

    this.setState({center: {
      lat,
      lng
    }})

  }

  handleSelectLatLng(position){
    this.setState({lat: position.lat, lng: position.lng})
  }


  render(){
    const {display, cities, unit} = this.props
    return(
      <div style={{visibility: (display ? 'visible' : 'hidden'), height: (display ? 'auto' : '0px')}}>
        {cities &&
        <div className="form-field">
          <AutoCompleteValidator
            floatingLabelText="City"
            filter={AutoComplete.caseInsensitiveFilter}
            dataSource={cities}
            dataSourceConfig={ {text: 'name', value: 'id'}  }
            value={this.state.city}
            searchText={this.state.searchText}
            openOnFocus={true}
            fullWidth={true}
            onNewRequest={this.handleSelectCity}
            name="city"
            validators={['required']}
            errorMessages={['this field is required']}
          />

           <input style={{display: 'none'}} type="text" name="city_id" value={this.state.city} />
        </div> }

        {/* Address* */}
        <div className="form-field">
            <TextField
            floatingLabelText="Address"
             style={{width: '100%'}}
             name="address"
             defaultValue={unit.address}
           />
        </div>


        <div className="form-field">
          <GoogleMap
            onChangePlace={this.handleOnChangePlace}
            center={this.state.center}
            zoom={this.state.zoom}
            />

            <input type="hidden" name="latitude" value={this.state.center.lat} />
            <input type="hidden" name="longitude" value={this.state.center.lng}   />
        </div>


      </div>

    )
  }
}

export default Step3
