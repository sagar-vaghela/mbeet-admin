import React, {Component} from 'react'
import {connect} from 'react-redux'

import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading} from '../../../GlobalComponents/GlobalComponents'

// Units Redux Actions
import {getUnit} from '../../../../Redux/actions/unitsActions'

  // External Packages
import { CSSTransitionGroup } from 'react-transition-group'

import UnitDetailes from './UnitDetailes'
// Material Icon
import LocationCity       from 'material-ui/svg-icons/social/location-city'

class ShowUnit extends Component{

  componentWillMount(){
    const {dispatch, match} =  this.props
    const unit_id = match.params.id
    dispatch(getUnit(unit_id))
  }

  render(){

    // Store props
    const {unit, fetching, errMsg} = this.props

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Show Unit',
      icon: (<LocationCity className="pagetitle-icon"/>),
    }

    // Trassiosn Options
    const TrassiosnOptions = {
      transitionName: 'example',
      transitionAppear: true,
      transitionAppearTimeout: 500,
      transitionEnterTimeout: 500,
      transitionLeaveTimeout: 300,
    }

    return(
      <PagesContainer>
        <CSSTransitionGroup {...TrassiosnOptions}>
          <Breadcrumb path={['units','show unit']} />
          <PageHeader {...pageHeaderOptions} />
          <div className="page-container">
            {(fetching || errMsg) && <Loading error={errMsg} />}
            {unit && <UnitDetailes unit={unit} />}
          </div>
        </CSSTransitionGroup>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    unit: store.units.unit,
    errMsg: store.units.errMsg,
    fetching: store.units.fetching,
  }
}

ShowUnit = connect(mapStateToProps)(ShowUnit)
export default ShowUnit
