import React from 'react'
import dateFormat  from 'dateformat'

const DisabledUnits = (props) => {
  const {unit} = props
  const dis_units = unit.units_disabled

  return (
    <div>
    {dis_units.length > 0 ? (
      <table className="table">
        <thead>
          <tr>
            <th>Date</th>
            <th>Disabled Units</th>
            <th>Enabled Units</th>
          </tr>
        </thead>

        <tbody>
          {dis_units.map((disUnit, index) => {
            return (
              <tr key={'disun'+index}>
                <td>{dateFormat(new Date(disUnit.day_disabled),'dddd, mmmm dS, yyyy')}</td>
                <td>{disUnit.number_of_disabled_units}</td>
                <td>{disUnit.number_of_enabled_units}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    ):(
      <div className="alert alert-danger">
        There are no disabled units on this unit
      </div>
    )}

    </div>
  )
}
export default DisabledUnits
