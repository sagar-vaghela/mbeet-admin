import React from 'react'
import {
  unitType,
  availableAs,
  unitStatus,
} from '../../../../../Utilities/options'
import { Link }           from 'react-router-dom'

const UnitInfo = (props) => {
  const {unit} = props
  const stars = [1,2,3,4,5]
  return (
    <table className="table table-hoverd">
      <tr>
        <td style={{width: '200px'}}>ID</td>
        <td>{unit.id}</td>
      </tr>
      <tr>
        <td>Owner</td>
        <td>
          <Link to={'/users/show/'+unit.owner.user_id}>
            {unit.owner.name}
          </Link>
        </td>
      </tr>
      <tr>
        <td>Title English</td>
        <td>{unit.title_en}</td>
      </tr>

      <tr>
        <td>Title Arabic</td>
        <td>{unit.title_ar}</td>
      </tr>

      <tr>
        <td>Description English</td>
        <td>{unit.title_en}</td>
      </tr>

      <tr>
        <td>Description Arabic</td>
        <td>{unit.title_ar}</td>
      </tr>

      <tr>
        <td>Number of Guests</td>
        <td>{unit.number_of_guests}</td>
      </tr>

      <tr>
        <td>Number of Rooms</td>
        <td>{unit.number_of_rooms}</td>
      </tr>

      <tr>
        <td>Number of Beds</td>
        <td>{unit.number_of_beds}</td>
      </tr>

      <tr>
        <td>Number of Baths</td>
        <td>{unit.number_of_baths}</td>
      </tr>

      <tr>
        <td>Available as</td>
        <td>{availableAs[unit.available_as]}</td>
      </tr>

      <tr>
        <td>City</td>
        <td>{unit.city.name}</td>
      </tr>

      <tr>
        <td>Address</td>
        <td>{unit.address}</td>
      </tr>

      <tr>
        <td>Price</td>
        <td>{unit.price}</td>
      </tr>

      <tr>
        <td>Service Charge</td>
        <td>{unit.service_charge}</td>
      </tr>

      <tr>
        <td>Unit Class</td>
        <td>{unit.unit_class}</td>
      </tr>

      <tr>
        <td>Unit Type</td>
        <td>{unitType[unit.unit_type]}</td>
      </tr>

      <tr>
        <td>Total area</td>
        <td>{unit.total_area}</td>
      </tr>

      <tr>
        <td>Created at</td>
        <td>{unit.created_at}</td>
      </tr>

      <tr>
        <td>Rating</td>
        <td>
          <p className="unit-rating" style={{marginBottom: '0'}}>
            {stars.map(star => {
              const starClass = (star <= unit.avg_star) ? 'fa fa-star' : 'fa fa-star-o'
              return (<i key={'star'+star} className={starClass} aria-hidden="true"></i>)
            })}
          </p>
        </td>
      </tr>

      <tr>
        <td>Unit Status</td>
        <td>{unit.unit_status ? 'Published' : 'Waiting For approval'}</td>
      </tr>

      <tr>
        <td>Unit Enabled</td>
        <td>{unit.unit_enabled ? 'Enabled' : 'Disabled'}</td>
      </tr>

      {unit.message &&
        <tr>
          <td>Reason for Last Updated</td>
          <td>{unit.message}</td>
        </tr>
      }
    </table>
  )
}
export default UnitInfo
