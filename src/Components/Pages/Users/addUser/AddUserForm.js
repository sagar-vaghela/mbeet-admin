/*
* AddUserForm: Object
* Child of: AddUser
*/

// Main Packages
import React, {Component} from 'react'

import {cloudinaryUpload, imagePath, pathToUrl} from '../../../../Utilities/cloudinaryUpload'
import {countryCode} from '../../../../Utilities/countries_code'
// External Packages
import serializeForm from 'form-serialize'

import {Loading} from '../../../GlobalComponents/GlobalComponents'

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';

import { ValidatorForm, TextValidator, SelectValidator, DateValidator} from 'react-material-ui-form-validator'

class AddUserForm extends Component {
  constructor(props){
    super(props)

    this.state = {
      userRole: null,
      image: null,
      loading: false,
      country_code: null,
      email_verified: false,
      phone_verified: false,
      form: {},
    }

    this.handleUpload = this.handleUpload.bind(this)
    this.handleCountryCode = this.handleCountryCode.bind(this)
    this.hanldeEmailverified = this.hanldeEmailverified.bind(this)
    this.hanldePhoneverified = this.hanldePhoneverified.bind(this)
    this.handleChangeField = this.handleChangeField.bind(this)
  }

  handleChangeField(event){
    const { form } = this.state;
    form[event.target.name] = event.target.value
    this.setState({ form })
    console.log(this.state.form);
  }

  handleUpload(e){
    e.preventDefault()
    const file = e.target.files

    this.setState({loading: true})

    cloudinaryUpload(file).then(img => {
      const imgURL = imagePath(img[0].url)
      this.setState({image: imgURL, loading: false})
    })



  }
  handleChange = (event, index, value) => this.setState({userRole: value});
  handleCountryCode = (event, index, value) => this.setState({country_code: value})
  hanldeEmailverified = (event, isInputChecked) => this.setState({email_verified: isInputChecked})
  hanldePhoneverified = (event, isInputChecked) => this.setState({phone_verified: isInputChecked})

  render(){
    const {form} = this.state
    const {roles} = this.props
    return(
      <ValidatorForm ref="form" onSubmit={this.props.handleSubmit}>

        {this.state.loading && <Loading />}
        <div className="form-field">
          <SelectValidator

             floatingLabelText="Select Role"
             value={this.state.userRole}
             onChange={this.handleChange}
             name="userrole"
             style={{width: '100%'}}
             validators={['required']}
             errorMessages={['this field is required']}
           >
             {roles && roles.map(role => {
               return (<MenuItem
                         key={role.id}
                         value={role.id}
                         primaryText={role.name} />)
             })}
           </SelectValidator>
           <input  name="role_id" value={this.state.userRole? this.state.userRole: ''} type="hidden"/>
        </div>

        <div className="form-field">
          <TextValidator
              floatingLabelText="Name"
              onChange={this.handleChangeField}
              name="name"
              value={form.name}
              style={{width: '100%'}}
              validators={['required']}
              errorMessages={['this field is required']}
            />
        </div>


         <div className="form-field">


         <TextValidator
             floatingLabelText="Email"
             onChange={this.handleChangeField}
             name="email"
             value={form.email}
             style={{width: '100%'}}
             validators={['required', 'isEmail']}
             errorMessages={['this field is required', 'email is not valid']}
           />


        </div>

        <div className="form-field">

          <SelectValidator
             floatingLabelText="Select Country"
             value={this.state.country_code}
             onChange={this.handleCountryCode}
             name="country_code"
             style={{width: '100%'}}
             validators={['required']}
             errorMessages={['this field is required']}
           >
            {countryCode.map(country => {
              return (<MenuItem
                        key={country.name}
                        value={country.dial_code}
                        primaryText={country.name} />)
            })}

           </SelectValidator>
           <input  name="country_code" value={this.state.country_code? this.state.country_code : ''} type="text" style={{display: 'none'}} />

          <TextValidator
            floatingLabelText="Phone"
            onChange={this.handleChangeField}
            name="phone"
            type="number"
            value={form.phone}
            style={{width: '100%'}}
            validators={['required']}
            errorMessages={['this field is required']}
         />
        </div>

        <div className="form-field">
          <DateValidator

              hintText="Date of birth "
              container="inline"
              name="date_of_birth"
              mode="landscape"

              style={{color: 'red'}} />
        </div>

        <div className="form-field">
          <label>Image </label>
          {this.state.image &&
            <span>
              <img src={pathToUrl(this.state.image)} width="100" height="110" />
              <input type="hidden" name="picture_attributes[name]" value={this.state.image} />
            </span>
          }
          <br />
          <input type="file" name="file" onChange={this.handleUpload} />

        </div>

        <div className="form-field">
          <label>Gender</label>
          <RadioButtonGroup name="gender" defaultSelected="not_light">
              <RadioButton
                value="male"
                label="Male"

              />
              <RadioButton
                value="female"
                label="Female"

              />

          </RadioButtonGroup>
        </div>

        <div className="form-field">
            <Toggle
              label="Email Verified"
              defaultToggled={this.state.email_verified}
              onToggle={this.hanldeEmailverified}
              />
              <input type="hidden" name="email_verified" value={this.state.email_verified} />
        </div>

        <div className="form-field">
            <Toggle
              label="Phone Verified"
              defaultToggled={this.state.phone_verified}
              onToggle={this.hanldePhoneverified}
              />
              <input type="hidden" name="phone_verified" value={this.state.phone_verified} />
        </div>

        <hr />

        <div className="form-field">
          <TextField
           hintText="Facebook ID"
           style={{width: '100%'}}
           type="text"
           name="facebook_uid"
         />
        </div>


        <div className="form-field">
          <TextField
           hintText="Twitter ID"
           style={{width: '100%'}}
           type="text"
           name="twitter_uid"
         />
        </div>

        <div className="form-field">
          <TextField
           hintText="Gmail ID"
           style={{width: '100%'}}
           type="text"
           name="gmail_uid"
         />
        </div>



      <div className="form-field submit">
        <RaisedButton label="Create User" type="submit" primary={true} />
      </div>

      </ValidatorForm>
    )
  }

}

export default AddUserForm;
