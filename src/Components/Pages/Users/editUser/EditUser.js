import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Redirect } from 'react-router'
import swal from 'sweetalert'
// Users Redux Actions
import {getUser, editUser} from '../../../../Redux/actions/usersActions'
import { getRoles } from '../../../../Redux/actions/rolesActions'

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading,
  AlertMsg} from '../../../GlobalComponents/GlobalComponents'

import EditUserForm from './EditUserForm'

// Material UI Icons
import People             from 'material-ui/svg-icons/social/people'

class EditUser extends Component{

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      redirect: false,
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentWillMount(){

    const {dispatch, match} = this.props
    const userid = match.params.id

    dispatch(getUser(userid)).then(() => this.setState({loading: false}))
    dispatch(getRoles());
  }

  handleSubmit(e){
    e.preventDefault()
    const {dispatch, match, succMsg} = this.props
    const userid = match.params.id
    var formData = new FormData(e.target)
    let obj = e.target
    dispatch(editUser(userid, formData)).then(() => {
      const _this = this
      obj.reset()
      swal({
        title: "User updated successfully",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Back to users list",
        closeOnConfirm: true,
      },
      function(){
        _this.setState({redirect: true})
      });
    })

  }

  // render
  render(){

    // Store props
    const {user, errMsg, succMsg, fetching, roles} = this.props

    // Pageheader Options
    const pageHeaderOptions = {
      title: 'Edit User',
      icon: (<People className="pagetitle-icon"/>),
    }


    return(
      <PagesContainer path={this.props.location}>
        <Breadcrumb path={['users','edit: '+(user ? user.name : '')]} />
        <PageHeader {...pageHeaderOptions} />

        {this.state.redirect && <Redirect to="/users" />}

        <div className="page-container">
          {(fetching) && <Loading />}

          <div className="row justify-content-center">
            <div  className="col-lg-8 col-md-8">
              {(user && !this.state.loading) && <EditUserForm handleSubmit={this.handleSubmit} user={user} roles={roles}/>}
              {(errMsg) && <AlertMsg error={errMsg} />}
            </div>
          </div>

        </div>
      </PagesContainer>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    user: store.users.user,
    roles: store.roles.roles,
    errMsg: store.users.errMsg,
    succMsg: store.users.succMsg,
    fetching: store.users.fetching,
  }
}

EditUser = connect(mapStateToProps)(EditUser)
export default EditUser
