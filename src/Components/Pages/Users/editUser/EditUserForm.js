/*
* AddUserForm: Object
* Child of: AddUser
*/

// Main Packages
import React, {Component} from 'react'
import {cloudinaryUpload, imagePath, pathToUrl} from '../../../../Utilities/cloudinaryUpload'
import {countryCode} from '../../../../Utilities/countries_code'
import {Loading} from '../../../GlobalComponents/GlobalComponents'
// External Packages
import serializeForm from 'form-serialize'

// Material UI
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';


class EditUserForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      userRole: this.props.user.role.id,
      image: (this.props.user.picture_attributes) ? imagePath(this.props.user.picture_attributes.name) : null,
      loading: false,
      country_code: this.props.user.country_code,
      email_verified: this.props.user.email_verified,
      phone_verified: this.props.user.phone_verified,
    }

    this.handleUpload = this.handleUpload.bind(this)
    this.handleCountryCode = this.handleCountryCode.bind(this)
    this.hanldeEmailverified = this.hanldeEmailverified.bind(this)
    this.hanldePhoneverified = this.hanldePhoneverified.bind(this)
  }


  handleChange = (event, index, value) => this.setState({userRole: value});
  handleCountryCode = (event, index, value) => this.setState({country_code: value})
  hanldeEmailverified = (event, isInputChecked) => this.setState({email_verified: isInputChecked})
  hanldePhoneverified = (event, isInputChecked) => this.setState({phone_verified: isInputChecked})


  handleUpload(e){
    e.preventDefault()
    const files = e.target.files
    this.setState({loading: true})
    //console.log(files)
    cloudinaryUpload(files)
    .then(result => {
      let url = imagePath(result[0].url)
      this.setState({image: url, loading: false, img_view: result[0].url})
    })
    .catch(err => console.log(err))

  }

  render(){
    const {user, roles} = this.props
    return(
      <form onSubmit={this.props.handleSubmit}>

        <div className="form-field">
          <SelectField
             floatingLabelText="Select Role"
             value={this.state.userRole}
             onChange={this.handleChange}
             name="userrole"
             style={{width: '100%'}}
           >
           {roles && roles.map(role => {
             return (<MenuItem
                       key={role.id}
                       value={role.id}
                       primaryText={role.name} />)
           })}
           </SelectField>
           <input name="role_id" value={this.state.userRole} type="hidden" />
        </div>

       <div className="form-field">
         <TextField
         required
          hintText="Name"
          style={{width: '100%'}}
          name="name"
          defaultValue={user.name}
        />
      </div>

      <div className="form-field">
        <TextField
         hintText="Email"
         style={{width: '100%'}}
         type="email"
         name="email"
         defaultValue={user.email}
       />
      </div>



      <div className="form-field">

        <SelectField
          required
           floatingLabelText="Select Country"
           value={this.state.country_code}
           onChange={this.handleCountryCode}
           name="country_code"
           style={{width: '100%'}}
         >
          {countryCode.map(country => {
            return (<MenuItem
                      key={country.name}
                      value={country.dial_code}
                      primaryText={country.name} />)
          })}

         </SelectField>
         <input required name="country_code" value={this.state.country_code} type="text" style={{display: 'none'}} />

        <TextField
          required
          hintText="Phone"
          style={{width: '100%'}}
          type="number"
          name="phone"
          defaultValue={user.phone}
       />
      </div>

      <div className="form-field">
        <DatePicker
            required
            hintText="Date of birth "
            container="inline"
            name="date_of_birth"
            mode="landscape"
            defaultDate={new Date(user.date_of_birth)}
            style={{color: 'red'}} />
      </div>


      <div className="form-field">
        <label>Image </label>
        <img src={pathToUrl(this.state.image)} width="100" height="100" /><br />
        <input type="file" name="file" onChange={this.handleUpload} />
        <input type="hidden" name="picture_attributes[name]" value={this.state.image? this.state.image : ''} />
      </div>

      <div className="form-field">
        <label>Gender</label>
        <RadioButtonGroup name="gender" defaultSelected={user.gender}>
            <RadioButton
              value="male"
              label="Male"
            />
            <RadioButton
              value="female"
              label="Female"
            />

        </RadioButtonGroup>
      </div>

      <div className="form-field">
          <Toggle
            label="Email Verified"
            defaultToggled={this.state.email_verified}
            onToggle={this.hanldeEmailverified}
            />
            <input type="hidden" name="email_verified" value={this.state.email_verified} />
      </div>

      <div className="form-field">
          <Toggle
            label="Phone Verified"
            defaultToggled={this.state.phone_verified}
            onToggle={this.hanldePhoneverified}
            />
            <input type="hidden" name="phone_verified" value={this.state.phone_verified} />
      </div>

      <hr />

      <div className="form-field">
        <TextField
         hintText="Facebook ID"
         style={{width: '100%'}}
         type="text"
         name="facebook_uid"
         defaultValue={user.facebook_uid}
       />
      </div>


      <div className="form-field">
        <TextField
         hintText="Twitter ID"
         style={{width: '100%'}}
         type="text"
         name="twitter_uid"
         defaultValue={user.twitter_uid}
       />
      </div>

      <div className="form-field">
        <TextField
         hintText="Gmail ID"
         style={{width: '100%'}}
         type="text"
         name="gmail_uid"
         defaultValue={user.gmail_uid}
       />
      </div>

      <div className="form-field submit">
        <RaisedButton label="Update" type="submit" primary={true} />
      </div>

      </form>
    )
  }

}

export default EditUserForm;
