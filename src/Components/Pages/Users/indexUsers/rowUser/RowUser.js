// Main Packages
import React from 'react';
import { Link }           from 'react-router-dom'

// Material UI
import MenuItem from 'material-ui/MenuItem';
import IconMenu     from 'material-ui/IconMenu';
import IconButton   from 'material-ui/IconButton';
import Avatar from 'material-ui/Avatar';

// Material UI Icons
import DoneIcon           from 'material-ui/svg-icons/action/done';
import ClearIcon          from 'material-ui/svg-icons/content/clear';
import DeleteIcon         from 'material-ui/svg-icons/action/delete';
import ModeEditIcon       from 'material-ui/svg-icons/editor/mode-edit';
import RemoveRedEyeIcon   from 'material-ui/svg-icons/image/remove-red-eye';
import MoreVertIcon       from 'material-ui/svg-icons/navigation/more-vert';

// Style
import './rowUser.css'

import { CSSTransitionGroup } from 'react-transition-group'

const RowUser = (props) => {
  const {user, onDelete, restoreUser, permissions} = props
  const avatar = (user.picture_attributes) ? user.picture_attributes.name : './images/user-avatar.png'
  return (
    <CSSTransitionGroup
      component="tr"
transitionName="example"
transitionAppear={true}
transitionAppearTimeout={5000}
transitionEnter={false}
transitionLeave={false}>
      {/* ID */}
      <td>{user.id}</td>

      {/* User */}
      <td width="250">
        <Avatar
            src={avatar}
            size={40}
            className="user-avatar"
            />
          <Link to={'/users/show/'+user.id} className="username-with-avatar">
            {user.name}
          </Link>
          <span className="user-role" data-value={user.role.name}>
            {user.role.name}
          </span>
      </td>

      {/* Email */}
      <td>{user.email}</td>

      {/* Phone */}
      <td>{user.phone}</td>

      {/* Email verified */}
      <td>
        {user.email_verified ? (
          <DoneIcon className="done-icon" />
        ):(
          <ClearIcon className="clear-icon" />
        )}
      </td>

      {/* Phone verified */}
      <td>
        {user.phone_verified ? (
          <DoneIcon className="done-icon" />
        ):(
          <ClearIcon className="clear-icon" />
        )}
      </td>

      {/* Action */}
      <td>
        {!user.soft_delete ? (
          <IconMenu
             iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
             anchorOrigin={{horizontal: 'left', vertical: 'top'}}
             targetOrigin={{horizontal: 'left', vertical: 'top'}}
          >

            {permissions.users_permissions.show &&
              <Link to={'/users/show/'+user.id} className="MenuItemBtn">
                <MenuItem primaryText="View" leftIcon={<RemoveRedEyeIcon />} />
              </Link>
            }

            {permissions.users_permissions.edit &&
              <Link to={'/users/edit/'+user.id} className="MenuItemBtn">
                <MenuItem primaryText="Edit" leftIcon={<ModeEditIcon />} />
              </Link>
            }

            {permissions.users_permissions.restore &&
              <MenuItem primaryText="Delete" leftIcon={<DeleteIcon />} onClick={() => onDelete(user.id)} />
            }
          </IconMenu>
        ):(
          <div>
          {permissions.users_permissions.restore &&
            <a href="#" onClick={(e) => restoreUser(e, user.id)}>
              <DeleteIcon className="deleted-icon" />
            </a>
          }</div>
        )}

      </td>
    </CSSTransitionGroup>
  )
}
export default RowUser
