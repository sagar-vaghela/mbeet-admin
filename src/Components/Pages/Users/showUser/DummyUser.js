import React from 'react'
import { Link }           from 'react-router-dom'

// External Packages
import ReactTooltip from 'react-tooltip'

// Material UI Icons
import Avatar from 'material-ui/Avatar';
import {Tabs, Tab} from 'material-ui/Tabs';
import Badge from 'material-ui/Badge';

// Material UI Icons
import People             from 'material-ui/svg-icons/social/people'
import LocationCityIcon   from 'material-ui/svg-icons/social/location-city'
import CreditCardIcon   from 'material-ui/svg-icons/action/credit-card'
import ModeEditIcon       from 'material-ui/svg-icons/editor/mode-edit';
import AssignmentIndIcon       from 'material-ui/svg-icons/action/assignment-ind';
import ShoppingCartIcon       from 'material-ui/svg-icons/action/shopping-cart';
import FavoriteIcon       from 'material-ui/svg-icons/action/favorite';
import DoneIcon           from 'material-ui/svg-icons/action/done';
import ClearIcon          from 'material-ui/svg-icons/content/clear';

const DummyUser = () => {

  const spanStyle = {
    background: '#ccc',
    display: 'inline-block',
    width: '70%',
    height: '10px',
    borderRadius: '5px'
  }

  return (
  <div className="page-container clearfix">
    <ReactTooltip place="top" type="dark" effect="float"/>

    <div className="row">
      <div className="col-lg-6 col-md-6">
        <div className="row">

          <div className="col-lg-4 col-md-4">
            <Avatar
              src="http://static.tumblr.com/9e2c9a2cb793806c25c0c63ba9430a34/9fbywyd/ODQnetvej/tumblr_static_d4d6grnpo3k0c8800gw4w8gws.jpg"
              size={140}
              className="user-avatar"
              />
          </div>
          <div className="col-lg-8 col-md-8">
            <h3 className="user-name">
              <span style={spanStyle}></span>
            </h3>
            <span style={spanStyle}></span>
            <span style={spanStyle}></span>
            <span style={spanStyle}></span>
          </div>

        </div>
      </div>

      <div className="col-lg-4 col-md-6 push-md-2">

        <div className="statics">
          <div className="statics-box">

            <span style={spanStyle}></span>
              <Avatar
                src="http://static.tumblr.com/9e2c9a2cb793806c25c0c63ba9430a34/9fbywyd/ODQnetvej/tumblr_static_d4d6grnpo3k0c8800gw4w8gws.jpg"
                size={30}
                />
          </div>

          <div className="statics-box">
            <span style={spanStyle}></span>
              <Avatar
                src="http://static.tumblr.com/9e2c9a2cb793806c25c0c63ba9430a34/9fbywyd/ODQnetvej/tumblr_static_d4d6grnpo3k0c8800gw4w8gws.jpg"
                size={30}
                />
          </div>
          <div className="statics-box">
            <span style={spanStyle}></span>
              <Avatar
                src="http://static.tumblr.com/9e2c9a2cb793806c25c0c63ba9430a34/9fbywyd/ODQnetvej/tumblr_static_d4d6grnpo3k0c8800gw4w8gws.jpg"
                size={30}
                />
          </div>
          <div className="statics-box">
            <span style={spanStyle}></span>
              <Avatar
                src="http://static.tumblr.com/9e2c9a2cb793806c25c0c63ba9430a34/9fbywyd/ODQnetvej/tumblr_static_d4d6grnpo3k0c8800gw4w8gws.jpg"
                size={30}
                />
          </div>
        </div>

      </div>
    </div>

    <div className="row user-tabs">
      <div className="col-lg-3" style={{textAlign:'center'}}>

          <Avatar
            src="http://static.tumblr.com/9e2c9a2cb793806c25c0c63ba9430a34/9fbywyd/ODQnetvej/tumblr_static_d4d6grnpo3k0c8800gw4w8gws.jpg"
            size={30}
            /><br />
          <span style={spanStyle}></span>
      </div>
      <div className="col-lg-3" style={{textAlign:'center'}}>

          <Avatar
            src="http://static.tumblr.com/9e2c9a2cb793806c25c0c63ba9430a34/9fbywyd/ODQnetvej/tumblr_static_d4d6grnpo3k0c8800gw4w8gws.jpg"
            size={30}
            /><br />
          <span style={spanStyle}></span>
      </div>
      <div className="col-lg-3" style={{textAlign:'center'}}>

          <Avatar
            src="http://static.tumblr.com/9e2c9a2cb793806c25c0c63ba9430a34/9fbywyd/ODQnetvej/tumblr_static_d4d6grnpo3k0c8800gw4w8gws.jpg"
            size={30}
            /><br />
          <span style={spanStyle}></span>
      </div>
      <div className="col-lg-3" style={{textAlign:'center'}}>

          <Avatar
            src="http://static.tumblr.com/9e2c9a2cb793806c25c0c63ba9430a34/9fbywyd/ODQnetvej/tumblr_static_d4d6grnpo3k0c8800gw4w8gws.jpg"
            size={30}
            /><br />
          <span style={spanStyle}></span>
      </div>
    </div>

  </div>
)
}
export default DummyUser
