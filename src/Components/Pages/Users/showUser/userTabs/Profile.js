import React from 'react'

// Material UI Icons
import DoneIcon           from 'material-ui/svg-icons/action/done';
import ClearIcon          from 'material-ui/svg-icons/content/clear';

import dateFormat  from 'dateformat'

const Profile = (props) => {

  const {user} = props

  return (
    <table className="table profile">
      <tbody>
        <tr>
          <td>Name</td>
          <td>{user.name}</td>
        </tr>
        <tr>
          <td>Gender</td>
          <td>{user.gender}</td>
        </tr>
        <tr>
          <td>Role</td>
          <td>{user.role.name}</td>
        </tr>
        <tr>
          <td>E-mail</td>
          <td>{user.email}</td>
        </tr>

        <tr>
          <td>Email Verified</td>
          <td>
            {user.email_verified ? (
              <DoneIcon style={{color: '#25c1ad'}} />
            ):(
              <ClearIcon style={{color: '#f35071'}} />
            )}
          </td>
        </tr>
        <tr>
          <td>Phone Verified</td>
            <td>
              {user.phone_verified ? (
                <DoneIcon style={{color: '#25c1ad'}} />
              ):(
                <ClearIcon style={{color: '#f35071'}} />
              )}
            </td>
        </tr>

        <tr>
          <td>Date of Birth</td>
          <td>
          {user.date_of_birth ?
            dateFormat(new Date(user.date_of_birth),'dddd, mmmm dS, yyyy')
            : 'N/A'
          }</td>
        </tr>
        <tr>
          <td>Facebook</td>
          <td>
            {user.facebook_uid ? (
              <a target="_blank" href={'http://fb.com/'+user.facebook_uid}>{user.facebook_uid}</a>
            ):(
              'N/A'
            )}

          </td>
        </tr>
        <tr>
          <td>Twitter</td>
          <td>
            {user.twitter_uid ? (
              <a target="_blank" href={'http://twitter.com/'+user.twitter_uid}>{user.twitter_uid}</a>
            ):(
              'N/A'
            )}
          </td>
        </tr>
        <tr>
          <td>Google Plus</td>
            <td>
              {user.gmail_uid ? (
                <a target="_blank" href={'http://plus.google.com/'+user.gmail_uid}>{user.gmail_uid}</a>
              ):(
                'N/A'
              )}
            </td>
        </tr>
        <tr>
          <td>Joined</td>
          <td>
          {user.created_at ?
            dateFormat(new Date(user.created_at),'dddd, mmmm dS, yyyy, h:MM TT')
            : 'N/A'
          }</td>

        </tr>
      </tbody>
    </table>
  )
}
export default Profile
