/****************
*   Bookings Actions
*
*   @GET_Bookings
*   @GET_Booking
*
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

/*
*   getUsers - fetch users
*/
export function getBookings(limit = 10, offset = 0, fltr = null, sort_by='created_at', sort_direction='desc', search = '', dateFilter = null){

  let url           = '/admin/'+API_VERSION+'/bookings?limit='+limit+'&offset='+offset
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by='+sort_by+'&sort_direction='+sort_direction+'&search='+search

  if(fltr)
    url += '&'+fltr.params+'='+fltr.value

  if(dateFilter){
    if(dateFilter.from && dateFilter.to)
      url += `&type=${dateFilter.type}&from=${dateFilter.from}&to=${dateFilter.to}`
  }

  return {
    type: 'GET_BOOKINGS',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/***************************
* Get Booking
****************************/
export function getBooking(bookingid){

  const url           = '/admin/'+API_VERSION+'/bookings/'+bookingid
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}


  return {
    type: 'GET_BOOKING',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/***************************
* Cancel Booking
****************************/
export function cancelBooking(bookingid){

  const url           = '/admin/'+API_VERSION+'/bookings/'+bookingid+'/cancel'
  const method        = 'DELETE'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}


  return {
    type: 'CANCEL_BOOKING',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*******************
* Users Search
*
* @s: (String)
********************/
export function bookingsSearch(s){

  let url           = '/admin/'+API_VERSION+'/bookings?search='+s
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}


  url += '&sort_by=id&sort_direction=desc&limit=10'

  return {
    type: 'SEARCH_BOOKINGS',
    payload: handleAPI(url, method, false, Authorization)
  }

}


/*
*   Add Booking
*
*   @dataForm
*/
export function addBooking(data){

  const url           = '/admin/'+API_VERSION+'/bookings'
  const method        = 'POST'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'ADD_BOOKING',
    payload: handleAPI(url, method, data, Authorization)
  }

}
