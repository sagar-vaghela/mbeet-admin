/****************
*   Coupons Actions
*
*   @GET_COUPONS
*   @GET_COUPON
*   @ADD_COUPON
*   @DELETE_COUPON
*   @UPDATE_COUPON
*
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

/*
*   getCoupons - fetch coupons
*/
// export function getCoupons(limit = 10, offset = 0, fltr = null, sort_by = 'id', sort_direction = 'asc', search = ''){
export function getCoupons(limit = 10, offset = 0, sort_by = 'id', sort_direction = 'asc', search = ''){

  let url           = '/admin/'+API_VERSION+'/coupons?limit='+limit+'&offset='+offset
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by='+sort_by+'&sort_direction='+sort_direction+'&search='+search
  //
  // if(fltr)
  //   url += '&'+fltr.params+'='+fltr.value

  return {
    type: 'GET_COUPONS',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   Get Coupon
*
*   @couponid
*/
export function getCoupon(couponid){

  const url           = '/admin/'+API_VERSION+'/coupons/'+couponid
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}



  return {
    type: 'GET_COUPON',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   GET Coupon
*
*   @couponid: (Intger)
*/
export function addCoupon(data){

  const url           = '/admin/'+API_VERSION+'/coupons'
  const method        = 'POST'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'ADD_COUPON',
    payload: handleAPI(url, method, data, Authorization)
  }

}


/*
*   Delete Coupon
*
*   @couponid: (Intger)
*/
export function deleteCoupon(couponid){

  const url           = '/admin/'+API_VERSION+'/coupons/'+couponid
  const method        = 'DELETE'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'DELETE_COUPON',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(couponid)
      }).catch(err => {
        reject(err)
      })
    })
  }

}

export function restoreCoupon(couponid){

  const url           = '/admin/'+API_VERSION+'/coupon_restores/'+couponid
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'RESTORE_COUPON',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(couponid)
      }).catch(err => {
        reject(err)
      })
    })
  }

}




/*
*   EDIT Coupon
*
*   @couponid: (Intger)
*   @data: fromData
*/
export function editCoupon(couponid, data){

  const url           = '/admin/'+API_VERSION+'/coupons/'+couponid
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'EDIT_COUPON',
    payload: handleAPI(url, method, data, Authorization)
  }

}

/*******************
* Coupons Search
*
* @s: (String)
********************/
export function couponsSearch(s){

  let url           = '/admin/'+API_VERSION+'/coupons?search='+s
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by=id&sort_direction=desc&limit=10'

  return {
    type: 'SEARCH_COUPONS',
    payload: handleAPI(url, method, false, Authorization)
  }

}
