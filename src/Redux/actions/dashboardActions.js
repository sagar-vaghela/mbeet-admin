/****************
*   Bookings Actions
*
*   @GET_Bookings
*   @GET_Booking
*
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

export function getRegisteredUsers(date = null){
  const action = 'GET_REGISTERED_USERS'+(date ? '_TODAY' : '')
  const method = 'GET'
  let url      = '/admin/'+API_VERSION+'/users/dashbord_users'
  url         += (date) ? `?date=${date}` : ''

  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: action,
    payload: handleAPI(url, method, false, Authorization)
  }
}

export function getTotalBookings(date = null, type = null){
  const action = 'GET_TOTAL_BOOKINGS'+(date ? '_TODAY' : '')
  const method = 'GET'
  let url      = '/admin/'+API_VERSION+'/bookings/dashbord_bookings'
  url         += (date) ? `?date=${date}` : ''
  url         += (type) ? `&type=${type}` : ''

  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: action,
    payload: handleAPI(url, method, false, Authorization)
  }
}
