/****************
*   LoginAttempts Actions
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

/*
*   getLoginAttempts - fetch LoginAttempts
*/
export function getLoginAttempts(limit = 10, offset = 0, fltr = null, sort_by='id', sort_direction='asc', search = ''){

  let url           = '/admin/'+API_VERSION+'/login_attempts?limit='+limit+'&offset='+offset
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by='+sort_by+'&sort_direction='+sort_direction+'&search='+search

  if(fltr)
    url += '&'+fltr.params+'='+fltr.value

  return {
    type: 'GET_LOGINATTEMPTS',
    payload: handleAPI(url, method, false, Authorization)
  }
}

export function citiesSearch(s){

  let url           = '/admin/'+API_VERSION+'/login_attempts?search='+s
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by=id&sort_direction=desc&limit=10'

  return {
    type: 'SEARCH_LOGINATTEMPTS',
    payload: handleAPI(url, method, false, Authorization)
  }

}
