/****************
*   Logs Actions
*
*   @GET_LOGS
*   @GET_LOG
*   @ADD_LOG
*   @DELETE_LOG
*   @UPDATE_LOG
*
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

/*
*   getLogs - fetch logs
*/
// export function getLogs(limit = 10, offset = 0, fltr = null, sort_by = 'id', sort_direction = 'asc', search = ''){
export function getLogs(limit = 10, offset = 0, sort_by = 'id', sort_direction = 'asc', search = ''){

  let url           = '/admin/'+API_VERSION+'/logs?limit='+limit+'&offset='+offset
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by='+sort_by+'&sort_direction='+sort_direction+'&search='+search
  //
  // if(fltr)
  //   url += '&'+fltr.params+'='+fltr.value

  return {
    type: 'GET_LOGS',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   Get Log
*
*   @logid
*/
export function getLog(logid){

  const url           = '/admin/'+API_VERSION+'/logs/'+logid
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}



  return {
    type: 'GET_LOG',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   GET Log
*
*   @logid: (Intger)
*/
export function addLog(data){

  const url           = '/admin/'+API_VERSION+'/logs'
  const method        = 'POST'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'ADD_LOG',
    payload: handleAPI(url, method, data, Authorization)
  }

}


/*
*   Delete Log
*
*   @logid: (Intger)
*/
export function deleteLog(logid){

  const url           = '/admin/'+API_VERSION+'/logs/'+logid
  const method        = 'DELETE'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'DELETE_LOG',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(logid)
      }).catch(err => {
        reject(err)
      })
    })
  }

}

export function restoreLog(logid){

  const url           = '/admin/'+API_VERSION+'/log_restores/'+logid
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'RESTORE_LOG',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(logid)
      }).catch(err => {
        reject(err)
      })
    })
  }

}




/*
*   EDIT Log
*
*   @logid: (Intger)
*   @data: fromData
*/
export function editLog(logid, data){

  const url           = '/admin/'+API_VERSION+'/logs/'+logid
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'EDIT_LOG',
    payload: handleAPI(url, method, data, Authorization)
  }

}

/*******************
* Logs Search
*
* @s: (String)
********************/
export function logsSearch(s){

  let url           = '/admin/'+API_VERSION+'/logs?search='+s
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by=id&sort_direction=desc&limit=10'

  return {
    type: 'SEARCH_LOGS',
    payload: handleAPI(url, method, false, Authorization)
  }

}
