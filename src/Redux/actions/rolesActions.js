/****************
*   Roles Actions
*
*   @GET_ROLES
*   @GET_ROLE
*   @ADD_ROLE
*   @DELETE_ROLE
*   @UPDATE_ROLE
*
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

/*
*   getRoles - fetch roles
*/
// export function getRoles(limit = 10, offset = 0, fltr = null, sort_by = 'id', sort_direction = 'asc', search = ''){
export function getRoles(limit = 10, offset = 0, sort_by = 'id', sort_direction = 'asc', search = ''){

  let url           = '/admin/'+API_VERSION+'/roles?limit='+limit+'&offset='+offset
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by='+sort_by+'&sort_direction='+sort_direction+'&search='+search
  //
  // if(fltr)
  //   url += '&'+fltr.params+'='+fltr.value

  return {
    type: 'GET_ROLES',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   Get Role
*
*   @roleid
*/
export function getRole(roleid){

  const url           = '/admin/'+API_VERSION+'/roles/'+roleid
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}



  return {
    type: 'GET_ROLE',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   GET Role
*
*   @roleid: (Intger)
*/
export function addRole(data){

  const url           = '/admin/'+API_VERSION+'/roles'
  const method        = 'POST'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'ADD_ROLE',
    payload: handleAPI(url, method, data, Authorization)
  }

}


/*
*   Delete Role
*
*   @roleid: (Intger)
*/
export function deleteRole(roleid){

  const url           = '/admin/'+API_VERSION+'/roles/'+roleid
  const method        = 'DELETE'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'DELETE_ROLE',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(roleid)
      }).catch(err => {
        reject(err)
      })
    })
  }

}

export function restoreRole(roleid){

  const url           = '/admin/'+API_VERSION+'/role_restores/'+roleid
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'RESTORE_ROLE',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(roleid)
      }).catch(err => {
        reject(err)
      })
    })
  }

}




/*
*   EDIT Role
*
*   @roleid: (Intger)
*   @data: fromData
*/
export function editRole(roleid, data){

  const url           = '/admin/'+API_VERSION+'/roles/'+roleid
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'EDIT_ROLE',
    payload: handleAPI(url, method, data, Authorization)
  }

}

/*******************
* Roles Search
*
* @s: (String)
********************/
export function rolesSearch(s){

  let url           = '/admin/'+API_VERSION+'/roles?search='+s
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by=id&sort_direction=desc&limit=10'

  return {
    type: 'SEARCH_ROLES',
    payload: handleAPI(url, method, false, Authorization)
  }

}
