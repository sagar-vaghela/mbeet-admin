/****************
*   TopDestinations Actions
*
*   @GET_TOPDESTINATIONS
*   @GET_TOPDESTINATION
*   @ADD_TOPDESTINATION
*   @DELETE_TOPDESTINATION
*   @UPDATE_TOPDESTINATION
*
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

/*
*   getTopDestinations - fetch top_destinations
*/
// export function getTopDestinations(limit = 10, offset = 0, fltr = null, sort_by = 'id', sort_direction = 'asc', search = ''){
export function getTopDestinations(limit = 10, offset = 0, sort_by = 'id', sort_direction = 'asc', search = ''){

  let url           = '/admin/'+API_VERSION+'/cities/top_destinations?limit='+limit+'&offset='+offset
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by='+sort_by+'&sort_direction='+sort_direction+'&search='+search
  //
  // if(fltr)
  //   url += '&'+fltr.params+'='+fltr.value
  return {
    type: 'GET_TOPDESTINATIONS',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   getTopDestinationsList - fetch top_destinations_list
*/
// export function getTopDestinationsList(limit = 10, offset = 0, fltr = null, sort_by = 'id', sort_direction = 'asc', search = ''){
export function getTopDestinationsList(limit = 10, offset = 0, sort_by = 'id', sort_direction = 'asc', search = ''){

  let url           = '/admin/'+API_VERSION+'/top_destinations?limit='+limit+'&offset='+offset
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by='+sort_by+'&sort_direction='+sort_direction+'&search='+search

  return {
    type: 'GET_TOPDESTINATIONSLIST',
    payload: handleAPI(url, method, false, Authorization)
  }

}



/*
*   Get TopDestination
*
*   @topDestinationid
*/
export function getTopDestination(topDestinationid){

  const url           = '/admin/'+API_VERSION+'/top_destinations/'+topDestinationid
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}



  return {
    type: 'GET_TOPDESTINATION',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   GET TopDestination
*
*   @topDestinationid: (Intger)
*/
export function addTopDestination(data){

  const url           = '/admin/'+API_VERSION+'/top_destinations'
  const method        = 'POST'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'ADD_TOPDESTINATION',
    payload: handleAPI(url, method, data, Authorization)
  }

}


/*
*   Delete TopDestination
*
*   @topDestinationid: (Intger)
*/
export function deleteTopDestination(topDestinationid){
  const url           = '/admin/'+API_VERSION+'/top_destinations/'+topDestinationid
  const method        = 'DELETE'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'DELETE_TOPDESTINATION',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(topDestinationid)
      }).catch(err => {
        reject(err)
      })
    })
  }

}


/*
*   EDIT TopDestination
*
*   @topDestinationid: (Intger)
*   @data: fromData
*/
export function editTopDestination(topDestinationid, data){

  const url           = '/admin/'+API_VERSION+'/top_destinations/'+topDestinationid
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'EDIT_TOPDESTINATION',
    payload: handleAPI(url, method, data, Authorization)
  }

}

/*******************
* TopDestinations Search
*
* @s: (String)
********************/
export function top_destinationsSearch(s){

  let url           = '/admin/'+API_VERSION+'/cities/top_destinations?search='+s
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by=id&sort_direction=desc&limit=10'

  return {
    type: 'SEARCH_TOPDESTINATIONS',
    payload: handleAPI(url, method, false, Authorization)
  }
}

/*******************
* TopDestinationsList Search
*
* @s: (String)
********************/
export function top_destinationsListSearch(s){

  let url           = '/admin/'+API_VERSION+'/top_destinations?search='+s
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by=id&sort_direction=desc&limit=10'

  return {
    type: 'SEARCH_TOPDESTINATIONSLIST',
    payload: handleAPI(url, method, false, Authorization)
  }
}
