/****************
*   Users Actions
*
*   @GET_UNITS
*   @GET_UNIT
*   @DELETE_UNIT
*   @UPDATE_UNIT
*
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

/*
*   GET Units
*/
export function getUnits(limit = 12, offset = 0, fltr = null, sort_by = 'id', sort_direction = 'asc', search = ''){

  let url           = '/admin/'+API_VERSION+'/units?'
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  // params
  url += 'limit='+limit
  url += '&offset='+offset
  url += '&search='+search
  //url += '&soft_delete='+soft_delete

  if(fltr)
    url += '&'+fltr.params+'='+fltr.value

  return {
    type: 'GET_UNITS',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/******************
* GET Unit
******************/
export function getUnit(unit_id){

  const url = '/admin/'+API_VERSION+'/units/'+unit_id
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'GET_UNIT',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/******************
* GET Units Rules
******************/
export function getRules(){

  const url           = '/'+API_VERSION+'/rules'
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'GET_RULES',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/******************
* GET Units Amenities
******************/
export function getAmenities(){

  const url           = '/'+API_VERSION+'/amenities'
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'GET_AMENITIES',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/******************
* Add Unit
******************/
export function addUnit(data){

  const url           = '/admin/'+API_VERSION+'/units'
  const method        = 'POST'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'ADD_UNIT',
    payload: handleAPI(url, method, data, Authorization)
  }

}

/******************
* Edit Unit
******************/
export function updateUnit(unit_id, data){


  const url           = '/admin/'+API_VERSION+'/units/'+unit_id
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'UPDATE_UNIT',
    payload: handleAPI(url, method, data, Authorization)
  }

}

/******************
* Delete Unit
******************/
export function deleteUnit(unitid){

  const url           = '/admin/'+API_VERSION+'/units/'+unitid
  const method        = 'DELETE'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'DELETE_UNIT',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(unitid)
      }).catch(err => {
        reject(err)
      })
    })
  }
}

/******************
* Restore Unit
******************/
export function restoreUnit(unitid){

  const url           = '/admin/'+API_VERSION+'/units/'+unitid+'/restore'
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'RESTORE_UNIT',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(unitid)
      }).catch(err => {
        reject(err)
      })
    })
  }
}

/*******************
* Users Search
*
* @s: (String)
********************/
export function unitsSearch(s, limit = 8){

  let url           = '/admin/'+API_VERSION+'/units?search='+s
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by=id&sort_direction=desc&limit='+limit

  return {
    type: 'SEARCH_UNITS',
    payload: handleAPI(url, method, false, Authorization)
  }

}

export function removeImg(img_id){

  const url     = '/'+API_VERSION+'/user_units_images/'+img_id
  const method  = 'DELETE'
  const locale  = localStorage.getItem('LOCALE')

  return {
    type: 'REMOVE_IMG',
    payload: handleAPI(url, method)
  }

}

export function disableUnit(unit_id, formData){
  const url           = '/admin/'+API_VERSION+'/units/'+unit_id+'/disable'
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'DISABLE_UNIT',
    payload: handleAPI(url, method, formData, Authorization)
  }
}

export function enableUnit(unit_id, formData){
  const url           = '/admin/'+API_VERSION+'/units/'+unit_id+'/enable'
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'ENABLE_UNIT',
    payload: handleAPI(url, method, formData, Authorization)
  }
}

// export function enableUnit(unit_id){
//
//   const url           = '/admin/'+API_VERSION+'/units/'+unit_id+'/enable'
//   const method        = 'PUT'
//   const locale        = localStorage.getItem('LOCALE')
//   const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}
//
//   return {
//     type: 'ENABLE_UNIT',
//     payload: handleAPI(url, method, false, Authorization)
//   }
//
// }

export function clearMsg(){
  return {
    type: 'CLEAR_MSG',
    payload: new Promise((resolve, reject) => resolve(1))
  }
}
