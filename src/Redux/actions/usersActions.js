/****************
*   Users Actions
*
*   @GET_USERS
*   @GET_USER
*   @ADD_USER
*   @DELETE_USER
*   @UPDATE_USER
*   @GET_USER_UNITS
*
/****************/

import {handleAPI, API_VERSION} from '../../Utilities/handleAPI'

/*
*   getUsers - fetch users
*/
export function getUsers(limit = 10, offset = 0, fltr = null, sort_by = 'id', sort_direction = 'asc', search = ''){

  let url           = '/admin/'+API_VERSION+'/users?limit='+limit+'&offset='+offset
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by='+sort_by+'&sort_direction='+sort_direction+'&search='+search

  if(fltr)
    url += '&'+fltr.params+'='+fltr.value

  return {
    type: 'GET_USERS',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   Get User
*
*   @userid
*/
export function getUser(userid){

  const url           = '/admin/'+API_VERSION+'/users/'+userid
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}



  return {
    type: 'GET_USER',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   Get CurrentUserPermission
*/
export function getCurrentUserPermission(userid){

  const url           = '/admin/'+API_VERSION+'/users/getCurrentUserPermission'
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}



  return {
    type: 'GET_CURRENTUSERPERMISSIONS',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   GET User Units
*
*   @userid: (Intger)
*/
export function getUserUnits(userid){

  const url           = '/admin/'+API_VERSION+'/users/'+userid+'/units'
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'GET_USER_UNITS',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/*
*   GET User Units
*
*   @userid: (Intger)
*/
export function addUser(data){

  const url           = '/admin/'+API_VERSION+'/users'
  const method        = 'POST'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'ADD_USER',
    payload: handleAPI(url, method, data, Authorization)
  }

}


/*
*   Delete User
*
*   @userid: (Intger)
*/
export function deleteUser(userid){

  const url           = '/admin/'+API_VERSION+'/users/'+userid
  const method        = 'DELETE'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'DELETE_USER',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(userid)
      }).catch(err => {
        reject(err)
      })
    })
  }

}

export function restoreUser(userid){

  const url           = '/admin/'+API_VERSION+'/user_restores/'+userid
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'RESTORE_USER',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(userid)
      }).catch(err => {
        reject(err)
      })
    })
  }

}




/*
*   EDIT User
*
*   @userid: (Intger)
*   @data: fromData
*/
export function editUser(userid, data){

  const url           = '/admin/'+API_VERSION+'/users/'+userid
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'EDIT_USER',
    payload: handleAPI(url, method, data, Authorization)
  }

}

/*******************
* Users Search
*
* @s: (String)
********************/
export function usersSearch(s){

  let url           = '/admin/'+API_VERSION+'/users?search='+s
  const method        = 'GET'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  url += '&sort_by=id&sort_direction=desc&limit=10'

  return {
    type: 'SEARCH_USERS',
    payload: handleAPI(url, method, false, Authorization)
  }

}

/******************
* Delete Unit
******************/
export function deleteUnit(unitid){

  const url           = '/admin/'+API_VERSION+'/units/'+unitid
  const method        = 'DELETE'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'DELETE_USER_UNIT',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(unitid)
      }).catch(err => {
        reject(err)
      })
    })
  }
}

/******************
* Restore Unit
******************/
export function restoreUnit(unitid){

  const url           = '/admin/'+API_VERSION+'/units/'+unitid+'/restore'
  const method        = 'PUT'
  const locale        = localStorage.getItem('LOCALE')
  const Authorization = {'access-token':sessionStorage.getItem('TOKEN')}

  return {
    type: 'RESTORE_USER_UNIT',
    payload: new Promise((resolve, reject) => {
      handleAPI(url, method, false, Authorization).then(() => {
        resolve(unitid)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
