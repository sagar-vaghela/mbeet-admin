/*
* Booking Reducer
*/

import {handleResponseErr} from '../../Utilities/functions'


const DEFAULT_STATE = {
  bookings: null,
  booking: null,
  total: 0,
  errMsg: null,
  succMsg: null,
  fetching: false,
}

let changes = null
export default function bookingsReducer (state = DEFAULT_STATE, action) {

  switch (action.type){

    /***************************
    *   GET BOOKINGS
    ****************************/
    // On Pending
    case "GET_BOOKINGS_PENDING":
    case "GET_BOOKING_PENDING":
    case "SEARCH_BOOKINGS_PENDING":
      return {...state, fetching: true, booking: null, errMsg: null, succMsg: null}


    // On Success
    case "GET_BOOKINGS_FULFILLED":
    case "SEARCH_BOOKINGS_FULFILLED":
      changes = {
        bookings: action.payload.data.data.booking_list,
        total: action.payload.data.data.result_total,
        fetching: false,
      }
      return {...state, ...changes}

    // On Failed
    case "GET_BOOKINGS_REJECTED":
    case "GET_BOOKING_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}


    /***************************
    *   GET BOOKING
    ****************************/
    // On Success get booking
    case "GET_BOOKING_FULFILLED":
      changes = {
        booking: action.payload.data.data.booking,
        fetching: false,
      }
      return {...state, ...changes}


  } // / End Switch

  return state

}
