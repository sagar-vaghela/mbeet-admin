/*
* Cities Reducer
*/

import {handleResponseErr} from '../../Utilities/functions'

const DEFAULT_STATE = {
  cities: null,
  city: null,
  city_list_with_unit: null,
  top_destin_cities: null,
  total: 0,
  errMsg: null,
  succMsg: null,
  fetching: false,
}

let changes = null
export default function citiesReducer (state = DEFAULT_STATE, action) {

  switch (action.type){

    /***************************
    *   GET CITIES
    ****************************/
    // On Pending
    case "GET_CITIES_PENDING":
    case "GET_CITYLISTWITHUNIT_PENDING":
    case "GET_TOPDESTINCITIES_PENDING":
    case "GET_CITY_PENDING":
    case "ADD_CITY_PENDING":
    case "SEARCH_CITIES_PENDING":
      return {...state, fetching: true, errMsg: null, succMsg: null, city: null}


    // On Success
    case "GET_CITIES_FULFILLED":
    case "SEARCH_CITIES_FULFILLED":
      changes = {
        cities: action.payload.data.data.cities,
        total: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    case "GET_CITYLISTWITHUNIT_FULFILLED":
      changes = {
        city_list_with_unit: action.payload.data.data.cities,
        total: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    case "GET_TOPDESTINCITIES_FULFILLED":
      changes = {
        top_destin_cities: action.payload.data.data.cities,
        total: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On Failed
    case "GET_CITIES_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}


    /***************************
    *   GET City
    ****************************/
    // On Success
    case "GET_CITY_FULFILLED":
      changes = {
        city: action.payload.data.data.cities,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On Failed
    case "GET_CITY_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    case "GET_CITYLISTWITHUNIT_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}


    case "GET_TOPDESTINCITIES_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}


    /*************************
    *   Add City
    **************************/
    // On Success Add user
    case "ADD_CITY_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    case "ADD_CITY_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Edit City
    **************************/
    // On Pending
    case "EDIT_CITY_PENDING":
      return {...state, fetching: true, errMsg: null, succMsg: null}
    // On Success
    case "EDIT_CITY_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,

        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On Failed
    case "EDIT_CITY_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /***************************
    * DELETE City
    ****************************/
    // On Success Delete
    case "DELETE_CITY_FULFILLED":
      changes = {
        cities : state.cities.filter(city => city.id != action.payload),
        fetching: false,
        errMsg: null,
        succMsg: null,
      }
      return {...state, ...changes}

    // On failed delete
    case "DELETE_CITY_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}




  } // / End Switch

  return state

}
