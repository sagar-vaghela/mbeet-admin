/*
* Coupon Reducer
*/

import {handleResponseErr} from '../../Utilities/functions'


const DEFAULT_STATE = {
  coupons: null,
  coupon: null,
  totalCoupons: 0,
  errMsg: null,
  succMsg: null,
  fetching: false,
}

let changes = null
export default function couponsReducer (state = DEFAULT_STATE, action) {

  switch (action.type){

    /*
    * On Pending
    * Action: active fetching
    */
    case "GET_COUPONS_PENDING":
    case "GET_COUPON_PENDING":
    case "ADD_COUPON_PENDING":
    case "DELETE_COUPON_PENDING":
    case "EDIT_COUPON_PENDING":
    case "SEARCH_COUPONS_PENDING":
    case "FILTER_COUPONS_PENDING":
      return {...state, fetching: true, errMsg: null, succMsg: null, coupon: null}


    /*************************
    *   Index Coupons
    **************************/
    /*
    * On Failed
    * Action: put error message on the state
    */
    case "GET_COUPONS_REJECTED":
    case "FILTER_COUPONS_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*
    * On Success
    * Action: put the coupons on the state
    */
    case "GET_COUPONS_FULFILLED":
    case "FILTER_COUPONS_FULFILLED":
      changes = {
        coupons: action.payload.data.data.coupons,
        totalCoupons: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }

      return {...state, ...changes}

    // On Delete
    case "DELETE_COUPON_FULFILLED":
    case "RESTORE_COUPON_FULFILLED":
      changes = {
        coupons : state.coupons.filter(coupon => coupon.id != action.payload),
        fetching: false,
        errMsg: null,
        succMsg: null,
      }
      return {...state, ...changes}

    // On failed delete
    case "DELETE_COUPON_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Show Coupon
    **************************/

    // On success get coupon
    case "GET_COUPON_FULFILLED":
      changes = {
        coupon: action.payload.data.data,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On failed get coupon
    case "GET_COUPON_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Add Coupon
    **************************/
    // On Success Add coupon
    case "ADD_COUPON_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    case "ADD_COUPON_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Edit Coupon
    **************************/
    // On Success
    case "EDIT_COUPON_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On Failed
    case "EDIT_COUPON_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Search Coupons
    **************************/
    case "SEARCH_COUPONS_FULFILLED":
      changes = {
        coupons: action.payload.data.data.coupons,
        totalCoupons: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

  } // / End Switch

  return state

}
