/*
* Booking Reducer
*/

import {handleResponseErr} from '../../Utilities/functions'


const DEFAULT_STATE = {
  total_users: {
    data: null,
    errMsg: null,
  },
  today_users: {
    data: null,
    errMsg: null,
  },
  total_bookings: {
    data: null,
    errMsg: null,
  },
  today_bookings: {
    data: null,
    errMsg: null,
  }
}

export default function dashboardReducer (state = DEFAULT_STATE, action) {

  switch (action.type){

    /******************
    * Registerd Users
    *******************/
    case 'GET_REGISTERED_USERS_FULFILLED':
      return {...state,
        total_users: {
          data: action.payload.data,
          errMsg: null
        }
      }

    case 'GET_REGISTERED_USERS_REJECTED':
      return {...state,
        total_users:{
          ...state.total_users,
          errMsg: handleResponseErr(action.payload)
        }
      }

    /* total registered today */
    case 'GET_REGISTERED_USERS_TODAY_FULFILLED':
      return {...state,
        today_users: {
          data: action.payload.data,
          errMsg: null
        }
      }

    case 'GET_REGISTERED_USERS_TODAY_REJECTED':
      return {...state,
        today_users:{
          ...state.total_users,
          errMsg: handleResponseErr(action.payload)
        }
      }

    /*****************
    * Total Bookings
    ******************/
    case 'GET_TOTAL_BOOKINGS_FULFILLED':
      return {...state,
        total_bookings: {
          data: action.payload.data,
          errMsg: null
        }
      }

    case 'GET_TOTAL_BOOKINGS_REJECTED':
      return {...state,
        total_bookings:{
          ...state.total_bookings,
          errMsg: handleResponseErr(action.payload)
        }
      }

    case 'GET_TOTAL_BOOKINGS_TODAY_FULFILLED':
      return {...state,
        today_bookings: {
          data: action.payload.data,
          errMsg: null
        }
      }
    case 'GET_TOTAL_BOOKINGS_TODAY_REJECTED':
      return {...state,
        today_bookings:{
          ...state.today_bookings,
          errMsg: handleResponseErr(action.payload)
        }
      }
  }

  return state

}
