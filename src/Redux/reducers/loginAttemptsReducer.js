/*
* LoginAttempts Reducer
*/

import {handleResponseErr} from '../../Utilities/functions'

const DEFAULT_STATE = {
  loginAttempts: null,
  loginAttempt: null,
  total: 0,
  errMsg: null,
  succMsg: null,
  fetching: false,
}

let changes = null
export default function loginAttemptsReducer (state = DEFAULT_STATE, action) {

  switch (action.type){

    /***************************
    *   GET LOGINATTEMPTS
    ****************************/
    // On Pending
    case "GET_LOGINATTEMPTS_PENDING":
    case "SEARCH_LOGINATTEMPTS_PENDING":
      return {...state, fetching: true, errMsg: null, succMsg: null, loginAttempt: null}


    // On Success
    case "GET_LOGINATTEMPTS_FULFILLED":
    case "SEARCH_LOGINATTEMPTS_FULFILLED":
      changes = {
        loginAttempts: action.payload.data.data.login_attempts,
        total: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On Failed
    case "GET_LOGINATTEMPTS_REJECTED":
      changes = {
      errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}


  } // / End Switch

  return state

}
