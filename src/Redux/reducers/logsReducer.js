/*
* Log Reducer
*/

import {handleResponseErr} from '../../Utilities/functions'


const DEFAULT_STATE = {
  logs: null,
  log: null,
  total: 0,
  errMsg: null,
  succMsg: null,
  fetching: false,
}

let changes = null
export default function logsReducer (state = DEFAULT_STATE, action) {

  switch (action.type){

    /*
    * On Pending
    * Action: active fetching
    */
    case "GET_LOGS_PENDING":
    case "GET_LOG_PENDING":
    case "ADD_LOG_PENDING":
    case "DELETE_LOG_PENDING":
    case "EDIT_LOG_PENDING":
    case "SEARCH_LOGS_PENDING":
    case "FILTER_LOGS_PENDING":
      return {...state, fetching: true, errMsg: null, succMsg: null, log: null}


    /*************************
    *   Index Logs
    **************************/
    /*
    * On Failed
    * Action: put error message on the state
    */
    case "GET_LOGS_REJECTED":
    case "FILTER_LOGS_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*
    * On Success
    * Action: put the logs on the state
    */
    case "GET_LOGS_FULFILLED":
    case "FILTER_LOGS_FULFILLED":
      changes = {
        logs: action.payload.data.data.logs,
        total: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }

      return {...state, ...changes}

    // On Delete
    case "DELETE_LOG_FULFILLED":
    case "RESTORE_LOG_FULFILLED":
      changes = {
        logs : state.logs.filter(log => log.id != action.payload),
        fetching: false,
        errMsg: null,
        succMsg: null,
      }
      return {...state, ...changes}

    // On failed delete
    case "DELETE_LOG_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Show Log
    **************************/

    // On success get log
    case "GET_LOG_FULFILLED":
      changes = {
        log: action.payload.data.data,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On failed get log
    case "GET_LOG_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Add Log
    **************************/
    // On Success Add log
    case "ADD_LOG_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    case "ADD_LOG_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Edit Log
    **************************/
    // On Success
    case "EDIT_LOG_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On Failed
    case "EDIT_LOG_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Search Logs
    **************************/
    case "SEARCH_LOGS_FULFILLED":
      changes = {
        logs: action.payload.data.data.logs,
        total: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

  } // / End Switch

  return state

}
