/*
* Role Reducer
*/

import {handleResponseErr} from '../../Utilities/functions'


const DEFAULT_STATE = {
  roles: null,
  role: null,
  totalRoles: 0,
  errMsg: null,
  succMsg: null,
  fetching: false,
}

let changes = null
export default function rolesReducer (state = DEFAULT_STATE, action) {

  switch (action.type){

    /*
    * On Pending
    * Action: active fetching
    */
    case "GET_ROLES_PENDING":
    case "GET_ROLE_PENDING":
    case "ADD_ROLE_PENDING":
    case "DELETE_ROLE_PENDING":
    case "EDIT_ROLE_PENDING":
    case "SEARCH_ROLES_PENDING":
    case "FILTER_ROLES_PENDING":
      return {...state, fetching: true, errMsg: null, succMsg: null, role: null}


    /*************************
    *   Index Roles
    **************************/
    /*
    * On Failed
    * Action: put error message on the state
    */
    case "GET_ROLES_REJECTED":
    case "FILTER_ROLES_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*
    * On Success
    * Action: put the roles on the state
    */
    case "GET_ROLES_FULFILLED":
    case "FILTER_ROLES_FULFILLED":
      changes = {
        roles: action.payload.data.data.roles,
        totalRoles: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }

      return {...state, ...changes}

    // On Delete
    case "DELETE_ROLE_FULFILLED":
    case "RESTORE_ROLE_FULFILLED":
      changes = {
        roles : state.roles.filter(role => role.id != action.payload),
        fetching: false,
        errMsg: null,
        succMsg: null,
      }
      return {...state, ...changes}

    // On failed delete
    case "DELETE_ROLE_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Show Role
    **************************/

    // On success get role
    case "GET_ROLE_FULFILLED":
      changes = {
        role: action.payload.data.data,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On failed get role
    case "GET_ROLE_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Add Role
    **************************/
    // On Success Add role
    case "ADD_ROLE_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    case "ADD_ROLE_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Edit Role
    **************************/
    // On Success
    case "EDIT_ROLE_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On Failed
    case "EDIT_ROLE_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Search Roles
    **************************/
    case "SEARCH_ROLES_FULFILLED":
      changes = {
        roles: action.payload.data.data.roles,
        totalRoles: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

  } // / End Switch

  return state

}
