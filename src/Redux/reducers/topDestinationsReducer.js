/*
* Role Reducer
*/

import {handleResponseErr} from '../../Utilities/functions'


const DEFAULT_STATE = {
  top_destinations: null,
  top_destination: null,
  totalTop_destinations: 0,
  top_destinationsList: null,
  totalTop_destinationsList: 0,
  errMsg: null,
  succMsg: null,
  fetching: false,
}

let changes = null
export default function top_destinationsReducer (state = DEFAULT_STATE, action) {

  switch (action.type){

    /*
    * On Pending
    * Action: active fetching
    */
    case "GET_TOPDESTINATIONS_PENDING":
    case "GET_TOPDESTINATIONSLIST_PENDING":
    case "GET_TOPDESTINATION_PENDING":
    case "ADD_TOPDESTINATION_PENDING":
    case "DELETE_TOPDESTINATION_PENDING":
    case "EDIT_TOPDESTINATION_PENDING":
    case "SEARCH_TOPDESTINATIONS_PENDING":
    case "SEARCH_TOPDESTINATIONSLIST_PENDING":
    case "FILTER_TOPDESTINATIONS_PENDING":
      return {...state, fetching: true, errMsg: null, succMsg: null, top_destination: null}


    /*************************
    *   Index Roles
    **************************/
    /*
    * On Failed
    * Action: put error message on the state
    */
    case "GET_TOPDESTINATIONS_REJECTED":
    case "GET_TOPDESTINATIONSLIST_REJECTED":
    case "FILTER_TOPDESTINATIONS_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*
    * On Success
    * Action: put the top_destinations on the state
    */
    case "GET_TOPDESTINATIONS_FULFILLED":
    case "FILTER_TOPDESTINATIONS_FULFILLED":
      changes = {
        top_destinations: action.payload.data.data.top_destinations,
        totalTop_destinations: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }

      return {...state, ...changes}

    case "GET_TOPDESTINATIONSLIST_FULFILLED":
      changes = {
        top_destinationsList: action.payload.data.data.top_destination,
        totalTop_destinationsList: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }

      return {...state, ...changes}

    // On Delete
    case "DELETE_TOPDESTINATION_FULFILLED":

    // On failed delete
    case "DELETE_TOPDESTINATION_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Show Role
    **************************/

    // On success get top_destination
    case "GET_TOPDESTINATION_FULFILLED":
      changes = {
        top_destination: action.payload.data.data.top_destination,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On failed get top_destination
    case "GET_TOPDESTINATION_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Add Role
    **************************/
    // On Success Add top_destination
    case "ADD_TOPDESTINATION_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    case "ADD_TOPDESTINATION_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Edit Role
    **************************/
    // On Success
    case "EDIT_TOPDESTINATION_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    // On Failed
    case "EDIT_TOPDESTINATION_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Search Roles
    **************************/
    case "SEARCH_TOPDESTINATIONS_FULFILLED":
      changes = {
        top_destinations: action.payload.data.data.top_destinations,
        totalTop_destinations: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

  } // / End Switch

  return state

}
