/*
* Units Reducer
*/

import {handleResponseErr} from '../../Utilities/functions'


const DEFAULT_STATE = {
  units: null,
  unit: null,
  rules: null,
  amenities: null,
  totalUnits: 0,
  errMsg: null,
  succMsg: null,
  fetching: false,
}

let changes = null
export default function unitsReducer (state = DEFAULT_STATE, action) {

  switch (action.type){

    /*
    * On Pending
    * Action: active fetching
    */
    case "GET_UNITS_PENDING":
    case "ADD_UNIT_PENDING":
    case "RESTORE_UNIT_PENDING":
    case "SEARCH_UNITS_PENDING":
    case "GET_UNIT_PENDING":
      return {...state, fetching: true, errMsg: null, succMsg: null, unit: null}

    /*************************
    *   Index Units
    **************************/
    /*
    * On Success
    * Action: put the users on the state
    */
    case "GET_UNITS_FULFILLED":
    case "SEARCH_BOOKINGS_FULFILLED":
    case "SEARCH_UNITS_FULFILLED":
      changes = {
        units: action.payload.data.data.units,
        totalUnits: action.payload.data.data.result_total,
        errMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    /***********************
    *   Get Unit
    ************************/
    // On sucess get unit
    case "GET_UNIT_FULFILLED":
      changes = {
        unit: action.payload.data.data.units,
        errMsg: null,
        succMsg: null,
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Update Unit
    **************************/
    case "UPDATE_UNIT_PENDING":
      return {...state, fetching: true, errMsg: null, succMsg: null}

    case "UPDATE_UNIT_FULFILLED":
      changes = {
        unit: action.payload.data.data.units,
        errMsg: null,
        succMsg: action.payload.data.message,
        fetching: false,
      }
      return {...state, ...changes}
    /*
    * On Failed
    * Action: put error message on the state
    */
    case "GET_UNITS_REJECTED":
    case "ADD_UNIT_REJECTED":
    case "GET_UNIT_REJECTED":
    case "UPDATE_UNIT_REJECTED":
      changes = {
        errMsg: handleResponseErr(action.payload),
        fetching: false,
      }
      return {...state, ...changes}

    case "DISABLE_UNIT_PENDING":
        changes = {
          disableErrMsg: null,
          succMsg: null,
        }
        return {...state, ...changes}

    case "DISABLE_UNIT_REJECTED":
          changes = {
            disableErrMsg: handleResponseErr(action.payload),
            fetching: false,
          }
          return {...state, ...changes}

    case "ENABLE_UNIT_PENDING":
      changes = {
        // fetching: true,
        enableErrMsg: null,
        succMsg: null,
      }
      return {...state, ...changes}

    case "ENABLE_UNIT_REJECTED":
          changes = {
            enableErrMsg: handleResponseErr(action.payload),
            fetching: false,
          }
          return {...state, ...changes}

    case "ENABLE_UNIT_FULFILLED":
        changes = {
          succMsg: action.payload.data.message,
          fetching: false,
        }
        return {...state, ...changes}
    /*************************
    *   GET Rules
    **************************/
    case "GET_RULES_FULFILLED":
      changes = {
        rules: action.payload.data.data.rules,
      }
      return {...state, ...changes}



    /*************************
    *   GET Amenities
    **************************/
    case "GET_AMENITIES_FULFILLED":
      changes = {
        amenities: action.payload.data.data.amenities,
      }
      return {...state, ...changes}

    /*************************
    *   Add Unit
    **************************/
    // On Success add unit
    case "ADD_UNIT_FULFILLED":
    case "DISABLE_UNIT_FULFILLED":
      changes = {
        succMsg: action.payload.data.message,
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Delete Unit
    **************************/
    // On Success
    case "DELETE_UNIT_FULFILLED":
      state.units.map(unit => {
        if(unit.id === action.payload)
          unit.soft_delete = true
      })
      return state

    /*************************
    *   Restore Unit
    **************************/
    // On Success
    case "RESTORE_UNIT_FULFILLED":
      state.units.map(unit => {
        if(unit.id === action.payload)
          unit.soft_delete = false
      })
      changes = {
        fetching: false,
      }
      return {...state, ...changes}

    /*************************
    *   Remove Unit Image
    **************************/
    // On Success
    case "REMOVE_IMG_FULFILLED":
      const imgID = action.payload.data.data.city.id
      state.unit.images = state.unit.images.filter(img => img.id != imgID)
      return state

    case "CLEAR_MSG_PENDING":
      changes = {
        errMsg: null,
        succMsg: null,
        disableErrMsg: null,
      }
      return {...state, ...changes}

  }// end switch

  return state

}
