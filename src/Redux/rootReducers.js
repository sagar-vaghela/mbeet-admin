import  {combineReducers} from  'redux'

// Reducers
import authReducer from './reducers/authReducer'
import usersReducer from './reducers/usersReducer'
import unitsReducer from './reducers/unitsReducer'
import bookingsReducer from './reducers/bookingsReducer'
import citiesReducer from './reducers/citiesReducer'
import dashboardReducer from './reducers/dashboardReducer'
import couponsReducer from './reducers/couponsReducer'
import rolesReducer from './reducers/rolesReducer'
import logsReducer from './reducers/logsReducer'
import topDestinationsReducer from './reducers/topDestinationsReducer'
import specialPricesReducer from './reducers/specialPricesReducer'
import loginAttemptsReducer from './reducers/loginAttemptsReducer'


const rootReducers = combineReducers({
  auth: authReducer,
  users: usersReducer,
  units: unitsReducer,
  bookings: bookingsReducer,
  cities: citiesReducer,
  dashboard: dashboardReducer,
  coupons: couponsReducer,
  roles: rolesReducer,
  logs: logsReducer,
  topDestinations: topDestinationsReducer,
  specialPrices: specialPricesReducer,
  loginAttempts: loginAttemptsReducer,
});


export default rootReducers;
