import React, {Component} from 'react'
import {connect} from 'react-redux'
import { Route, Switch } from "react-router-dom"

// Authenticated Pages Routers
import DashboardRouters from './pagesRouters/DashboardRouters'
import UsersRouters from './pagesRouters/UsersRouters'
import UnitsRouters from './pagesRouters/UnitsRouters'
import SpecialPricesRouters from './pagesRouters/SpecialPricesRouters'
import BookingsRouters from './pagesRouters/BookingsRouters'
import CouponsRouters from './pagesRouters/CouponsRouters'
import CitiesRouters from './pagesRouters/CitiesRouters'
import RolesRouters from './pagesRouters/RolesRouters'
import LogsRouters from './pagesRouters/LogsRouters'
import TopDestinationsRouters from './pagesRouters/TopDestinationsRouters'
import LoginAttemptsRouters from './pagesRouters/LoginAttemptsRouters'

import Sidebar from '../Components/Sidebar/Sidebar'
import { getCurrentUserPermission } from '../Redux/actions/usersActions'

class AuthPagesRouters extends Component{

  componentWillMount(){
    const {dispatch} = this.props
    dispatch(getCurrentUserPermission())
  }

  render(){
    const {current_user_permissions} = this.props
    return (
      <div className="row clearfix">
        <div className="h-100 col-lg-2 col-md-2">
          <Sidebar permissions={current_user_permissions} />
        </div>
        <DashboardRouters permissions={current_user_permissions} />
        <UsersRouters permissions={current_user_permissions} />
        <UnitsRouters permissions={current_user_permissions} />
        <SpecialPricesRouters permissions={current_user_permissions} />
        <BookingsRouters permissions={current_user_permissions} />
        <CouponsRouters permissions={current_user_permissions} />
        <CitiesRouters permissions={current_user_permissions} />
        <RolesRouters permissions={current_user_permissions} />
        <TopDestinationsRouters permissions={current_user_permissions} />
        <LogsRouters permissions={current_user_permissions} />
        <LoginAttemptsRouters permissions={current_user_permissions} />
      </div>
    )
  }
}

const mapStateToProps = (store) => {
  return {
    current_user_permissions: store.users.current_user_permissions,
    errMsg: store.users.errMsg,
    fetchingUnits: store.users.fetchingUnits,
  }
}

AuthPagesRouters = connect(mapStateToProps, null, null, {
  pure: false
})(AuthPagesRouters)

export default AuthPagesRouters
