import React, {Component} from 'react'
import { Route, Switch } from "react-router-dom"

// Authenticated Pages Routers

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Bookings Components
const IndexBookings   = asyncComponent(() => import('../../Components/Pages/Bookings/indexBooking/IndexBooking'))
const ShowBooking   = asyncComponent(() => import('../../Components/Pages/Bookings/showBooking/ShowBooking'))
const AddBooking   = asyncComponent(() => import('../../Components/Pages/Bookings/addBooking/AddBooking'))

class BookingsRouters extends Component{
  render(){
    const {permissions} = this.props
    return (
      <Switch>
        {permissions && (!permissions.dashboard_permissions.view && !permissions.users_permissions.view && !permissions.unit_permissions.view && permissions.booking_permissions.view) &&
          <Route path="/" exact component={IndexBookings} />
        }
        {permissions && permissions.booking_permissions.view &&
          <Route path="/bookings" exact component={IndexBookings} />
        }
        {permissions && permissions.booking_permissions.show &&
          <Route path="/bookings/:id" exact component={ShowBooking} />
        }
        <Route path="/newBooking" exact component={AddBooking} />
      </Switch>
    )
  }
}

export default BookingsRouters
