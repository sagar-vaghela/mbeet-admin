import React, {Component} from 'react'
import { Route, Switch } from "react-router-dom"

// Authenticated Pages Routers

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Cities Components
const IndexCities   = asyncComponent(() => import('../../Components/Pages/Cities/indexCities/IndexCities'))
const AddCity   = asyncComponent(() => import('../../Components/Pages/Cities/addCity/AddCity'))
const EditCity   = asyncComponent(() => import('../../Components/Pages/Cities/editCity/EditCity'))

class CitiesRouters extends Component{
  render(){
    const {permissions} = this.props
    return (
      <Switch>
        {permissions && (!permissions.dashboard_permissions.view && !permissions.users_permissions.view && !permissions.unit_permissions.view && !permissions.booking_permissions.view && !permissions.coupon_permissions.view
        && permissions.city_permissions.view
         ) &&
          <Route path="/" exact component={IndexCities} />
        }
        {permissions && permissions.city_permissions.view &&
          <Route path="/cities" exact component={IndexCities} />
        }
        {permissions && permissions.city_permissions.edit &&
          <Route path="/cities/edit/:id" exact component={EditCity} />
        }
        {permissions && permissions.city_permissions.create &&
          <Route path="/cities/add" exact component={AddCity} />
        }
      </Switch>
    )
  }
}

export default CitiesRouters
