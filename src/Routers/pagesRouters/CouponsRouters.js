import React, {Component} from 'react'
import { Route, Switch } from "react-router-dom"

// Authenticated Pages Routers

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Coupons Components
const IndexCoupons   = asyncComponent(() => import('../../Components/Pages/Coupons/indexCoupons/IndexCoupons'))
const EditCoupon     = asyncComponent(() => import('../../Components/Pages/Coupons/editCoupon/EditCoupon'))
const AddCoupon     = asyncComponent(() => import('../../Components/Pages/Coupons/addCoupon/AddCoupon'))

class CouponsRouters extends Component{
  render(){
    const {permissions} = this.props
    return (
      <Switch>

        {permissions && (!permissions.dashboard_permissions.view && !permissions.users_permissions.view && !permissions.unit_permissions.view && !permissions.booking_permissions.view && permissions.coupon_permissions.view ) &&
          <Route path="/" exact component={IndexCoupons} />
        }

        {permissions && permissions.coupon_permissions.view &&
          <Route path="/coupons" exact component={IndexCoupons} />
        }
        {permissions && permissions.coupon_permissions.edit &&
          <Route path="/coupons/edit/:id" exact component={EditCoupon} />
        }
        {permissions && permissions.coupon_permissions.create &&
          <Route path="/coupons/add" exact component={AddCoupon} />
        }
      </Switch>
    )
  }
}

export default CouponsRouters
