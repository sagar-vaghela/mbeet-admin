import React, {Component} from 'react'
import { Route, Switch } from "react-router-dom"

// Authenticated Pages Routers

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Dashboard Components
const Dashboard     = asyncComponent(() => import('../../Components/Pages/Dashboard/Dashboard'))


class DashboardRouters extends Component{
  render(){
    const {permissions} = this.props
    return (
      <Switch>
        {permissions && permissions.dashboard_permissions.view &&
          <Route exact path="/"  component={Dashboard} />
        }
      </Switch>
    )
  }
}

export default DashboardRouters
