import React, {Component} from 'react'
import { Route, Switch } from "react-router-dom"

// Authenticated Pages Routers

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// IndexLoginAttempts Components
const IndexLoginAttempts   = asyncComponent(() => import('../../Components/Pages/LoginAttempts/indexLoginAttempts/IndexLoginAttempts'))

class LoginAttemptsRouters extends Component{
  render(){
    const {permissions} = this.props
    return (
      <Switch>
          <Route path="/loginAttempts" exact component={IndexLoginAttempts} />
      </Switch>
    )
  }
}

export default LoginAttemptsRouters
