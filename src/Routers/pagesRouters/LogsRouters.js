import React, {Component} from 'react'
import { Route, Switch } from "react-router-dom"

// Authenticated Pages Routers

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Logs Components
const IndexLogs   = asyncComponent(() => import('../../Components/Pages/Logs/indexLogs/IndexLogs'))
// const ShowLog     = asyncComponent(() => import('../../Components/Pages/Logs/showLog/ShowLog'))

class LogsRouters extends Component{
  render(){
    const {permissions} = this.props
    return (
      <Switch>
          <Route path="/logs" exact component={IndexLogs} />
          {/* <Route path="/logs/show/:id" exact component={ShowLog} /> */}
      </Switch>
    )
  }
}

export default LogsRouters
