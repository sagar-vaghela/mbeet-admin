import React, {Component} from 'react'
import { Route, Switch } from "react-router-dom"

// Authenticated Pages Routers

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Roles Components
const IndexRoles   = asyncComponent(() => import('../../Components/Pages/Roles/indexRoles/IndexRoles'))
const AddRole     = asyncComponent(() => import('../../Components/Pages/Roles/addRole/AddRole'))
const ShowRole     = asyncComponent(() => import('../../Components/Pages/Roles/showRole/ShowRole'))
const EditRole     = asyncComponent(() => import('../../Components/Pages/Roles/editRole/EditRole'))

class RolesRouters extends Component{
  render(){
    const {permissions} = this.props
    return (
      <Switch>
        {permissions && (!permissions.dashboard_permissions.view && !permissions.users_permissions.view && !permissions.unit_permissions.view && !permissions.booking_permissions.view && !permissions.coupon_permissions.view && !permissions.city_permissions.view && permissions.role_permissions.view ) &&
          <Route path="/" exact component={IndexRoles} />
        }
        {permissions && permissions.role_permissions.view &&
          <Route path="/roles" exact component={IndexRoles} />
        }
        {permissions && permissions.role_permissions.edit &&
          <Route path="/roles/edit/:id" exact component={EditRole} />
        }
        {permissions && permissions.role_permissions.show &&
          <Route path="/roles/show/:id" exact component={ShowRole} />
        }
        {permissions && permissions.role_permissions.create &&
          <Route path="/roles/add" exact component={AddRole} />
        }
      </Switch>
    )
  }
}

export default RolesRouters
