import React, {Component} from 'react'
import { Route, Switch } from "react-router-dom"

// Authenticated Pages Routers

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// SpecialPrices Components
const IndexSpecialPrices   = asyncComponent(() => import('../../Components/Pages/Units/SpecialPrices/indexSpecialPrices/IndexSpecialPrices'))
const AddSpecialPrice   = asyncComponent(() => import('../../Components/Pages/Units/SpecialPrices/addSpecialPrice/AddSpecialPrice'))
const EditSpecialPrice   = asyncComponent(() => import('../../Components/Pages/Units/SpecialPrices/editSpecialPrice/EditSpecialPrice'))

class SpecialPricesRouters extends Component{
  render(){
    const {permissions} = this.props
    return (
      <Switch>
        {permissions && permissions.special_price_permissions.view &&
          <Route path="/units/:id/special_price" exact component={IndexSpecialPrices} />
        }
        {permissions && permissions.special_price_permissions.edit &&
          <Route path="/units/:unit_id/special_price/edit/:id" exact component={EditSpecialPrice} />
        }
        {permissions && permissions.special_price_permissions.create &&
          <Route path="/units/:id/special_price/add" exact component={AddSpecialPrice} />
        }
      </Switch>
    )
  }
}

export default SpecialPricesRouters
