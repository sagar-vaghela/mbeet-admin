import React, {Component} from 'react'
import { Route, Switch } from "react-router-dom"

// Authenticated Pages Routers

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Top Destinations Components
const IndexTopDestination   = asyncComponent(() => import('../../Components/Pages/TopDestinations/indexTopDestinations/IndexTopDestinations'))
const AddTopDestination   = asyncComponent(() => import('../../Components/Pages/TopDestinations/addTopDestination/AddTopDestination'))
const EditTopDestination   = asyncComponent(() => import('../../Components/Pages/TopDestinations/editTopDestination/EditTopDestination'))

class TopDestinationsRouters extends Component{
  render(){
    const {permissions} = this.props
    return (
      <Switch>
        {permissions && (!permissions.dashboard_permissions.view && !permissions.users_permissions.view && !permissions.unit_permissions.view && !permissions.booking_permissions.view && !permissions.coupon_permissions.view && !permissions.city_permissions.view && !permissions.role_permissions.view && permissions.top_destination_permissions.view) &&
          <Route path="/" exact component={IndexTopDestination} />
        }
        {permissions && permissions.top_destination_permissions.view &&
          <Route path="/topDestinations" exact component={IndexTopDestination} />
        }
        {permissions && permissions.top_destination_permissions.create &&
          <Route path="/topDestinations/add" exact component={AddTopDestination} />
        }
        {permissions && permissions.top_destination_permissions.edit &&
          <Route path="/topDestinations/edit/:id" exact component={EditTopDestination} />
        }

      </Switch>
    )
  }
}

export default TopDestinationsRouters
