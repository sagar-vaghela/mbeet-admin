import React, {Component} from 'react'
import { Route, Switch } from "react-router-dom"

// Authenticated Pages Routers

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Units Components
const IndexUnits   = asyncComponent(() => import('../../Components/Pages/Units/indexUnits/IndexUnits'))
const AddUnit   = asyncComponent(() => import('../../Components/Pages/Units/addUnit/AddUnit'))
const EditUnit   = asyncComponent(() => import('../../Components/Pages/Units/editUnit/EditUnit'))
const ShowUnit   = asyncComponent(() => import('../../Components/Pages/Units/showUnit/ShowUnit'))

class UnitsRouters extends Component{
  render(){
    const {permissions} = this.props
    return (
      <Switch>
        {permissions && (!permissions.dashboard_permissions.view && !permissions.users_permissions.view && permissions.unit_permissions.view) &&
          <Route path="/" exact component={IndexUnits} />
        }
        {permissions && permissions.unit_permissions.view &&
          <Route path="/units" exact component={IndexUnits} />
        }
        {permissions && permissions.unit_permissions.edit &&
          <Route path="/units/edit/:id" exact component={EditUnit} />
        }
        {permissions && permissions.unit_permissions.show &&
          <Route path="/units/show/:id" exact component={ShowUnit} />
        }
        {permissions && permissions.unit_permissions.create &&
          <Route path="/units/add" exact component={AddUnit} />
        }
      </Switch>
    )
  }
}

export default UnitsRouters
