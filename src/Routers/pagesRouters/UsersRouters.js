import React, {Component} from 'react'
import { Route, Switch } from "react-router-dom"

// Authenticated Pages Routers

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Login Components
const IndexUsers   = asyncComponent(() => import('../../Components/Pages/Users/indexUsers/IndexUsers'))
const ShowUser     = asyncComponent(() => import('../../Components/Pages/Users/showUser/ShowUser'))
const EditUser     = asyncComponent(() => import('../../Components/Pages/Users/editUser/EditUser'))
const AddUser     = asyncComponent(() => import('../../Components/Pages/Users/addUser/AddUser'))

class UsersRouters extends Component{
  render(){
    const {permissions} = this.props
    return (
      <Switch>
        {permissions && (!permissions.dashboard_permissions.view && permissions.users_permissions.view) &&
          <Route path="/" exact component={IndexUsers} />
        }
        {permissions && permissions.users_permissions.view &&
          <Route path="/users" exact component={IndexUsers} />
        }
        {permissions && permissions.users_permissions.edit &&
          <Route path="/users/edit/:id" exact component={EditUser} />
        }
        {permissions && permissions.users_permissions.show &&
          <Route path="/users/show/:id" exact component={ShowUser} />
        }
        {permissions && permissions.users_permissions.create &&
          <Route path="/users/add" exact component={AddUser} />
        }
      </Switch>
    )
  }
}

export default UsersRouters
