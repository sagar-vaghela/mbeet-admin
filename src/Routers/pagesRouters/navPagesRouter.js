import React from 'react'
import { Route, Switch } from "react-router-dom"

import asyncComponent from '../../Components/GlobalComponents/AsyncComponent'

// Login Components
const About           = asyncComponent(() => import('../../Components/Pages/About/About'))
const Privacy         = asyncComponent(() => import('../../Components/Pages/Privacy/Privacy'))
const Login           = asyncComponent(() => import('../../Components/Pages/Auth/login/Login'))
const ResetPassword   = asyncComponent(() => import('../../Components/Pages/Auth/resetPassword/ResetPassword'))



const navPagesRouter = (
  <Switch>
    <Route path="/login" component={Login} />
    <Route path="/reset_password" component={ResetPassword} />
    <Route path="/about" component={About} />
    <Route path="/privacy" component={Privacy} />
  </Switch>
)
export default navPagesRouter
