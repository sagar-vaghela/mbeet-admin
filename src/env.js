/*
 *   App Environments
 *   use this file to add, edit or delete API URL
 *
 *   @env {Object} : API URLs
 *   @hosts {Object} : list of hosts that will use this App
 */


// API URLs related to evn
const env = {
  // development: 'http://api.dev.mbeetapp.com',
  // development: 'http://192.168.1.240:3000',
  // development: 'http://api.pro.mbeetapp.com',
  development: 'http://localhost:3000',

 staging: 'http://api.staging.mbeetapp.com',
 production: 'http://api.pro.mbeetapp.com',
}

// hosts that will use this App
let hosts = {
 development: ['dev.mbeet.clicksandbox.com', 'dev.mbeetapp.com', '13.228.110.8', 'localhost', 'local.mbeet.dev'],
 staging: [, 'staging.mbeet.clicksandbox.com','staging.mbeetapp.com', '52.76.100.147', 'local.mbeet.staging'],
 production: ['pro.mbeet.clicksandbox.com','mbeetapp.com','13.229.15.225', 'pro.mbeet.clicksandbox1.com', 'local.mbeet.prod']
}

const hostname = window.location.hostname
export const current_env = Object.keys(hosts).filter(host => hosts[host].includes(hostname))
export const API_URL = current_env.length != 0 ? env[current_env.toString()] : env.development
